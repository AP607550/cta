/********************
+------------------------------------------------------------+
 | Class Name: CtaAvailableFormSectionServiceImpl.java                                  |
 | Class Purpose: Controller for cta_available_form_section                          |
 | Comment : This is API layer for cta_available_form_section                 |
 | Author : Mriganka Shekhar Roy                 |
+------------------------------------------------------------+
 ************/
package com.ctastudio.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ctastudio.dto.CtaAvailableFormSection;
import com.ctastudio.service.CtaAvailableFormSectionServiceImpl;
import com.ctastudio.util.URLDetails;

@Controller("ctaAvailableFormSectionController")
public class CtaAvailableFormSectionController {

	private static final Logger LOGGER = Logger
			.getLogger(CtaAvailableFormSectionController.class);

	@Autowired
	CtaAvailableFormSectionServiceImpl ctaAvailableFormSectionServiceImpl;

	@RequestMapping(value = URLDetails.CtaAvailableFormSection_Get_On_id, method = RequestMethod.GET)
	public @ResponseBody CtaAvailableFormSection getCtaAvailableFormSection(
			final HttpServletRequest request,
			final HttpServletResponse response, @PathVariable int formNum,
			@PathVariable String formSection) throws NumberFormatException,
			Exception {
		return ctaAvailableFormSectionServiceImpl.getCtaAvailableFormSection(
				formNum, formSection);
	}

	@RequestMapping(value = URLDetails.CtaAvailableFormSection_Get_All, method = RequestMethod.GET)
	public @ResponseBody List<CtaAvailableFormSection> getCtaAvailableFormSections(
			final HttpServletRequest request, final HttpServletResponse response)
			throws NumberFormatException, Exception {
		return ctaAvailableFormSectionServiceImpl
				.getAllCtaAvailableFormSections();
	}

	@RequestMapping(value = URLDetails.CtaAvailableFormSection_Create, method = RequestMethod.POST)
	public @ResponseBody CtaAvailableFormSection createCtaAvailableFormSection(
			final HttpServletRequest request,
			final HttpServletResponse response,
			@RequestBody CtaAvailableFormSection ctaAvailableFormSection)
			throws NumberFormatException, Exception {
		ctaAvailableFormSectionServiceImpl
				.createCtaAvailableFormSection(ctaAvailableFormSection);
		return ctaAvailableFormSection;
	}

	@RequestMapping(value = URLDetails.CtaAvailableFormSection_Update, method = RequestMethod.POST)
	public @ResponseBody CtaAvailableFormSection updateCtaAvailableFormSection(
			final HttpServletRequest request,
			final HttpServletResponse response,
			@RequestBody CtaAvailableFormSection ctaAvailableFormSection)
			throws NumberFormatException, Exception {
		ctaAvailableFormSectionServiceImpl
				.updateCtaAvailableFormSection(ctaAvailableFormSection);
		if (ctaAvailableFormSection.getId() != null)
			ctaAvailableFormSection = ctaAvailableFormSectionServiceImpl
					.getCtaAvailableFormSection(ctaAvailableFormSection.getId()
							.getFormNum(), ctaAvailableFormSection.getId()
							.getFormSection());
		return ctaAvailableFormSection;
	}

	@RequestMapping(value = URLDetails.CtaAvailableFormSection_Delete, method = RequestMethod.GET)
	public @ResponseBody String deleteCtaAvailableFormSection(
			final HttpServletRequest request,
			final HttpServletResponse response, @PathVariable int formNum,
			@PathVariable String formSection) throws NumberFormatException,
			Exception {

		boolean result = false;
		try {
			ctaAvailableFormSectionServiceImpl.deleteCtaAvailableFormSection(
					formNum, formSection);
			result = true;
		} catch (Exception e) {
			LOGGER.error("Error in delete operation for formNum:" + formNum, e);
		}
		return String.valueOf(result);
	}
}
