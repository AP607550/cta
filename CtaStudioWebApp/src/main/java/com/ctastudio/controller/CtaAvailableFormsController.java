/********************
+------------------------------------------------------------+
 | Class Name: CtaAvailableFormsController.java                                  |
 | Class Purpose: Controller for cta_available_forms                          |
 | Comment : This is API layer for cta_available_forms                 |
 | Author : Mriganka Shekhar Roy                 |
+------------------------------------------------------------+
 ************/
package com.ctastudio.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ctastudio.dto.CtaAvailableForm;
import com.ctastudio.dto.MultipleCtaAvailableForms;
import com.ctastudio.service.CtaAvailableFormsServiceImpl;
import com.ctastudio.util.URLDetails;

@Controller("ctaAvailableFormsController")
public class CtaAvailableFormsController {

	private static final Logger LOGGER = Logger
			.getLogger(CtaAvailableFormsController.class);

	@Autowired
	CtaAvailableFormsServiceImpl ctaAvailableFormsServiceImpl;

	@RequestMapping(value = URLDetails.CtaAvailableForm_Get_On_id, method = RequestMethod.GET)
	public @ResponseBody CtaAvailableForm getCtaAvailableForm(
			final HttpServletRequest request,
			final HttpServletResponse response, @PathVariable String id)
			throws NumberFormatException, Exception {
		return ctaAvailableFormsServiceImpl.getCtaAvailableForm(Integer
				.parseInt(id));
	}

	@RequestMapping(value = URLDetails.CtaAvailableForm_Get_All, method = RequestMethod.GET)
	public @ResponseBody List<CtaAvailableForm> getCtaAvailableForms(
			final HttpServletRequest request, final HttpServletResponse response)
			throws NumberFormatException, Exception {
		return ctaAvailableFormsServiceImpl.getAllCtaAvailableForms();
	}

	@RequestMapping(value = URLDetails.CtaAvailableForm_Create, method = RequestMethod.POST)
	public @ResponseBody CtaAvailableForm createCtaAvailableForm(
			final HttpServletRequest request,
			final HttpServletResponse response,
			@RequestBody CtaAvailableForm ctaAvailableForm)
			throws NumberFormatException, Exception {
		ctaAvailableFormsServiceImpl.createCtaAvailableForm(ctaAvailableForm);
		return ctaAvailableForm;
	}

	@RequestMapping(value = URLDetails.CtaAvailableForm_Update, method = RequestMethod.POST)
	public @ResponseBody CtaAvailableForm updateCtaAvailableForm(
			final HttpServletRequest request,
			final HttpServletResponse response,
			@RequestBody CtaAvailableForm ctaAvailableForm)
			throws NumberFormatException, Exception {
		ctaAvailableFormsServiceImpl.updateCtaAvailableForm(ctaAvailableForm);
		ctaAvailableForm = ctaAvailableFormsServiceImpl
				.getCtaAvailableForm(ctaAvailableForm.getFormNum());
		return ctaAvailableForm;
	}

	@RequestMapping(value = URLDetails.CtaAvailableForm_Delete, method = RequestMethod.GET)
	public @ResponseBody String deleteCtaAvailableForm(
			final HttpServletRequest request,
			final HttpServletResponse response, @PathVariable String id)
			throws NumberFormatException, Exception {

		boolean result = false;
		try {
			ctaAvailableFormsServiceImpl.deleteCtaAvailableForm(Integer
					.parseInt(id));
			result = true;
		} catch (Exception e) {
			LOGGER.error("Error in delete operation for id:" + id, e);
		}
		return String.valueOf(result);
	}
	
	@RequestMapping(value = URLDetails.CtaAvailableForm_Multiple, method = RequestMethod.POST)
	public @ResponseBody MultipleCtaAvailableForms multipleCtaAvailableForm(
			final HttpServletRequest request,
			final HttpServletResponse response,
			@RequestBody MultipleCtaAvailableForms multipleCtaAvailableForms)
			throws NumberFormatException, Exception {
		multipleCtaAvailableForms = ctaAvailableFormsServiceImpl
				.performMultipleOperation(multipleCtaAvailableForms);
		return multipleCtaAvailableForms;
	}

	@RequestMapping(value = URLDetails.CtaAvailableForm_Get_By_VersionNumber, method = RequestMethod.GET)
	public @ResponseBody List<CtaAvailableForm> getCtaAvailableFormByVersionNumber(
			final HttpServletRequest request,
			final HttpServletResponse response,
			@PathVariable String versionNumber) throws NumberFormatException,
			Exception {
		return ctaAvailableFormsServiceImpl
				.getCtaAvailableFormByVersionNumber(versionNumber);
	}
}
