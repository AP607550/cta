/********************
+------------------------------------------------------------+
 | Class Name: CtaProtocolController.java                                  |
 | Class Purpose: Controller for cta_study                          |
 | Comment : This is API layer for cta_study                 |
 | Author : Mriganka Shekhar Roy                 |
+------------------------------------------------------------+
 ************/
package com.ctastudio.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ctastudio.dto.CtaProtocol;
import com.ctastudio.dto.MultipleCtaProtocols;
import com.ctastudio.service.CtaProtocolServiceImpl;
import com.ctastudio.util.URLDetails;

@Controller("ctaProtocolController")
public class CtaProtocolController {

	private static final Logger LOGGER = Logger
			.getLogger(CtaProtocolController.class);

	@Autowired
	CtaProtocolServiceImpl ctaProtocolServiceImpl;

	@RequestMapping(value = URLDetails.CtaProtocol_Get_On_id, method = RequestMethod.GET)
	public @ResponseBody CtaProtocol getCtaProtocol(
			final HttpServletRequest request,
			final HttpServletResponse response, @PathVariable String id)
			throws NumberFormatException, Exception {
		return ctaProtocolServiceImpl.getCtaProtocol(Integer.parseInt(id));
	}

	@RequestMapping(value = URLDetails.CtaProtocol_Get_All, method = RequestMethod.GET)
	public @ResponseBody List<CtaProtocol> getCtaProtocols(
			final HttpServletRequest request, final HttpServletResponse response)
			throws NumberFormatException, Exception {
		return ctaProtocolServiceImpl.getAllCtaProtocols();
	}

	@RequestMapping(value = URLDetails.CtaProtocol_Create, method = RequestMethod.POST)
	public @ResponseBody CtaProtocol createCtaProtocol(
			final HttpServletRequest request,
			final HttpServletResponse response,
			@RequestBody CtaProtocol ctaProtocol) throws NumberFormatException,
			Exception {
		ctaProtocolServiceImpl.createCtaProtocol(ctaProtocol);
		return ctaProtocol;
	}

	@RequestMapping(value = URLDetails.CtaProtocol_Update, method = RequestMethod.POST)
	public @ResponseBody CtaProtocol updateCtaProtocol(
			final HttpServletRequest request,
			final HttpServletResponse response,
			@RequestBody CtaProtocol ctaProtocol) throws NumberFormatException,
			Exception {
		ctaProtocolServiceImpl.updateCtaProtocol(ctaProtocol);
		ctaProtocol = ctaProtocolServiceImpl.getCtaProtocol(ctaProtocol
				.getProtocolNum());
		return ctaProtocol;
	}

	@RequestMapping(value = URLDetails.CtaProtocol_Delete, method = RequestMethod.GET)
	public @ResponseBody String deleteCtaProtocol(
			final HttpServletRequest request,
			final HttpServletResponse response, @PathVariable String id)
			throws NumberFormatException, Exception {

		boolean result = false;
		try {
			ctaProtocolServiceImpl.deleteCtaProtocol(Integer.parseInt(id));
			result = true;
		} catch (Exception e) {
			LOGGER.error("Error in delete operation for id:" + id, e);
		}
		return String.valueOf(result);
	}

	@RequestMapping(value = URLDetails.CtaProtocol_Multiple, method = RequestMethod.POST)
	public @ResponseBody MultipleCtaProtocols multipleCtaProtocols(
			final HttpServletRequest request,
			final HttpServletResponse response,
			@RequestBody MultipleCtaProtocols multipleCtaProtocols)
			throws NumberFormatException, Exception {
		multipleCtaProtocols = ctaProtocolServiceImpl
				.performMultipleOperation(multipleCtaProtocols);
		return multipleCtaProtocols;
	}

	@RequestMapping(value = URLDetails.CtaProtocol_Get_By_VersionNumber, method = RequestMethod.GET)
	public @ResponseBody List<CtaProtocol> getCtaProtocolByVersionNumber(
			final HttpServletRequest request,
			final HttpServletResponse response,
			@PathVariable String versionNumber) throws NumberFormatException,
			Exception {
		return ctaProtocolServiceImpl
				.getCtaProtocolByVersionNumber(versionNumber);
	}
}
