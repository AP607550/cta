package com.ctastudio.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ctastudio.dto.CtaProtocolVisit;
import com.ctastudio.service.CtaProtocolVisitServiceImpl;
import com.ctastudio.util.URLDetails;

@Controller("ctaProtocolVisitController")
public class CtaProtocolVisitController {

	private static final Logger LOGGER = Logger
			.getLogger(CtaProtocolVisitController.class);

	@Autowired
	CtaProtocolVisitServiceImpl ctaProtocolVisitServiceImpl;

	@RequestMapping(value = URLDetails.CtaProtocolVisit_Get_On_id, method = RequestMethod.GET)
	public @ResponseBody CtaProtocolVisit getCtaProtocolVisit(
			final HttpServletRequest request,
			final HttpServletResponse response, @PathVariable int protocolNum,
			@PathVariable int protocolVisitNum) throws NumberFormatException,
			Exception {
		return ctaProtocolVisitServiceImpl.getCtaProtocolVisit(
				protocolNum, protocolVisitNum);
	}

	@RequestMapping(value = URLDetails.CtaProtocolVisit_Get_All, method = RequestMethod.GET)
	public @ResponseBody List<CtaProtocolVisit> getCtaProtocolVisits(
			final HttpServletRequest request, final HttpServletResponse response)
			throws NumberFormatException, Exception {
		return ctaProtocolVisitServiceImpl
				.getAllCtaProtocolVisits();
	}

	@RequestMapping(value = URLDetails.CtaProtocolVisit_Create, method = RequestMethod.POST)
	public @ResponseBody CtaProtocolVisit createCtaProtocolVisit(
			final HttpServletRequest request,
			final HttpServletResponse response,
			@RequestBody CtaProtocolVisit ctaProtocolVisit)
			throws NumberFormatException, Exception {
		ctaProtocolVisitServiceImpl
				.createCtaProtocolVisit(ctaProtocolVisit);
		return ctaProtocolVisit;
	}

	@RequestMapping(value = URLDetails.CtaProtocolVisit_Update, method = RequestMethod.POST)
	public @ResponseBody CtaProtocolVisit updateCtaProtocolVisit(
			final HttpServletRequest request,
			final HttpServletResponse response,
			@RequestBody CtaProtocolVisit ctaProtocolVisit)
			throws NumberFormatException, Exception {
		ctaProtocolVisitServiceImpl
				.updateCtaProtocolVisit(ctaProtocolVisit);
		if (ctaProtocolVisit.getId() != null)
			ctaProtocolVisit = ctaProtocolVisitServiceImpl
					.getCtaProtocolVisit(ctaProtocolVisit.getId()
							.getProtocolNum(), ctaProtocolVisit.getId()
							.getProtocolVisitNum());
		return ctaProtocolVisit;
	}

	@RequestMapping(value = URLDetails.CtaProtocolVisit_Delete, method = RequestMethod.GET)
	public @ResponseBody String deleteCtaProtocolVisit(
			final HttpServletRequest request,
			final HttpServletResponse response, @PathVariable int protocolNum,
			@PathVariable int protocolVisitNum) throws NumberFormatException,
			Exception {

		boolean result = false;
		try {
			ctaProtocolVisitServiceImpl.deleteCtaProtocolVisit(
					protocolNum, protocolVisitNum);
			result = true;
		} catch (Exception e) {
			LOGGER.error("Error in delete operation for protocolNum:" + protocolNum, e);
		}
		return String.valueOf(result);
	}
}
