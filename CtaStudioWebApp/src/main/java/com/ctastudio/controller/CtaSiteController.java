/********************
+------------------------------------------------------------+
 | Class Name: CtaSiteController.java                                  |
 | Class Purpose: Controller for cta_site                          |
 | Comment : This is API layer for cta_site                 |
 | Author : Mriganka Shekhar Roy                 |
+------------------------------------------------------------+
 ************/
package com.ctastudio.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ctastudio.dto.CtaSite;
import com.ctastudio.dto.MultipleCtaSites;
import com.ctastudio.service.CtaSiteServiceImpl;
import com.ctastudio.util.URLDetails;

@Controller("ctaSiteController")
public class CtaSiteController {

	private static final Logger LOGGER = Logger
			.getLogger(CtaSiteController.class);

	@Autowired
	CtaSiteServiceImpl ctaSiteServiceImpl;

	@RequestMapping(value = URLDetails.CtaSite_Get_On_site_num, method = RequestMethod.GET)
	public @ResponseBody CtaSite getCtaSite(final HttpServletRequest request,
			final HttpServletResponse response, @PathVariable String site_num)
			throws NumberFormatException, Exception {
		return ctaSiteServiceImpl.getCtaSite(Integer.parseInt(site_num));
	}

	@RequestMapping(value = URLDetails.CtaSite_Get_All, method = RequestMethod.GET)
	public @ResponseBody List<CtaSite> getCtaSites(
			final HttpServletRequest request, final HttpServletResponse response)
			throws NumberFormatException, Exception {
		return ctaSiteServiceImpl.getAllCtaSites();
	}

	@RequestMapping(value = URLDetails.CtaSite_Create, method = RequestMethod.POST)
	public @ResponseBody CtaSite createCtaSite(
			final HttpServletRequest request,
			final HttpServletResponse response, @RequestBody CtaSite ctaSite)
			throws NumberFormatException, Exception {
		ctaSiteServiceImpl.createCtaSite(ctaSite);
		return ctaSite;
	}

	@RequestMapping(value = URLDetails.CtaSite_Update, method = RequestMethod.POST)
	public @ResponseBody CtaSite updateCtaSite(
			final HttpServletRequest request,
			final HttpServletResponse response, @RequestBody CtaSite ctaSite)
			throws NumberFormatException, Exception {
		ctaSiteServiceImpl.updateCtaSite(ctaSite);
		ctaSite = ctaSiteServiceImpl.getCtaSite(ctaSite.getSiteNum());
		return ctaSite;
	}

	@RequestMapping(value = URLDetails.CtaSite_Delete, method = RequestMethod.GET)
	public @ResponseBody String deleteCtaSite(final HttpServletRequest request,
			final HttpServletResponse response, @PathVariable String site_num)
			throws NumberFormatException, Exception {

		boolean result = false;
		try {
			ctaSiteServiceImpl.deleteCtaSite(Integer.parseInt(site_num));
			result = true;
		} catch (Exception e) {
			LOGGER.error("Error in delete operation for site_num:" + site_num,
					e);
		}
		return String.valueOf(result);
	}

	@RequestMapping(value = URLDetails.CtaSite_Multiple, method = RequestMethod.POST)
	public @ResponseBody MultipleCtaSites multipleCtaSite(
			final HttpServletRequest request,
			final HttpServletResponse response,
			@RequestBody MultipleCtaSites multipleCtaSites)
			throws NumberFormatException, Exception {
		multipleCtaSites = ctaSiteServiceImpl
				.performMultipleOperation(multipleCtaSites);
		return multipleCtaSites;
	}

	@RequestMapping(value = URLDetails.CtaSite_Get_By_VersionNumber, method = RequestMethod.GET)
	public @ResponseBody List<CtaSite> getCtaSiteByVersionNumber(
			final HttpServletRequest request,
			final HttpServletResponse response,
			@PathVariable String versionNumber) throws NumberFormatException,
			Exception {
		return ctaSiteServiceImpl.getCtaSiteByVersionNumber(versionNumber);
	}

}
