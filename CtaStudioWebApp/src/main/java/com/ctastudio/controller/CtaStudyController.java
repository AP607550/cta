/********************
+------------------------------------------------------------+
 | Class Name: CtaStudyController.java                                  |
 | Class Purpose: Controller for cta_study                          |
 | Comment : This is API layer for cta_study                 |
 | Author : Mriganka Shekhar Roy                 |
+------------------------------------------------------------+
 ************/
package com.ctastudio.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ctastudio.dto.CtaStudy;
import com.ctastudio.dto.MultipleCtaStudies;
import com.ctastudio.service.CtaStudyServiceImpl;
import com.ctastudio.util.URLDetails;

@Controller("ctaStudyController")
public class CtaStudyController {

	private static final Logger LOGGER = Logger
			.getLogger(CtaStudyController.class);

	@Autowired
	CtaStudyServiceImpl ctaStudyServiceImpl;

	@RequestMapping(value = URLDetails.CtaStudy_Get_On_id, method = RequestMethod.GET)
	public @ResponseBody CtaStudy getCtaStudy(final HttpServletRequest request,
			final HttpServletResponse response, @PathVariable String id)
			throws NumberFormatException, Exception {
		return ctaStudyServiceImpl.getCtaStudy(Integer.parseInt(id));
	}

	@RequestMapping(value = URLDetails.CtaStudy_Get_All, method = RequestMethod.GET)
	public @ResponseBody List<CtaStudy> getCtaStudys(
			final HttpServletRequest request, final HttpServletResponse response)
			throws NumberFormatException, Exception {
		return ctaStudyServiceImpl.getAllCtaStudys();
	}

	@RequestMapping(value = URLDetails.CtaStudy_Create, method = RequestMethod.POST)
	public @ResponseBody CtaStudy createCtaStudy(
			final HttpServletRequest request,
			final HttpServletResponse response, @RequestBody CtaStudy ctaStudy)
			throws NumberFormatException, Exception {
		ctaStudyServiceImpl.createCtaStudy(ctaStudy);
		return ctaStudy;
	}

	@RequestMapping(value = URLDetails.CtaStudy_Update, method = RequestMethod.POST)
	public @ResponseBody CtaStudy updateCtaStudy(
			final HttpServletRequest request,
			final HttpServletResponse response, @RequestBody CtaStudy ctaStudy)
			throws NumberFormatException, Exception {
		ctaStudyServiceImpl.updateCtaStudy(ctaStudy);
		ctaStudy = ctaStudyServiceImpl.getCtaStudy(ctaStudy.getStudyNum());
		return ctaStudy;
	}

	@RequestMapping(value = URLDetails.CtaStudy_Delete, method = RequestMethod.GET)
	public @ResponseBody String deleteCtaStudy(
			final HttpServletRequest request,
			final HttpServletResponse response, @PathVariable String id)
			throws NumberFormatException, Exception {

		boolean result = false;
		try {
			ctaStudyServiceImpl.deleteCtaStudy(Integer.parseInt(id));
			result = true;
		} catch (Exception e) {
			LOGGER.error("Error in delete operation for id:" + id, e);
		}
		return String.valueOf(result);
	}

	@RequestMapping(value = URLDetails.CtaStudy_Multiple, method = RequestMethod.POST)
	public @ResponseBody MultipleCtaStudies multipleCtaStudy(
			final HttpServletRequest request,
			final HttpServletResponse response,
			@RequestBody MultipleCtaStudies multipleCtaStudies)
			throws NumberFormatException, Exception {
		multipleCtaStudies = ctaStudyServiceImpl
				.performMultipleOperation(multipleCtaStudies);
		return multipleCtaStudies;
	}

	@RequestMapping(value = URLDetails.CtaStudy_Get_By_VersionNumber, method = RequestMethod.GET)
	public @ResponseBody List<CtaStudy> getCtaStudyByVersionNumber(
			final HttpServletRequest request,
			final HttpServletResponse response,
			@PathVariable String versionNumber) throws NumberFormatException,
			Exception {
		return ctaStudyServiceImpl.getCtaStudyByVersionNumber(versionNumber);
	}
}
