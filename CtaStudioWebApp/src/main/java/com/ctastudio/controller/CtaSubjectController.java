/********************
+------------------------------------------------------------+
 | Class Name: CtaSubjectController.java                                  |
 | Class Purpose: controller for cta_subject                          |
 | Comment : This is API layer for cta_subject                 |
 | Author : Mriganka Shekhar Roy                 |
+------------------------------------------------------------+
 ************/
package com.ctastudio.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ctastudio.dto.CtaSubject;
import com.ctastudio.dto.MultipleCtaSubjects;
import com.ctastudio.service.CtaSubjectServiceImpl;
import com.ctastudio.util.URLDetails;

@Controller("ctaSubjectController")
public class CtaSubjectController {

	private static final Logger LOGGER = Logger
			.getLogger(CtaSubjectController.class);

	@Autowired
	CtaSubjectServiceImpl ctaSubjectServiceImpl;

	@RequestMapping(value = URLDetails.CtaSubject_Get_On_id, method = RequestMethod.GET)
	public @ResponseBody CtaSubject getCtaSubject(
			final HttpServletRequest request,
			final HttpServletResponse response, @PathVariable String id)
			throws NumberFormatException, Exception {
		return ctaSubjectServiceImpl.getCtaSubject(id);
	}

	@RequestMapping(value = URLDetails.CtaSubject_Get_All, method = RequestMethod.GET)
	public @ResponseBody List<CtaSubject> getCtaSubjects(
			final HttpServletRequest request, final HttpServletResponse response)
			throws NumberFormatException, Exception {
		return ctaSubjectServiceImpl.getAllCtaSubjects();
	}

	@RequestMapping(value = URLDetails.CtaSubject_Create, method = RequestMethod.POST)
	public @ResponseBody CtaSubject createCtaSubject(
			final HttpServletRequest request,
			final HttpServletResponse response,
			@RequestBody CtaSubject ctaSubject) throws NumberFormatException,
			Exception {
		ctaSubjectServiceImpl.createCtaSubject(ctaSubject);
		return ctaSubject;
	}

	@RequestMapping(value = URLDetails.CtaSubject_Update, method = RequestMethod.POST)
	public @ResponseBody CtaSubject updateCtaSubject(
			final HttpServletRequest request,
			final HttpServletResponse response,
			@RequestBody CtaSubject ctaSubject) throws NumberFormatException,
			Exception {
		ctaSubjectServiceImpl.updateCtaSubject(ctaSubject);
		ctaSubject = ctaSubjectServiceImpl.getCtaSubject(ctaSubject
				.getSubjectUuid());
		return ctaSubject;
	}

	@RequestMapping(value = URLDetails.CtaSubject_Delete, method = RequestMethod.GET)
	public @ResponseBody String deleteCtaSubject(
			final HttpServletRequest request,
			final HttpServletResponse response, @PathVariable String id)
			throws NumberFormatException, Exception {

		boolean result = false;
		try {
			ctaSubjectServiceImpl.deleteCtaSubject(id);
			result = true;
		} catch (Exception e) {
			LOGGER.error("Error in delete operation for id:" + id, e);
		}
		return String.valueOf(result);
	}

	@RequestMapping(value = URLDetails.CtaSubject_Multiple, method = RequestMethod.POST)
	public @ResponseBody MultipleCtaSubjects multipleCtaSubject(
			final HttpServletRequest request,
			final HttpServletResponse response,
			@RequestBody MultipleCtaSubjects multipleCtaSubjects)
			throws NumberFormatException, Exception {
		multipleCtaSubjects = ctaSubjectServiceImpl
				.performMultipleOperation(multipleCtaSubjects);
		return multipleCtaSubjects;
	}

	@RequestMapping(value = URLDetails.CtaSubject_Get_By_VersionNumber, method = RequestMethod.GET)
	public @ResponseBody List<CtaSubject> getCtaSubjectByVersionNumber(
			final HttpServletRequest request,
			final HttpServletResponse response,
			@PathVariable String versionNumber) throws NumberFormatException,
			Exception {
		return ctaSubjectServiceImpl
				.getCtaSubjectByVersionNumber(versionNumber);
	}
}
