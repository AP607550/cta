/********************
+------------------------------------------------------------+
 | Class Name: CtaUserController.java                                  |
 | Class Purpose: Controller for cta_user                          |
 | Comment : This is API layer for cta_user                 |
 | Author : Mriganka Shekhar Roy                 |
+------------------------------------------------------------+
 ************/
package com.ctastudio.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ctastudio.dto.CtaUser;
import com.ctastudio.dto.MultipleCtaUsers;
import com.ctastudio.service.CtaUserServiceImpl;
import com.ctastudio.util.URLDetails;

@Controller("ctaUserController")
public class CtaUserController {

	private static final Logger LOGGER = Logger
			.getLogger(CtaAvailableFormsController.class);

	@Autowired
	CtaUserServiceImpl ctaUserServiceImpl;

	@RequestMapping(value = URLDetails.CtaUser_Get_On_id, method = RequestMethod.GET)
	public @ResponseBody CtaUser getCtaUser(final HttpServletRequest request,
			final HttpServletResponse response, @PathVariable String id)
			throws NumberFormatException, Exception {
		return ctaUserServiceImpl.getCtaUser(id);
	}

	@RequestMapping(value = URLDetails.CtaUser_Get_All, method = RequestMethod.GET)
	public @ResponseBody List<CtaUser> getCtaUsers(
			final HttpServletRequest request, final HttpServletResponse response)
			throws NumberFormatException, Exception {
		return ctaUserServiceImpl.getAllCtaUsers();
	}

	@RequestMapping(value = URLDetails.CtaUser_Create, method = RequestMethod.POST)
	public @ResponseBody CtaUser createCtaUser(
			final HttpServletRequest request,
			final HttpServletResponse response, @RequestBody CtaUser ctaUser)
			throws NumberFormatException, Exception {
		ctaUserServiceImpl.createCtaUser(ctaUser);
		return ctaUser;
	}

	@RequestMapping(value = URLDetails.CtaUser_Update, method = RequestMethod.POST)
	public @ResponseBody CtaUser updateCtaUser(
			final HttpServletRequest request,
			final HttpServletResponse response, @RequestBody CtaUser ctaUser)
			throws NumberFormatException, Exception {
		ctaUserServiceImpl.updateCtaUser(ctaUser);
		ctaUser = ctaUserServiceImpl.getCtaUser(ctaUser.getUserCode());
		return ctaUser;
	}

	@RequestMapping(value = URLDetails.CtaUser_Delete, method = RequestMethod.GET)
	public @ResponseBody String deleteCtaUser(final HttpServletRequest request,
			final HttpServletResponse response, @PathVariable String id)
			throws NumberFormatException, Exception {

		boolean result = false;
		try {
			ctaUserServiceImpl.deleteCtaUser(id);
			result = true;
		} catch (Exception e) {
			LOGGER.error("Error in delete operation for id:" + id, e);
		}
		return String.valueOf(result);
	}

	@RequestMapping(value = URLDetails.CtaUser_Multiple, method = RequestMethod.POST)
	public @ResponseBody MultipleCtaUsers multipleCtaUser(
			final HttpServletRequest request,
			final HttpServletResponse response,
			@RequestBody MultipleCtaUsers multipleCtaUsers)
			throws NumberFormatException, Exception {
		multipleCtaUsers = ctaUserServiceImpl
				.performMultipleOperation(multipleCtaUsers);
		return multipleCtaUsers;
	}

	@RequestMapping(value = URLDetails.CtaUser_Get_By_VersionNumber, method = RequestMethod.GET)
	public @ResponseBody List<CtaUser> getCtaUserByVersionNumber(
			final HttpServletRequest request,
			final HttpServletResponse response,
			@PathVariable String versionNumber) throws NumberFormatException,
			Exception {
		return ctaUserServiceImpl.getCtaUserByVersionNumber(versionNumber);
	}
}
