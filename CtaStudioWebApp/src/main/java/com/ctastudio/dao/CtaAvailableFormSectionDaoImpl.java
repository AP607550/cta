/********************
+------------------------------------------------------------+
 | Class Name: CtaAvailableFormSectionDaoImpl.java                                  |
 | Class Purpose: DAO for cta_available_form_section                          |
 | Comment : This is DAO layer for cta_available_form_section                 |
 | Author : Mriganka Shekhar Roy                 |
+------------------------------------------------------------+
************/
package com.ctastudio.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ctastudio.dto.CtaAvailableFormSection;
import com.ctastudio.dto.CtaAvailableFormSectionPK;

@Repository
public class CtaAvailableFormSectionDaoImpl {

	private static final Logger LOGGER = Logger
			.getLogger(CtaAvailableFormSectionDaoImpl.class);

	@PersistenceContext
	protected EntityManager entityManager;

	@Transactional(readOnly = true)
	public CtaAvailableFormSection get(CtaAvailableFormSectionPK id) {
		if (id == null) {
			return null;
		} else {
			return entityManager.find(CtaAvailableFormSection.class, id);
		}

	}

	@Transactional(readOnly = true)
	public List<CtaAvailableFormSection> getAll() {
		return entityManager.createQuery(
				"Select o from " + CtaAvailableFormSection.class.getName()
						+ " o").getResultList();
	}

	@Transactional
	public void save(CtaAvailableFormSection object) {
		entityManager.persist(object);

	}

	@Transactional
	public void delete(CtaAvailableFormSection object) {
		entityManager.remove(entityManager.contains(object) ? object
				: entityManager.merge(object));

	}

	@Transactional
	public void update(CtaAvailableFormSection object) {
		entityManager.merge(object);
	}

}
