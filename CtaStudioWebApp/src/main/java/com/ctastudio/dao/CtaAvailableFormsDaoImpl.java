/********************
+------------------------------------------------------------+
 | Class Name: CtaAvailableFormsDaoImpl.java                                  |
 | Class Purpose: DAO for cta_available_forms                          |
 | Comment : This is DAO layer for cta_available_forms                 |
 | Author : Mriganka Shekhar Roy                 |
+------------------------------------------------------------+
 ************/
package com.ctastudio.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ctastudio.dto.CtaAvailableForm;
import com.ctastudio.dto.MultipleCtaAvailableForms;
import com.ctastudio.dto.MultipleCtaAvailableForms.CtaAvailableForms;
import com.ctastudio.util.Operations;

@Repository
public class CtaAvailableFormsDaoImpl {

	private static final Logger LOGGER = Logger
			.getLogger(CtaAvailableFormsDaoImpl.class);

	@PersistenceContext
	protected EntityManager entityManager;

	@Transactional(readOnly = true)
	public CtaAvailableForm get(Integer id) {
		if (id == null) {
			return null;
		} else {
			return entityManager.find(CtaAvailableForm.class, id);
		}

	}

	@Transactional(readOnly = true)
	public List<CtaAvailableForm> getAll() {
		return entityManager.createQuery(
				"Select o from " + CtaAvailableForm.class.getName() + " o")
				.getResultList();
	}

	@Transactional
	public void save(CtaAvailableForm object) {
		entityManager.persist(object);

	}

	@Transactional
	public void delete(CtaAvailableForm object) {
		entityManager.remove(entityManager.contains(object) ? object
				: entityManager.merge(object));

	}

	@Transactional
	public void update(CtaAvailableForm object) {
		entityManager.merge(object);
	}

	@Transactional(readOnly = true)
	public List<CtaAvailableForm> getCtaAvailableFormByVersionNumber(
			String versionNumber) {
		return entityManager
				.createQuery(
						"Select o from " + CtaAvailableForm.class.getName()
								+ " o where o.versionNumber= :versionNumber")
				.setParameter("versionNumber", versionNumber).getResultList();
	}

	@Transactional
	public MultipleCtaAvailableForms performMultipleOperation(
			MultipleCtaAvailableForms multipleCtaAvailableForms)
			throws Exception {

		List<CtaAvailableForms> ctaAvailableForms = multipleCtaAvailableForms
				.getCtaAvailableForm();

		List<CtaAvailableForms> newCtaAvailableForms = new ArrayList<>();
		for (CtaAvailableForms ctaAvailableForm : ctaAvailableForms) {

			List<CtaAvailableForm> CtaAvailableFormList = ctaAvailableForm
					.getCtaAvailableForms();

			List<CtaAvailableForm> newCtaAvailableFormList = new ArrayList<CtaAvailableForm>();
			if (ctaAvailableForm.getAction().equalsIgnoreCase(
					Operations.UPDATE_OPERATION)) {
				for (CtaAvailableForm entity : CtaAvailableFormList) {
					entityManager.merge(entity);
					newCtaAvailableFormList.add(entity);
				}
			} else if (ctaAvailableForm.getAction().equalsIgnoreCase(
					Operations.CREATE_OPERATION)) {
				for (CtaAvailableForm entity : CtaAvailableFormList) {
					entityManager.persist(entity);
					newCtaAvailableFormList.add(entity);
				}
			} else if (ctaAvailableForm.getAction().equalsIgnoreCase(
					Operations.DELETE_OPERATION)) {
				for (CtaAvailableForm entity : CtaAvailableFormList) {
					newCtaAvailableFormList.add(entity);
					entity = entityManager.find(CtaAvailableForm.class,
							entity.getFormNum());
					if (entity != null) {
						entityManager
								.remove(entityManager.contains(entity) ? entity
										: entityManager.merge(entity));
					}
				}
			}
			ctaAvailableForm.setCtaAvailableForms(newCtaAvailableFormList);
			newCtaAvailableForms.add(ctaAvailableForm);
		}
		multipleCtaAvailableForms.setCtaAvailableForm(newCtaAvailableForms);
		return multipleCtaAvailableForms;
	}

}
