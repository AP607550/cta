/********************
+------------------------------------------------------------+
 | Class Name: CtaProtocolDaoImpl.java                                  |
 | Class Purpose: DAO for cta_protocol                          |
 | Comment : This is DAO layer for cta_protocol                 |
 | Author : Mriganka Shekhar Roy                 |
+------------------------------------------------------------+
************/
package com.ctastudio.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ctastudio.dto.CtaProtocol;
import com.ctastudio.dto.MultipleCtaProtocols;
import com.ctastudio.dto.MultipleCtaProtocols.CtaProtocols;
import com.ctastudio.util.Operations;

@Repository
public class CtaProtocolDaoImpl {

	private static final Logger LOGGER = Logger
			.getLogger(CtaProtocolDaoImpl.class);

	@PersistenceContext
	protected EntityManager entityManager;
	
	@Transactional(readOnly = true)
	public CtaProtocol get(Integer id) {
		if (id == null) {
			return null;
		} else {
			return entityManager.find(CtaProtocol.class, id);
		}

	}

	@Transactional(readOnly = true)
	public List<CtaProtocol> getAll() {
		return entityManager.createQuery(
				"Select o from " + CtaProtocol.class.getName() + " o")
				.getResultList();
	}

	@Transactional
	public void save(CtaProtocol object) {
		entityManager.persist(object);

	}

	@Transactional
	public void delete(CtaProtocol object) {
		entityManager.remove(entityManager.contains(object) ? object : entityManager.merge(object));

	}

	@Transactional
	public void update(CtaProtocol object) {
		entityManager.merge(object);
	}
	
	@Transactional(readOnly = true)
	public List<CtaProtocol> getCtaProtocolByVersionNumber(
			String versionNumber) {
		return entityManager
				.createQuery(
						"Select o from " + CtaProtocol.class.getName()
								+ " o where o.versionNumber= :versionNumber")
				.setParameter("versionNumber", versionNumber).getResultList();
	}

	@Transactional
	public MultipleCtaProtocols performMultipleOperation(
			MultipleCtaProtocols multipleCtaProtocols)
			throws Exception {

		List<CtaProtocols> ctaProtocols = multipleCtaProtocols
				.getCtaProtocol();

		List<CtaProtocols> newCtaProtocols = new ArrayList<>();
		for (CtaProtocols ctaProtocol : ctaProtocols) {

			List<CtaProtocol> CtaProtocolList = ctaProtocol
					.getCtaProtocols();

			List<CtaProtocol> newCtaProtocolList = new ArrayList<CtaProtocol>();
			if (ctaProtocol.getAction().equalsIgnoreCase(
					Operations.UPDATE_OPERATION)) {
				for (CtaProtocol entity : CtaProtocolList) {
					entityManager.merge(entity);
					newCtaProtocolList.add(entity);
				}
			} else if (ctaProtocol.getAction().equalsIgnoreCase(
					Operations.CREATE_OPERATION)) {
				for (CtaProtocol entity : CtaProtocolList) {
					entityManager.persist(entity);
					newCtaProtocolList.add(entity);
				}
			} else if (ctaProtocol.getAction().equalsIgnoreCase(
					Operations.DELETE_OPERATION)) {
				for (CtaProtocol entity : CtaProtocolList) {
					newCtaProtocolList.add(entity);
					entity = entityManager.find(CtaProtocol.class,
							entity.getProtocolNum());
					if (entity != null) {
						entityManager
								.remove(entityManager.contains(entity) ? entity
										: entityManager.merge(entity));
					}
				}
			}
			ctaProtocol.setCtaProtocols(newCtaProtocolList);
			newCtaProtocols.add(ctaProtocol);
		}
		multipleCtaProtocols.setCtaProtocol(newCtaProtocols);
		return multipleCtaProtocols;
	}

}
