package com.ctastudio.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ctastudio.dto.CtaProtocolVisit;
import com.ctastudio.dto.CtaProtocolVisitPK;

@Repository
public class CtaProtocolVisitDaoImpl {
	
	private static final Logger LOGGER = Logger
			.getLogger(CtaProtocolVisitDaoImpl.class);

	@PersistenceContext
	protected EntityManager entityManager;

	@Transactional(readOnly = true)
	public CtaProtocolVisit get(CtaProtocolVisitPK id) {
		if (id == null) {
			return null;
		} else {
			return entityManager.find(CtaProtocolVisit.class, id);
		}

	}

	@Transactional(readOnly = true)
	public List<CtaProtocolVisit> getAll() {
		return entityManager.createQuery(
				"Select o from " + CtaProtocolVisit.class.getName() + " o")
				.getResultList();
	}

	@Transactional
	public void save(CtaProtocolVisit object) {
		entityManager.persist(object);

	}

	@Transactional
	public void delete(CtaProtocolVisit object) {
		entityManager.remove(entityManager.contains(object) ? object : entityManager.merge(object));

	}

	@Transactional
	public void update(CtaProtocolVisit object) {
		entityManager.merge(object);
	}
	
	@Transactional(readOnly = true)
	public List<CtaProtocolVisit> getCtaProtocolVisitByVersionNumber(
			String versionNumber) {
		return entityManager
				.createQuery(
						"Select o from " + CtaProtocolVisit.class.getName()
								+ " o where o.versionNumber= :versionNumber")
				.setParameter("versionNumber", versionNumber).getResultList();
	}
	
	@Transactional(readOnly = true)
	public List<CtaProtocolVisit> getCtaProtocolVisitByProtocolNumber(
			int protocolNumber) {
		return entityManager
				.createQuery(
						"Select o from " + CtaProtocolVisit.class.getName()
								+ " o where o.id.protocolNum= :protocolNum")
				.setParameter("protocolNum", protocolNumber).getResultList();
	}
	
	@Transactional(readOnly = true)
	public Integer getMaxProtocolVistNumberForProtocolNumber(
			int protocolNumber) {
		return (Integer)entityManager
				.createQuery(
						"Select max(o.id.protocolVisitNum) from " + CtaProtocolVisit.class.getName()
								+ " o where o.id.protocolNum= :protocolNum")
				.setParameter("protocolNum", protocolNumber).getSingleResult();
	}

	/*@Transactional
	public MultipleCtaProtocolVisits performMultipleOperation(
			MultipleCtaProtocolVisits multipleCtaProtocolVisits)
			throws Exception {

		List<CtaProtocolVisits> ctaProtocolVisits = multipleCtaProtocolVisits
				.getCtaProtocolVisit();

		List<CtaProtocolVisits> newCtaProtocolVisits = new ArrayList<>();
		for (CtaProtocolVisits ctaProtocolVisit : ctaProtocolVisits) {

			List<CtaProtocolVisit> CtaProtocolVisitList = ctaProtocolVisit
					.getCtaProtocolVisits();

			List<CtaProtocolVisit> newCtaProtocolVisitList = new ArrayList<CtaProtocolVisit>();
			if (ctaProtocolVisit.getAction().equalsIgnoreCase(
					Operations.UPDATE_OPERATION)) {
				for (CtaProtocolVisit entity : CtaProtocolVisitList) {
					entityManager.merge(entity);
					newCtaProtocolVisitList.add(entity);
				}
			} else if (ctaProtocolVisit.getAction().equalsIgnoreCase(
					Operations.CREATE_OPERATION)) {
				for (CtaProtocolVisit entity : CtaProtocolVisitList) {
					entityManager.persist(entity);
					newCtaProtocolVisitList.add(entity);
				}
			} else if (ctaProtocolVisit.getAction().equalsIgnoreCase(
					Operations.DELETE_OPERATION)) {
				for (CtaProtocolVisit entity : CtaProtocolVisitList) {
					newCtaProtocolVisitList.add(entity);
					entity = entityManager.find(CtaProtocolVisit.class,
							entity.getSiteNum());
					if (entity != null) {
						entityManager
								.remove(entityManager.contains(entity) ? entity
										: entityManager.merge(entity));
					}
				}
			}
			ctaProtocolVisit.setCtaProtocolVisits(newCtaProtocolVisitList);
			newCtaProtocolVisits.add(ctaProtocolVisit);
		}
		multipleCtaProtocolVisits.setCtaProtocolVisit(newCtaProtocolVisits);
		return multipleCtaProtocolVisits;
	}*/
}
