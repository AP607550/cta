/********************
+------------------------------------------------------------+
 | Class Name: CtaSiteDaoImpl.java                                  |
 | Class Purpose: DAO for cta_site                          |
 | Comment : This is DAO layer for cta_site                 |
 | Author : Mriganka Shekhar Roy                 |
+------------------------------------------------------------+
************/
package com.ctastudio.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ctastudio.dto.CtaSite;
import com.ctastudio.dto.MultipleCtaSites;
import com.ctastudio.dto.MultipleCtaSites.CtaSites;
import com.ctastudio.util.Operations;

@Repository
public class CtaSiteDaoImpl {
	
	private static final Logger LOGGER = Logger
			.getLogger(CtaSiteDaoImpl.class);

	@PersistenceContext
	protected EntityManager entityManager;

	@Transactional(readOnly = true)
	public CtaSite get(Integer id) {
		if (id == null) {
			return null;
		} else {
			return entityManager.find(CtaSite.class, id);
		}

	}

	@Transactional(readOnly = true)
	public List<CtaSite> getAll() {
		return entityManager.createQuery(
				"Select o from " + CtaSite.class.getName() + " o")
				.getResultList();
	}

	@Transactional
	public void save(CtaSite object) {
		entityManager.persist(object);

	}

	@Transactional
	public void delete(CtaSite object) {
		entityManager.remove(entityManager.contains(object) ? object : entityManager.merge(object));

	}

	@Transactional
	public void update(CtaSite object) {
		entityManager.merge(object);
	}
	
	@Transactional(readOnly = true)
	public List<CtaSite> getCtaSiteByVersionNumber(
			String versionNumber) {
		return entityManager
				.createQuery(
						"Select o from " + CtaSite.class.getName()
								+ " o where o.versionNumber= :versionNumber")
				.setParameter("versionNumber", versionNumber).getResultList();
	}

	@Transactional
	public MultipleCtaSites performMultipleOperation(
			MultipleCtaSites multipleCtaSites)
			throws Exception {

		List<CtaSites> ctaSites = multipleCtaSites
				.getCtaSite();

		List<CtaSites> newCtaSites = new ArrayList<>();
		for (CtaSites ctaSite : ctaSites) {

			List<CtaSite> CtaSiteList = ctaSite
					.getCtaSites();

			List<CtaSite> newCtaSiteList = new ArrayList<CtaSite>();
			if (ctaSite.getAction().equalsIgnoreCase(
					Operations.UPDATE_OPERATION)) {
				for (CtaSite entity : CtaSiteList) {
					entityManager.merge(entity);
					newCtaSiteList.add(entity);
				}
			} else if (ctaSite.getAction().equalsIgnoreCase(
					Operations.CREATE_OPERATION)) {
				for (CtaSite entity : CtaSiteList) {
					entityManager.persist(entity);
					newCtaSiteList.add(entity);
				}
			} else if (ctaSite.getAction().equalsIgnoreCase(
					Operations.DELETE_OPERATION)) {
				for (CtaSite entity : CtaSiteList) {
					newCtaSiteList.add(entity);
					entity = entityManager.find(CtaSite.class,
							entity.getSiteNum());
					if (entity != null) {
						entityManager
								.remove(entityManager.contains(entity) ? entity
										: entityManager.merge(entity));
					}
				}
			}
			ctaSite.setCtaSites(newCtaSiteList);
			newCtaSites.add(ctaSite);
		}
		multipleCtaSites.setCtaSite(newCtaSites);
		return multipleCtaSites;
	}


}
