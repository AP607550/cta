/********************
+------------------------------------------------------------+
 | Class Name: CtaStudyDaoImpl.java                                  |
 | Class Purpose: DAO for cta_study                          |
 | Comment : This is DAO layer for cta_study                 |
 | Author : Mriganka Shekhar Roy                 |
+------------------------------------------------------------+
 ************/
package com.ctastudio.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ctastudio.dto.CtaStudy;
import com.ctastudio.dto.MultipleCtaStudies;
import com.ctastudio.dto.MultipleCtaStudies.CtaStudies;
import com.ctastudio.util.Operations;

@Repository
public class CtaStudyDaoImpl {

	private static final Logger LOGGER = Logger
			.getLogger(CtaStudyDaoImpl.class);

	@PersistenceContext
	protected EntityManager entityManager;

	@Transactional(readOnly = true)
	public CtaStudy get(Integer id) {
		if (id == null) {
			return null;
		} else {
			return entityManager.find(CtaStudy.class, id);
		}

	}

	@Transactional(readOnly = true)
	public List<CtaStudy> getAll() {
		return entityManager.createQuery(
				"Select o from " + CtaStudy.class.getName() + " o")
				.getResultList();
	}

	@Transactional
	public void save(CtaStudy object) {
		entityManager.persist(object);

	}

	@Transactional
	public void delete(CtaStudy object) {
		entityManager.remove(entityManager.contains(object) ? object
				: entityManager.merge(object));

	}

	@Transactional
	public void update(CtaStudy object) {
		entityManager.merge(object);
	}

	@Transactional(readOnly = true)
	public List<CtaStudy> getCtaStudyByVersionNumber(String versionNumber) {
		return entityManager
				.createQuery(
						"Select o from " + CtaStudy.class.getName()
								+ " o where o.versionNumber= :versionNumber")
				.setParameter("versionNumber", versionNumber).getResultList();
	}

	@Transactional
	public MultipleCtaStudies performMultipleOperation(
			MultipleCtaStudies multipleCtaStudies) throws Exception {

		List<CtaStudies> ctaStudies = multipleCtaStudies.getCtaStudies();

		List<CtaStudies> newCtaStudies = new ArrayList<MultipleCtaStudies.CtaStudies>();
		for (CtaStudies ctaStudie : ctaStudies) {

			List<CtaStudy> ctaStudyList = ctaStudie.getCtaStudies();

			List<CtaStudy> newCtaStudyList = new ArrayList<CtaStudy>();
			if (ctaStudie.getAction().equalsIgnoreCase(
					Operations.UPDATE_OPERATION)) {
				for (CtaStudy ctaStudy : ctaStudyList) {
					entityManager.merge(ctaStudy);
					newCtaStudyList.add(ctaStudy);
				}
			} else if (ctaStudie.getAction().equalsIgnoreCase(
					Operations.CREATE_OPERATION)) {
				for (CtaStudy ctaStudy : ctaStudyList) {
					entityManager.persist(ctaStudy);
					newCtaStudyList.add(ctaStudy);
				}
			} else if (ctaStudie.getAction().equalsIgnoreCase(
					Operations.DELETE_OPERATION)) {
				for (CtaStudy ctaStudy : ctaStudyList) {
					newCtaStudyList.add(ctaStudy);
					ctaStudy = entityManager.find(CtaStudy.class,
							ctaStudy.getStudyNum());
					if (ctaStudy != null) {
						entityManager
								.remove(entityManager.contains(ctaStudy) ? ctaStudy
										: entityManager.merge(ctaStudy));
					}
				}
			}
			ctaStudie.setCtaStudies(newCtaStudyList);
			newCtaStudies.add(ctaStudie);
		}
		multipleCtaStudies.setCtaStudies(newCtaStudies);
		return multipleCtaStudies;
	}

}
