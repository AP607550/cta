/********************
+------------------------------------------------------------+
 | Class Name: CtaSubjectDaoImpl.java                                  |
 | Class Purpose: DAO for cta_subject                          |
 | Comment : This is DAO layer for cta_subject                 |
 | Author : Mriganka Shekhar Roy                 |
+------------------------------------------------------------+
************/
package com.ctastudio.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ctastudio.dto.CtaSubject;
import com.ctastudio.dto.MultipleCtaSubjects;
import com.ctastudio.dto.MultipleCtaSubjects.CtaSubjects;
import com.ctastudio.util.Operations;

@Repository
public class CtaSubjectDaoImpl {
	
	private static final Logger LOGGER = Logger
			.getLogger(CtaSubjectDaoImpl.class);
	
	@PersistenceContext
	protected EntityManager entityManager;

	@Transactional(readOnly = true)
	public CtaSubject get(String id) {
		if (id == null) {
			return null;
		} else {
			return entityManager.find(CtaSubject.class, id);
		}

	}

	@Transactional(readOnly = true)
	public List<CtaSubject> getAll() {
		return entityManager.createQuery(
				"Select o from " + CtaSubject.class.getName() + " o")
				.getResultList();
	}

	@Transactional
	public void save(CtaSubject object) {
		entityManager.persist(object);

	}

	@Transactional
	public void delete(CtaSubject object) {
		entityManager.remove(entityManager.contains(object) ? object : entityManager.merge(object));

	}

	@Transactional
	public void update(CtaSubject object) {
		entityManager.merge(object);
	}
	
	@Transactional(readOnly = true)
	public List<CtaSubject> getCtaSubjectByVersionNumber(
			String versionNumber) {
		return entityManager
				.createQuery(
						"Select o from " + CtaSubject.class.getName()
								+ " o where o.versionNumber= :versionNumber")
				.setParameter("versionNumber", versionNumber).getResultList();
	}

	@Transactional
	public MultipleCtaSubjects performMultipleOperation(
			MultipleCtaSubjects multipleCtaSubjects)
			throws Exception {

		List<CtaSubjects> ctaSubjects = multipleCtaSubjects
				.getCtaSubject();

		List<CtaSubjects> newCtaSubjects = new ArrayList<>();
		for (CtaSubjects ctaSubject : ctaSubjects) {

			List<CtaSubject> CtaSubjectList = ctaSubject
					.getCtaSubjects();

			List<CtaSubject> newCtaSubjectList = new ArrayList<CtaSubject>();
			if (ctaSubject.getAction().equalsIgnoreCase(
					Operations.UPDATE_OPERATION)) {
				for (CtaSubject entity : CtaSubjectList) {
					entityManager.merge(entity);
					newCtaSubjectList.add(entity);
				}
			} else if (ctaSubject.getAction().equalsIgnoreCase(
					Operations.CREATE_OPERATION)) {
				for (CtaSubject entity : CtaSubjectList) {
					entityManager.persist(entity);
					newCtaSubjectList.add(entity);
				}
			} else if (ctaSubject.getAction().equalsIgnoreCase(
					Operations.DELETE_OPERATION)) {
				for (CtaSubject entity : CtaSubjectList) {
					newCtaSubjectList.add(entity);
					entity = entityManager.find(CtaSubject.class,
							entity.getSubjectUuid());
					if (entity != null) {
						entityManager
								.remove(entityManager.contains(entity) ? entity
										: entityManager.merge(entity));
					}
				}
			}
			ctaSubject.setCtaSubjects(newCtaSubjectList);
			newCtaSubjects.add(ctaSubject);
		}
		multipleCtaSubjects.setCtaSubject(newCtaSubjects);
		return multipleCtaSubjects;
	}

}
