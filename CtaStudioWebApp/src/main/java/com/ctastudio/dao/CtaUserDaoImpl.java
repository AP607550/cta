/********************
+------------------------------------------------------------+
 | Class Name: CtaUserDaoImpl.java                                  |
 | Class Purpose: DAO for cta_user                          |
 | Comment : This is DAO layer for cta_user                 |
 | Author : Mriganka Shekhar Roy                 |
+------------------------------------------------------------+
************/
package com.ctastudio.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ctastudio.dto.CtaUser;
import com.ctastudio.dto.MultipleCtaUsers;
import com.ctastudio.dto.MultipleCtaUsers.CtaUsers;
import com.ctastudio.util.Operations;

@Repository
public class CtaUserDaoImpl {

	private static final Logger LOGGER = Logger
			.getLogger(CtaUserDaoImpl.class);
	
	@PersistenceContext
	protected EntityManager entityManager;

	@Transactional(readOnly = true)
	public CtaUser get(String id) {
		if (id == null) {
			return null;
		} else {
			return entityManager.find(CtaUser.class, id);
		}

	}

	@Transactional(readOnly = true)
	public List<CtaUser> getAll() {
		return entityManager.createQuery(
				"Select o from " + CtaUser.class.getName() + " o")
				.getResultList();
	}

	@Transactional
	public void save(CtaUser object) {
		entityManager.persist(object);

	}

	@Transactional
	public void delete(CtaUser object) {
		entityManager.remove(entityManager.contains(object) ? object : entityManager.merge(object));

	}

	@Transactional
	public void update(CtaUser object) {
		entityManager.merge(object);
	}
	
	@Transactional(readOnly = true)
	public List<CtaUser> getCtaUserByVersionNumber(
			String versionNumber) {
		return entityManager
				.createQuery(
						"Select o from " + CtaUser.class.getName()
								+ " o where o.versionNumber= :versionNumber")
				.setParameter("versionNumber", versionNumber).getResultList();
	}

	@Transactional
	public MultipleCtaUsers performMultipleOperation(
			MultipleCtaUsers multipleCtaUsers)
			throws Exception {

		List<CtaUsers> ctaUsers = multipleCtaUsers
				.getCtaUser();

		List<CtaUsers> newCtaUsers = new ArrayList<>();
		for (CtaUsers ctaUser : ctaUsers) {

			List<CtaUser> CtaUserList = ctaUser
					.getCtaUsers();

			List<CtaUser> newCtaUserList = new ArrayList<CtaUser>();
			if (ctaUser.getAction().equalsIgnoreCase(
					Operations.UPDATE_OPERATION)) {
				for (CtaUser entity : CtaUserList) {
					entityManager.merge(entity);
					newCtaUserList.add(entity);
				}
			} else if (ctaUser.getAction().equalsIgnoreCase(
					Operations.CREATE_OPERATION)) {
				for (CtaUser entity : CtaUserList) {
					entityManager.persist(entity);
					newCtaUserList.add(entity);
				}
			} else if (ctaUser.getAction().equalsIgnoreCase(
					Operations.DELETE_OPERATION)) {
				for (CtaUser entity : CtaUserList) {
					newCtaUserList.add(entity);
					entity = entityManager.find(CtaUser.class,
							entity.getUserCode());
					if (entity != null) {
						entityManager
								.remove(entityManager.contains(entity) ? entity
										: entityManager.merge(entity));
					}
				}
			}
			ctaUser.setCtaUsers(newCtaUserList);
			newCtaUsers.add(ctaUser);
		}
		multipleCtaUsers.setCtaUser(newCtaUsers);
		return multipleCtaUsers;
	}

}
