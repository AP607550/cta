package com.ctastudio.dao;

import java.util.List;

import com.ctastudio.dto.DomainObject;


public interface GenericDao<T extends DomainObject>{
	public T get(Long id);
	public List<T>getAll();
	public void save(T object);
	public void delete(T object);
	public void update(T object);

}
