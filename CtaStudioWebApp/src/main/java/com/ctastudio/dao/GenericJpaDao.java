package com.ctastudio.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.ctastudio.dto.DomainObject;


public class GenericJpaDao<T extends DomainObject> implements GenericDao<T> {

	@PersistenceContext
	protected EntityManager entityManager;

	private Class<T> type;

	public GenericJpaDao(Class<T> type) {
		super();
		this.type = type;

	}

	@Transactional(readOnly = true)
	public T get(Long id) {
		if (id == null) {
			return null;
		} else {
			return entityManager.find(type, id);
		}

	}

	@Transactional(readOnly = true)
	public List<T> getAll() {
		return entityManager.createQuery(
				"Select o from " + type.getName() + " o").getResultList();
	}

	public void save(T object) {
		entityManager.persist(object);

	}

	public void delete(T object) {
		entityManager.remove(object);

	}

	@Transactional
	public void update(T object) {
		entityManager.merge(object);
	}

}
