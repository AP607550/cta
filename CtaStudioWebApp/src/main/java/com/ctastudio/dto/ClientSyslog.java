package com.ctastudio.dto;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the client_syslog database table.
 * 
 */
@Entity
@Table(name="client_syslog")
public class ClientSyslog implements Serializable, DomainObject {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="client_device_uuid")
	private String clientDeviceUuid;

	@Lob
	@Column(name="json_id")
	private String jsonId;

	@Lob
	private String message;

	private int priority;

	@Lob
	private String source;

	@Lob
	@Column(name="updated_date_time")
	private String updatedDateTime;

	@Column(name="updated_ts_gmt")
	private int updatedTsGmt;

	public ClientSyslog() {
	}

	public String getClientDeviceUuid() {
		return this.clientDeviceUuid;
	}

	public void setClientDeviceUuid(String clientDeviceUuid) {
		this.clientDeviceUuid = clientDeviceUuid;
	}

	public String getJsonId() {
		return this.jsonId;
	}

	public void setJsonId(String jsonId) {
		this.jsonId = jsonId;
	}

	public String getMessage() {
		return this.message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public int getPriority() {
		return this.priority;
	}

	public void setPriority(int priority) {
		this.priority = priority;
	}

	public String getSource() {
		return this.source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getUpdatedDateTime() {
		return this.updatedDateTime;
	}

	public void setUpdatedDateTime(String updatedDateTime) {
		this.updatedDateTime = updatedDateTime;
	}

	public int getUpdatedTsGmt() {
		return this.updatedTsGmt;
	}

	public void setUpdatedTsGmt(int updatedTsGmt) {
		this.updatedTsGmt = updatedTsGmt;
	}

}