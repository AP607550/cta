package com.ctastudio.dto;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the cta_available_forms database table.
 * 
 */
@Entity
@Table(name="cta_available_forms")
public class CtaAvailableForm implements Serializable, DomainObject {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="form_num")
	private int formNum;

	@Lob
	private String description;

	@Lob
	@Column(name="form_name")
	private String formName;

	@Column(name="media_recording")
	private int mediaRecording;

	@Lob
	@Column(name="prereq_form_list")
	private String prereqFormList;

	@Lob
	@Column(name="version_date_time")
	private String versionDateTime;

	@Lob
	@Column(name="version_number")
	private String versionNumber;

	public CtaAvailableForm() {
	}

	public int getFormNum() {
		return this.formNum;
	}

	public void setFormNum(int formNum) {
		this.formNum = formNum;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getFormName() {
		return this.formName;
	}

	public void setFormName(String formName) {
		this.formName = formName;
	}

	public int getMediaRecording() {
		return this.mediaRecording;
	}

	public void setMediaRecording(int mediaRecording) {
		this.mediaRecording = mediaRecording;
	}

	public String getPrereqFormList() {
		return this.prereqFormList;
	}

	public void setPrereqFormList(String prereqFormList) {
		this.prereqFormList = prereqFormList;
	}

	public String getVersionDateTime() {
		return this.versionDateTime;
	}

	public void setVersionDateTime(String versionDateTime) {
		this.versionDateTime = versionDateTime;
	}

	public String getVersionNumber() {
		return this.versionNumber;
	}

	public void setVersionNumber(String versionNumber) {
		this.versionNumber = versionNumber;
	}

}