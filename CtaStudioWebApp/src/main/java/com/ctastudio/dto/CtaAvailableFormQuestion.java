package com.ctastudio.dto;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the cta_available_form_question database table.
 * 
 */
@Entity
@Table(name="cta_available_form_question")
public class CtaAvailableFormQuestion implements Serializable, DomainObject {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private CtaAvailableFormQuestionPK id;

	@Lob
	@Column(name="ans_type")
	private String ansType;

	@Lob
	private String qlabel;

	@Lob
	private String qtext;

	@Lob
	@Column(name="version_date_time")
	private String versionDateTime;

	@Lob
	@Column(name="version_number")
	private String versionNumber;

	public CtaAvailableFormQuestion() {
	}

	public CtaAvailableFormQuestionPK getId() {
		return this.id;
	}

	public void setId(CtaAvailableFormQuestionPK id) {
		this.id = id;
	}

	public String getAnsType() {
		return this.ansType;
	}

	public void setAnsType(String ansType) {
		this.ansType = ansType;
	}

	public String getQlabel() {
		return this.qlabel;
	}

	public void setQlabel(String qlabel) {
		this.qlabel = qlabel;
	}

	public String getQtext() {
		return this.qtext;
	}

	public void setQtext(String qtext) {
		this.qtext = qtext;
	}

	public String getVersionDateTime() {
		return this.versionDateTime;
	}

	public void setVersionDateTime(String versionDateTime) {
		this.versionDateTime = versionDateTime;
	}

	public String getVersionNumber() {
		return this.versionNumber;
	}

	public void setVersionNumber(String versionNumber) {
		this.versionNumber = versionNumber;
	}

}