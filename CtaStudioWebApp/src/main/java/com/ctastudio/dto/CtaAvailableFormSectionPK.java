package com.ctastudio.dto;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the cta_available_form_section database table.
 * 
 */
@Embeddable
public class CtaAvailableFormSectionPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="form_num")
	private int formNum;

	@Column(name="form_section")
	private String formSection;

	public CtaAvailableFormSectionPK() {
	}
	public int getFormNum() {
		return this.formNum;
	}
	public void setFormNum(int formNum) {
		this.formNum = formNum;
	}
	public String getFormSection() {
		return this.formSection;
	}
	public void setFormSection(String formSection) {
		this.formSection = formSection;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof CtaAvailableFormSectionPK)) {
			return false;
		}
		CtaAvailableFormSectionPK castOther = (CtaAvailableFormSectionPK)other;
		return 
			(this.formNum == castOther.formNum)
			&& this.formSection.equals(castOther.formSection);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.formNum;
		hash = hash * prime + this.formSection.hashCode();
		
		return hash;
	}
}