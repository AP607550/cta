package com.ctastudio.dto;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the cta_form_data database table.
 * 
 */
@Entity
@Table(name="cta_form_data")
public class CtaFormData implements Serializable, DomainObject {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private CtaFormDataPK id;

	@Lob
	@Column(name="form_data_ans")
	private String formDataAns;

	@Lob
	@Column(name="form_data_note")
	private String formDataNote;

	@Lob
	@Column(name="media_file_name")
	private String mediaFileName;

	@Column(name="media_time_offset")
	private int mediaTimeOffset;

	@Lob
	@Column(name="review_status")
	private String reviewStatus;

	@Column(name="updated_by_client_uuid")
	private String updatedByClientUuid;

	@Column(name="updated_by_user")
	private String updatedByUser;

	@Lob
	@Column(name="updated_comment")
	private String updatedComment;

	@Lob
	@Column(name="updated_date_time")
	private String updatedDateTime;

	@Column(name="updated_gmt_offset")
	private int updatedGmtOffset;

	@Column(name="updated_ts_gmt")
	private int updatedTsGmt;

	@Lob
	@Column(name="version_date_time")
	private String versionDateTime;

	@Lob
	@Column(name="version_number")
	private String versionNumber;

	public CtaFormData() {
	}

	public CtaFormDataPK getId() {
		return this.id;
	}

	public void setId(CtaFormDataPK id) {
		this.id = id;
	}

	public String getFormDataAns() {
		return this.formDataAns;
	}

	public void setFormDataAns(String formDataAns) {
		this.formDataAns = formDataAns;
	}

	public String getFormDataNote() {
		return this.formDataNote;
	}

	public void setFormDataNote(String formDataNote) {
		this.formDataNote = formDataNote;
	}

	public String getMediaFileName() {
		return this.mediaFileName;
	}

	public void setMediaFileName(String mediaFileName) {
		this.mediaFileName = mediaFileName;
	}

	public int getMediaTimeOffset() {
		return this.mediaTimeOffset;
	}

	public void setMediaTimeOffset(int mediaTimeOffset) {
		this.mediaTimeOffset = mediaTimeOffset;
	}

	public String getReviewStatus() {
		return this.reviewStatus;
	}

	public void setReviewStatus(String reviewStatus) {
		this.reviewStatus = reviewStatus;
	}

	public String getUpdatedByClientUuid() {
		return this.updatedByClientUuid;
	}

	public void setUpdatedByClientUuid(String updatedByClientUuid) {
		this.updatedByClientUuid = updatedByClientUuid;
	}

	public String getUpdatedByUser() {
		return this.updatedByUser;
	}

	public void setUpdatedByUser(String updatedByUser) {
		this.updatedByUser = updatedByUser;
	}

	public String getUpdatedComment() {
		return this.updatedComment;
	}

	public void setUpdatedComment(String updatedComment) {
		this.updatedComment = updatedComment;
	}

	public String getUpdatedDateTime() {
		return this.updatedDateTime;
	}

	public void setUpdatedDateTime(String updatedDateTime) {
		this.updatedDateTime = updatedDateTime;
	}

	public int getUpdatedGmtOffset() {
		return this.updatedGmtOffset;
	}

	public void setUpdatedGmtOffset(int updatedGmtOffset) {
		this.updatedGmtOffset = updatedGmtOffset;
	}

	public int getUpdatedTsGmt() {
		return this.updatedTsGmt;
	}

	public void setUpdatedTsGmt(int updatedTsGmt) {
		this.updatedTsGmt = updatedTsGmt;
	}

	public String getVersionDateTime() {
		return this.versionDateTime;
	}

	public void setVersionDateTime(String versionDateTime) {
		this.versionDateTime = versionDateTime;
	}

	public String getVersionNumber() {
		return this.versionNumber;
	}

	public void setVersionNumber(String versionNumber) {
		this.versionNumber = versionNumber;
	}

}