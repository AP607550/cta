package com.ctastudio.dto;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the cta_form_data database table.
 * 
 */
@Embeddable
public class CtaFormDataPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="subject_uuid")
	private String subjectUuid;

	@Column(name="subject_visit_num")
	private int subjectVisitNum;

	@Column(name="form_num")
	private int formNum;

	@Column(name="form_section")
	private String formSection;

	@Column(name="form_qnum")
	private int formQnum;

	public CtaFormDataPK() {
	}
	public String getSubjectUuid() {
		return this.subjectUuid;
	}
	public void setSubjectUuid(String subjectUuid) {
		this.subjectUuid = subjectUuid;
	}
	public int getSubjectVisitNum() {
		return this.subjectVisitNum;
	}
	public void setSubjectVisitNum(int subjectVisitNum) {
		this.subjectVisitNum = subjectVisitNum;
	}
	public int getFormNum() {
		return this.formNum;
	}
	public void setFormNum(int formNum) {
		this.formNum = formNum;
	}
	public String getFormSection() {
		return this.formSection;
	}
	public void setFormSection(String formSection) {
		this.formSection = formSection;
	}
	public int getFormQnum() {
		return this.formQnum;
	}
	public void setFormQnum(int formQnum) {
		this.formQnum = formQnum;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof CtaFormDataPK)) {
			return false;
		}
		CtaFormDataPK castOther = (CtaFormDataPK)other;
		return 
			this.subjectUuid.equals(castOther.subjectUuid)
			&& (this.subjectVisitNum == castOther.subjectVisitNum)
			&& (this.formNum == castOther.formNum)
			&& this.formSection.equals(castOther.formSection)
			&& (this.formQnum == castOther.formQnum);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.subjectUuid.hashCode();
		hash = hash * prime + this.subjectVisitNum;
		hash = hash * prime + this.formNum;
		hash = hash * prime + this.formSection.hashCode();
		hash = hash * prime + this.formQnum;
		
		return hash;
	}
}