package com.ctastudio.dto;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the cta_form_data_query database table.
 * 
 */
@Entity
@Table(name="cta_form_data_query")
public class CtaFormDataQuery implements Serializable, DomainObject {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private CtaFormDataQueryPK id;

	@Lob
	@Column(name="form_query")
	private String formQuery;

	@Lob
	@Column(name="form_reply")
	private String formReply;

	@Lob
	@Column(name="query_client_uuid")
	private String queryClientUuid;

	@Lob
	@Column(name="query_date_time")
	private String queryDateTime;

	@Lob
	@Column(name="reply_by_user")
	private String replyByUser;

	@Lob
	@Column(name="reply_client_uuid")
	private String replyClientUuid;

	@Lob
	@Column(name="reply_date_time")
	private String replyDateTime;

	@Column(name="reply_ts_gmt")
	private int replyTsGmt;

	@Lob
	@Column(name="version_date_time")
	private String versionDateTime;

	@Lob
	@Column(name="version_number")
	private String versionNumber;

	public CtaFormDataQuery() {
	}

	public CtaFormDataQueryPK getId() {
		return this.id;
	}

	public void setId(CtaFormDataQueryPK id) {
		this.id = id;
	}

	public String getFormQuery() {
		return this.formQuery;
	}

	public void setFormQuery(String formQuery) {
		this.formQuery = formQuery;
	}

	public String getFormReply() {
		return this.formReply;
	}

	public void setFormReply(String formReply) {
		this.formReply = formReply;
	}

	public String getQueryClientUuid() {
		return this.queryClientUuid;
	}

	public void setQueryClientUuid(String queryClientUuid) {
		this.queryClientUuid = queryClientUuid;
	}

	public String getQueryDateTime() {
		return this.queryDateTime;
	}

	public void setQueryDateTime(String queryDateTime) {
		this.queryDateTime = queryDateTime;
	}

	public String getReplyByUser() {
		return this.replyByUser;
	}

	public void setReplyByUser(String replyByUser) {
		this.replyByUser = replyByUser;
	}

	public String getReplyClientUuid() {
		return this.replyClientUuid;
	}

	public void setReplyClientUuid(String replyClientUuid) {
		this.replyClientUuid = replyClientUuid;
	}

	public String getReplyDateTime() {
		return this.replyDateTime;
	}

	public void setReplyDateTime(String replyDateTime) {
		this.replyDateTime = replyDateTime;
	}

	public int getReplyTsGmt() {
		return this.replyTsGmt;
	}

	public void setReplyTsGmt(int replyTsGmt) {
		this.replyTsGmt = replyTsGmt;
	}

	public String getVersionDateTime() {
		return this.versionDateTime;
	}

	public void setVersionDateTime(String versionDateTime) {
		this.versionDateTime = versionDateTime;
	}

	public String getVersionNumber() {
		return this.versionNumber;
	}

	public void setVersionNumber(String versionNumber) {
		this.versionNumber = versionNumber;
	}

}