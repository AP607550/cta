package com.ctastudio.dto;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the cta_form_data_review database table.
 * 
 */
@Entity
@Table(name="cta_form_data_review")
public class CtaFormDataReview implements Serializable, DomainObject {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private CtaFormDataReviewPK id;

	@Lob
	@Column(name="form_data_ans")
	private String formDataAns;

	@Lob
	@Column(name="form_data_note")
	private String formDataNote;

	@Lob
	@Column(name="review_status")
	private String reviewStatus;

	@Lob
	@Column(name="version_date_time")
	private String versionDateTime;

	@Lob
	@Column(name="version_number")
	private String versionNumber;

	public CtaFormDataReview() {
	}

	public CtaFormDataReviewPK getId() {
		return this.id;
	}

	public void setId(CtaFormDataReviewPK id) {
		this.id = id;
	}

	public String getFormDataAns() {
		return this.formDataAns;
	}

	public void setFormDataAns(String formDataAns) {
		this.formDataAns = formDataAns;
	}

	public String getFormDataNote() {
		return this.formDataNote;
	}

	public void setFormDataNote(String formDataNote) {
		this.formDataNote = formDataNote;
	}

	public String getReviewStatus() {
		return this.reviewStatus;
	}

	public void setReviewStatus(String reviewStatus) {
		this.reviewStatus = reviewStatus;
	}

	public String getVersionDateTime() {
		return this.versionDateTime;
	}

	public void setVersionDateTime(String versionDateTime) {
		this.versionDateTime = versionDateTime;
	}

	public String getVersionNumber() {
		return this.versionNumber;
	}

	public void setVersionNumber(String versionNumber) {
		this.versionNumber = versionNumber;
	}

}