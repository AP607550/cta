package com.ctastudio.dto;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the cta_form_media database table.
 * 
 */
@Entity
@Table(name="cta_form_media")
public class CtaFormMedia implements Serializable, DomainObject {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private CtaFormMediaPK id;

	@Column(name="end_ts_gmt")
	private int endTsGmt;

	@Lob
	@Column(name="media_file_name")
	private String mediaFileName;

	@Column(name="start_ts_gmt")
	private int startTsGmt;

	@Column(name="updated_by_client_uuid")
	private String updatedByClientUuid;

	@Column(name="updated_by_user")
	private String updatedByUser;

	@Lob
	@Column(name="updated_date_time")
	private String updatedDateTime;

	@Lob
	@Column(name="version_date_time")
	private String versionDateTime;

	@Lob
	@Column(name="version_number")
	private String versionNumber;

	public CtaFormMedia() {
	}

	public CtaFormMediaPK getId() {
		return this.id;
	}

	public void setId(CtaFormMediaPK id) {
		this.id = id;
	}

	public int getEndTsGmt() {
		return this.endTsGmt;
	}

	public void setEndTsGmt(int endTsGmt) {
		this.endTsGmt = endTsGmt;
	}

	public String getMediaFileName() {
		return this.mediaFileName;
	}

	public void setMediaFileName(String mediaFileName) {
		this.mediaFileName = mediaFileName;
	}

	public int getStartTsGmt() {
		return this.startTsGmt;
	}

	public void setStartTsGmt(int startTsGmt) {
		this.startTsGmt = startTsGmt;
	}

	public String getUpdatedByClientUuid() {
		return this.updatedByClientUuid;
	}

	public void setUpdatedByClientUuid(String updatedByClientUuid) {
		this.updatedByClientUuid = updatedByClientUuid;
	}

	public String getUpdatedByUser() {
		return this.updatedByUser;
	}

	public void setUpdatedByUser(String updatedByUser) {
		this.updatedByUser = updatedByUser;
	}

	public String getUpdatedDateTime() {
		return this.updatedDateTime;
	}

	public void setUpdatedDateTime(String updatedDateTime) {
		this.updatedDateTime = updatedDateTime;
	}

	public String getVersionDateTime() {
		return this.versionDateTime;
	}

	public void setVersionDateTime(String versionDateTime) {
		this.versionDateTime = versionDateTime;
	}

	public String getVersionNumber() {
		return this.versionNumber;
	}

	public void setVersionNumber(String versionNumber) {
		this.versionNumber = versionNumber;
	}

}