package com.ctastudio.dto;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the cta_form_media database table.
 * 
 */
@Embeddable
public class CtaFormMediaPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="subject_uuid")
	private String subjectUuid;

	@Column(name="subject_visit_num")
	private int subjectVisitNum;

	@Column(name="form_num")
	private int formNum;

	@Column(name="media_file_num")
	private int mediaFileNum;

	public CtaFormMediaPK() {
	}
	public String getSubjectUuid() {
		return this.subjectUuid;
	}
	public void setSubjectUuid(String subjectUuid) {
		this.subjectUuid = subjectUuid;
	}
	public int getSubjectVisitNum() {
		return this.subjectVisitNum;
	}
	public void setSubjectVisitNum(int subjectVisitNum) {
		this.subjectVisitNum = subjectVisitNum;
	}
	public int getFormNum() {
		return this.formNum;
	}
	public void setFormNum(int formNum) {
		this.formNum = formNum;
	}
	public int getMediaFileNum() {
		return this.mediaFileNum;
	}
	public void setMediaFileNum(int mediaFileNum) {
		this.mediaFileNum = mediaFileNum;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof CtaFormMediaPK)) {
			return false;
		}
		CtaFormMediaPK castOther = (CtaFormMediaPK)other;
		return 
			this.subjectUuid.equals(castOther.subjectUuid)
			&& (this.subjectVisitNum == castOther.subjectVisitNum)
			&& (this.formNum == castOther.formNum)
			&& (this.mediaFileNum == castOther.mediaFileNum);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.subjectUuid.hashCode();
		hash = hash * prime + this.subjectVisitNum;
		hash = hash * prime + this.formNum;
		hash = hash * prime + this.mediaFileNum;
		
		return hash;
	}
}