package com.ctastudio.dto;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the cta_form_status database table.
 * 
 */
@Entity
@Table(name="cta_form_status")
public class CtaFormStatus implements Serializable, DomainObject {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="form_status_num")
	private int formStatusNum;

	@Lob
	private String description;

	@Lob
	@Column(name="form_status_name")
	private String formStatusName;

	@Lob
	@Column(name="version_date_time")
	private String versionDateTime;

	@Lob
	@Column(name="version_number")
	private String versionNumber;

	public CtaFormStatus() {
	}

	public int getFormStatusNum() {
		return this.formStatusNum;
	}

	public void setFormStatusNum(int formStatusNum) {
		this.formStatusNum = formStatusNum;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getFormStatusName() {
		return this.formStatusName;
	}

	public void setFormStatusName(String formStatusName) {
		this.formStatusName = formStatusName;
	}

	public String getVersionDateTime() {
		return this.versionDateTime;
	}

	public void setVersionDateTime(String versionDateTime) {
		this.versionDateTime = versionDateTime;
	}

	public String getVersionNumber() {
		return this.versionNumber;
	}

	public void setVersionNumber(String versionNumber) {
		this.versionNumber = versionNumber;
	}

}