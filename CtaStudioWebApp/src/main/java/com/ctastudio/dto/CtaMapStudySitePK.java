package com.ctastudio.dto;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the cta_map_study_site database table.
 * 
 */
@Embeddable
public class CtaMapStudySitePK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="study_num")
	private int studyNum;

	@Column(name="site_num")
	private int siteNum;

	public CtaMapStudySitePK() {
	}
	public int getStudyNum() {
		return this.studyNum;
	}
	public void setStudyNum(int studyNum) {
		this.studyNum = studyNum;
	}
	public int getSiteNum() {
		return this.siteNum;
	}
	public void setSiteNum(int siteNum) {
		this.siteNum = siteNum;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof CtaMapStudySitePK)) {
			return false;
		}
		CtaMapStudySitePK castOther = (CtaMapStudySitePK)other;
		return 
			(this.studyNum == castOther.studyNum)
			&& (this.siteNum == castOther.siteNum);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.studyNum;
		hash = hash * prime + this.siteNum;
		
		return hash;
	}
}