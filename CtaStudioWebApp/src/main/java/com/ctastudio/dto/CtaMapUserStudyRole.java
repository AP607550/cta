package com.ctastudio.dto;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the cta_map_user_study_role database table.
 * 
 */
@Entity
@Table(name="cta_map_user_study_role")
public class CtaMapUserStudyRole implements Serializable, DomainObject {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private CtaMapUserStudyRolePK id;

	@Column(name="updated_by_client_uuid")
	private String updatedByClientUuid;

	@Column(name="updated_by_user")
	private String updatedByUser;

	@Lob
	@Column(name="updated_date_time")
	private String updatedDateTime;

	@Column(name="updated_gmt_offset")
	private int updatedGmtOffset;

	@Column(name="updated_ts_gmt")
	private int updatedTsGmt;

	@Lob
	@Column(name="version_date_time")
	private String versionDateTime;

	@Lob
	@Column(name="version_number")
	private String versionNumber;

	public CtaMapUserStudyRole() {
	}

	public CtaMapUserStudyRolePK getId() {
		return this.id;
	}

	public void setId(CtaMapUserStudyRolePK id) {
		this.id = id;
	}

	public String getUpdatedByClientUuid() {
		return this.updatedByClientUuid;
	}

	public void setUpdatedByClientUuid(String updatedByClientUuid) {
		this.updatedByClientUuid = updatedByClientUuid;
	}

	public String getUpdatedByUser() {
		return this.updatedByUser;
	}

	public void setUpdatedByUser(String updatedByUser) {
		this.updatedByUser = updatedByUser;
	}

	public String getUpdatedDateTime() {
		return this.updatedDateTime;
	}

	public void setUpdatedDateTime(String updatedDateTime) {
		this.updatedDateTime = updatedDateTime;
	}

	public int getUpdatedGmtOffset() {
		return this.updatedGmtOffset;
	}

	public void setUpdatedGmtOffset(int updatedGmtOffset) {
		this.updatedGmtOffset = updatedGmtOffset;
	}

	public int getUpdatedTsGmt() {
		return this.updatedTsGmt;
	}

	public void setUpdatedTsGmt(int updatedTsGmt) {
		this.updatedTsGmt = updatedTsGmt;
	}

	public String getVersionDateTime() {
		return this.versionDateTime;
	}

	public void setVersionDateTime(String versionDateTime) {
		this.versionDateTime = versionDateTime;
	}

	public String getVersionNumber() {
		return this.versionNumber;
	}

	public void setVersionNumber(String versionNumber) {
		this.versionNumber = versionNumber;
	}

}