package com.ctastudio.dto;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the cta_map_user_study_role database table.
 * 
 */
@Embeddable
public class CtaMapUserStudyRolePK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="user_code")
	private String userCode;

	@Column(name="study_num")
	private int studyNum;

	@Column(name="role_num")
	private int roleNum;

	public CtaMapUserStudyRolePK() {
	}
	public String getUserCode() {
		return this.userCode;
	}
	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}
	public int getStudyNum() {
		return this.studyNum;
	}
	public void setStudyNum(int studyNum) {
		this.studyNum = studyNum;
	}
	public int getRoleNum() {
		return this.roleNum;
	}
	public void setRoleNum(int roleNum) {
		this.roleNum = roleNum;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof CtaMapUserStudyRolePK)) {
			return false;
		}
		CtaMapUserStudyRolePK castOther = (CtaMapUserStudyRolePK)other;
		return 
			this.userCode.equals(castOther.userCode)
			&& (this.studyNum == castOther.studyNum)
			&& (this.roleNum == castOther.roleNum);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.userCode.hashCode();
		hash = hash * prime + this.studyNum;
		hash = hash * prime + this.roleNum;
		
		return hash;
	}
}