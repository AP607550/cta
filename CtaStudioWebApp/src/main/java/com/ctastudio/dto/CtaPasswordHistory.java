package com.ctastudio.dto;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the cta_password_history database table.
 * 
 */
@Entity
@Table(name="cta_password_history")
public class CtaPasswordHistory implements Serializable, DomainObject {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private CtaPasswordHistoryPK id;

	@Column(name="autogen_flag")
	private int autogenFlag;

	@Lob
	@Column(name="autogen_pass")
	private String autogenPass;

	@Lob
	@Column(name="password_digest")
	private String passwordDigest;

	@Column(name="updated_by_user")
	private String updatedByUser;

	@Lob
	@Column(name="updated_date_time")
	private String updatedDateTime;

	@Column(name="updated_gmt_offset")
	private int updatedGmtOffset;

	@Lob
	@Column(name="version_date_time")
	private String versionDateTime;

	@Lob
	@Column(name="version_number")
	private String versionNumber;

	public CtaPasswordHistory() {
	}

	public CtaPasswordHistoryPK getId() {
		return this.id;
	}

	public void setId(CtaPasswordHistoryPK id) {
		this.id = id;
	}

	public int getAutogenFlag() {
		return this.autogenFlag;
	}

	public void setAutogenFlag(int autogenFlag) {
		this.autogenFlag = autogenFlag;
	}

	public String getAutogenPass() {
		return this.autogenPass;
	}

	public void setAutogenPass(String autogenPass) {
		this.autogenPass = autogenPass;
	}

	public String getPasswordDigest() {
		return this.passwordDigest;
	}

	public void setPasswordDigest(String passwordDigest) {
		this.passwordDigest = passwordDigest;
	}

	public String getUpdatedByUser() {
		return this.updatedByUser;
	}

	public void setUpdatedByUser(String updatedByUser) {
		this.updatedByUser = updatedByUser;
	}

	public String getUpdatedDateTime() {
		return this.updatedDateTime;
	}

	public void setUpdatedDateTime(String updatedDateTime) {
		this.updatedDateTime = updatedDateTime;
	}

	public int getUpdatedGmtOffset() {
		return this.updatedGmtOffset;
	}

	public void setUpdatedGmtOffset(int updatedGmtOffset) {
		this.updatedGmtOffset = updatedGmtOffset;
	}

	public String getVersionDateTime() {
		return this.versionDateTime;
	}

	public void setVersionDateTime(String versionDateTime) {
		this.versionDateTime = versionDateTime;
	}

	public String getVersionNumber() {
		return this.versionNumber;
	}

	public void setVersionNumber(String versionNumber) {
		this.versionNumber = versionNumber;
	}

}