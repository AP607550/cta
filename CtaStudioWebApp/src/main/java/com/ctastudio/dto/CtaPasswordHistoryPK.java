package com.ctastudio.dto;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the cta_password_history database table.
 * 
 */
@Embeddable
public class CtaPasswordHistoryPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="user_code")
	private String userCode;

	@Column(name="updated_by_client_uuid")
	private String updatedByClientUuid;

	@Column(name="updated_ts_gmt")
	private int updatedTsGmt;

	public CtaPasswordHistoryPK() {
	}
	public String getUserCode() {
		return this.userCode;
	}
	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}
	public String getUpdatedByClientUuid() {
		return this.updatedByClientUuid;
	}
	public void setUpdatedByClientUuid(String updatedByClientUuid) {
		this.updatedByClientUuid = updatedByClientUuid;
	}
	public int getUpdatedTsGmt() {
		return this.updatedTsGmt;
	}
	public void setUpdatedTsGmt(int updatedTsGmt) {
		this.updatedTsGmt = updatedTsGmt;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof CtaPasswordHistoryPK)) {
			return false;
		}
		CtaPasswordHistoryPK castOther = (CtaPasswordHistoryPK)other;
		return 
			this.userCode.equals(castOther.userCode)
			&& this.updatedByClientUuid.equals(castOther.updatedByClientUuid)
			&& (this.updatedTsGmt == castOther.updatedTsGmt);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.userCode.hashCode();
		hash = hash * prime + this.updatedByClientUuid.hashCode();
		hash = hash * prime + this.updatedTsGmt;
		
		return hash;
	}
}