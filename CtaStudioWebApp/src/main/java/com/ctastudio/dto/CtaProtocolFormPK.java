package com.ctastudio.dto;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the cta_protocol_form database table.
 * 
 */
@Embeddable
public class CtaProtocolFormPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="protocol_num")
	private int protocolNum;

	@Column(name="protocol_visit_num")
	private int protocolVisitNum;

	@Column(name="form_num")
	private int formNum;

	public CtaProtocolFormPK() {
	}
	public int getProtocolNum() {
		return this.protocolNum;
	}
	public void setProtocolNum(int protocolNum) {
		this.protocolNum = protocolNum;
	}
	public int getProtocolVisitNum() {
		return this.protocolVisitNum;
	}
	public void setProtocolVisitNum(int protocolVisitNum) {
		this.protocolVisitNum = protocolVisitNum;
	}
	public int getFormNum() {
		return this.formNum;
	}
	public void setFormNum(int formNum) {
		this.formNum = formNum;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof CtaProtocolFormPK)) {
			return false;
		}
		CtaProtocolFormPK castOther = (CtaProtocolFormPK)other;
		return 
			(this.protocolNum == castOther.protocolNum)
			&& (this.protocolVisitNum == castOther.protocolVisitNum)
			&& (this.formNum == castOther.formNum);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.protocolNum;
		hash = hash * prime + this.protocolVisitNum;
		hash = hash * prime + this.formNum;
		
		return hash;
	}
}