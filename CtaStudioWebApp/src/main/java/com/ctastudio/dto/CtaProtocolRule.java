package com.ctastudio.dto;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the cta_protocol_rule database table.
 * 
 */
@Entity
@Table(name="cta_protocol_rule")
public class CtaProtocolRule implements Serializable, DomainObject {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private CtaProtocolRulePK id;

	@Column(name="created_by_user")
	private String createdByUser;

	@Lob
	@Column(name="protocol_rule")
	private String protocolRule;

	@Lob
	@Column(name="protocol_rule_desc")
	private String protocolRuleDesc;

	@Lob
	@Column(name="protocol_rule_value")
	private String protocolRuleValue;

	@Column(name="updated_by_client_uuid")
	private String updatedByClientUuid;

	@Column(name="updated_by_user")
	private String updatedByUser;

	@Lob
	@Column(name="updated_comment")
	private String updatedComment;

	@Lob
	@Column(name="updated_date_time")
	private String updatedDateTime;

	@Column(name="updated_gmt_offset")
	private int updatedGmtOffset;

	@Column(name="updated_ts_gmt")
	private int updatedTsGmt;

	@Lob
	@Column(name="version_date_time")
	private String versionDateTime;

	@Lob
	@Column(name="version_number")
	private String versionNumber;

	public CtaProtocolRule() {
	}

	public CtaProtocolRulePK getId() {
		return this.id;
	}

	public void setId(CtaProtocolRulePK id) {
		this.id = id;
	}

	public String getCreatedByUser() {
		return this.createdByUser;
	}

	public void setCreatedByUser(String createdByUser) {
		this.createdByUser = createdByUser;
	}

	public String getProtocolRule() {
		return this.protocolRule;
	}

	public void setProtocolRule(String protocolRule) {
		this.protocolRule = protocolRule;
	}

	public String getProtocolRuleDesc() {
		return this.protocolRuleDesc;
	}

	public void setProtocolRuleDesc(String protocolRuleDesc) {
		this.protocolRuleDesc = protocolRuleDesc;
	}

	public String getProtocolRuleValue() {
		return this.protocolRuleValue;
	}

	public void setProtocolRuleValue(String protocolRuleValue) {
		this.protocolRuleValue = protocolRuleValue;
	}

	public String getUpdatedByClientUuid() {
		return this.updatedByClientUuid;
	}

	public void setUpdatedByClientUuid(String updatedByClientUuid) {
		this.updatedByClientUuid = updatedByClientUuid;
	}

	public String getUpdatedByUser() {
		return this.updatedByUser;
	}

	public void setUpdatedByUser(String updatedByUser) {
		this.updatedByUser = updatedByUser;
	}

	public String getUpdatedComment() {
		return this.updatedComment;
	}

	public void setUpdatedComment(String updatedComment) {
		this.updatedComment = updatedComment;
	}

	public String getUpdatedDateTime() {
		return this.updatedDateTime;
	}

	public void setUpdatedDateTime(String updatedDateTime) {
		this.updatedDateTime = updatedDateTime;
	}

	public int getUpdatedGmtOffset() {
		return this.updatedGmtOffset;
	}

	public void setUpdatedGmtOffset(int updatedGmtOffset) {
		this.updatedGmtOffset = updatedGmtOffset;
	}

	public int getUpdatedTsGmt() {
		return this.updatedTsGmt;
	}

	public void setUpdatedTsGmt(int updatedTsGmt) {
		this.updatedTsGmt = updatedTsGmt;
	}

	public String getVersionDateTime() {
		return this.versionDateTime;
	}

	public void setVersionDateTime(String versionDateTime) {
		this.versionDateTime = versionDateTime;
	}

	public String getVersionNumber() {
		return this.versionNumber;
	}

	public void setVersionNumber(String versionNumber) {
		this.versionNumber = versionNumber;
	}

}