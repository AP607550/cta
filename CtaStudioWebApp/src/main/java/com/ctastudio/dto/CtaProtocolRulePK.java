package com.ctastudio.dto;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the cta_protocol_rule database table.
 * 
 */
@Embeddable
public class CtaProtocolRulePK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="protocol_rule_num")
	private int protocolRuleNum;

	@Column(name="protocol_num")
	private int protocolNum;

	public CtaProtocolRulePK() {
	}
	public int getProtocolRuleNum() {
		return this.protocolRuleNum;
	}
	public void setProtocolRuleNum(int protocolRuleNum) {
		this.protocolRuleNum = protocolRuleNum;
	}
	public int getProtocolNum() {
		return this.protocolNum;
	}
	public void setProtocolNum(int protocolNum) {
		this.protocolNum = protocolNum;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof CtaProtocolRulePK)) {
			return false;
		}
		CtaProtocolRulePK castOther = (CtaProtocolRulePK)other;
		return 
			(this.protocolRuleNum == castOther.protocolRuleNum)
			&& (this.protocolNum == castOther.protocolNum);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.protocolRuleNum;
		hash = hash * prime + this.protocolNum;
		
		return hash;
	}
}