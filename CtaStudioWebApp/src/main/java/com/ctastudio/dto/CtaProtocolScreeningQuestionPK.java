package com.ctastudio.dto;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the cta_protocol_screening_question database table.
 * 
 */
@Embeddable
public class CtaProtocolScreeningQuestionPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="protocol_screening_qnum")
	private int protocolScreeningQnum;

	@Column(name="protocol_num")
	private int protocolNum;

	public CtaProtocolScreeningQuestionPK() {
	}
	public int getProtocolScreeningQnum() {
		return this.protocolScreeningQnum;
	}
	public void setProtocolScreeningQnum(int protocolScreeningQnum) {
		this.protocolScreeningQnum = protocolScreeningQnum;
	}
	public int getProtocolNum() {
		return this.protocolNum;
	}
	public void setProtocolNum(int protocolNum) {
		this.protocolNum = protocolNum;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof CtaProtocolScreeningQuestionPK)) {
			return false;
		}
		CtaProtocolScreeningQuestionPK castOther = (CtaProtocolScreeningQuestionPK)other;
		return 
			(this.protocolScreeningQnum == castOther.protocolScreeningQnum)
			&& (this.protocolNum == castOther.protocolNum);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.protocolScreeningQnum;
		hash = hash * prime + this.protocolNum;
		
		return hash;
	}
}