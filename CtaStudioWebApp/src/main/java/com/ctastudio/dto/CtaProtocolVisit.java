package com.ctastudio.dto;

import java.io.Serializable;

import javax.persistence.*;


/**
 * The persistent class for the cta_protocol_visit database table.
 * 
 */
//@IdClass(CtaProtocolVisitPK.class)
@Entity
@Table(name="cta_protocol_visit")
public class CtaProtocolVisit implements Serializable, DomainObject {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private CtaProtocolVisitPK id;

	/*@Id
    @Column(name = "protocol_num")
	protected int protocolNum;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "protocol_visit_num")
	protected int protocolVisitNum;*/
	
	@Lob
	private String description;

	@Column(name="time_after_ref")
	private int timeAfterRef;

	@Column(name="time_minus_variance")
	private int timeMinusVariance;

	@Column(name="time_plus_variance")
	private int timePlusVariance;

	@Column(name="time_ref_visit_num")
	private int timeRefVisitNum;

	@Lob
	@Column(name="time_unit")
	private String timeUnit;

	@Column(name="updated_by_client_uuid")
	private String updatedByClientUuid;

	@Column(name="updated_by_user")
	private String updatedByUser;

	@Lob
	@Column(name="updated_date_time")
	private String updatedDateTime;

	@Column(name="updated_gmt_offset")
	private int updatedGmtOffset;

	@Column(name="updated_ts_gmt")
	private int updatedTsGmt;

	@Lob
	@Column(name="version_date_time")
	private String versionDateTime;

	@Lob
	@Column(name="version_number")
	private String versionNumber;

	@Lob
	@Column(name="visit_name")
	private String visitName;

	@Column(name="visit_type")
	private int visitType;

	public CtaProtocolVisit() {
	}

	public CtaProtocolVisitPK getId() {
		return this.id;
	}

	public void setId(CtaProtocolVisitPK id) {
		this.id = id;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getTimeAfterRef() {
		return this.timeAfterRef;
	}

	public void setTimeAfterRef(int timeAfterRef) {
		this.timeAfterRef = timeAfterRef;
	}

	public int getTimeMinusVariance() {
		return this.timeMinusVariance;
	}

	public void setTimeMinusVariance(int timeMinusVariance) {
		this.timeMinusVariance = timeMinusVariance;
	}

	public int getTimePlusVariance() {
		return this.timePlusVariance;
	}

	public void setTimePlusVariance(int timePlusVariance) {
		this.timePlusVariance = timePlusVariance;
	}

	public int getTimeRefVisitNum() {
		return this.timeRefVisitNum;
	}

	public void setTimeRefVisitNum(int timeRefVisitNum) {
		this.timeRefVisitNum = timeRefVisitNum;
	}

	public String getTimeUnit() {
		return this.timeUnit;
	}

	public void setTimeUnit(String timeUnit) {
		this.timeUnit = timeUnit;
	}

	public String getUpdatedByClientUuid() {
		return this.updatedByClientUuid;
	}

	public void setUpdatedByClientUuid(String updatedByClientUuid) {
		this.updatedByClientUuid = updatedByClientUuid;
	}

	public String getUpdatedByUser() {
		return this.updatedByUser;
	}

	public void setUpdatedByUser(String updatedByUser) {
		this.updatedByUser = updatedByUser;
	}

	public String getUpdatedDateTime() {
		return this.updatedDateTime;
	}

	public void setUpdatedDateTime(String updatedDateTime) {
		this.updatedDateTime = updatedDateTime;
	}

	public int getUpdatedGmtOffset() {
		return this.updatedGmtOffset;
	}

	public void setUpdatedGmtOffset(int updatedGmtOffset) {
		this.updatedGmtOffset = updatedGmtOffset;
	}

	public int getUpdatedTsGmt() {
		return this.updatedTsGmt;
	}

	public void setUpdatedTsGmt(int updatedTsGmt) {
		this.updatedTsGmt = updatedTsGmt;
	}

	public String getVersionDateTime() {
		return this.versionDateTime;
	}

	public void setVersionDateTime(String versionDateTime) {
		this.versionDateTime = versionDateTime;
	}

	public String getVersionNumber() {
		return this.versionNumber;
	}

	public void setVersionNumber(String versionNumber) {
		this.versionNumber = versionNumber;
	}

	public String getVisitName() {
		return this.visitName;
	}

	public void setVisitName(String visitName) {
		this.visitName = visitName;
	}

	public int getVisitType() {
		return this.visitType;
	}

	public void setVisitType(int visitType) {
		this.visitType = visitType;
	}

	/*public int getProtocolNum() {
		return protocolNum;
	}

	public void setProtocolNum(int protocolNum) {
		this.protocolNum = protocolNum;
	}

	public int getProtocolVisitNum() {
		return protocolVisitNum;
	}

	public void setProtocolVisitNum(int protocolVisitNum) {
		this.protocolVisitNum = protocolVisitNum;
	}*/

}