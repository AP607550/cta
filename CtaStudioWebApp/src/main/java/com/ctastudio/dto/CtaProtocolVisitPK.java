package com.ctastudio.dto;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the cta_protocol_visit database table.
 * 
 */
@Embeddable
public class CtaProtocolVisitPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="protocol_num")
	private int protocolNum;

	@Column(name="protocol_visit_num")
	private int protocolVisitNum;

	public CtaProtocolVisitPK() {
	}
	public int getProtocolNum() {
		return this.protocolNum;
	}
	public void setProtocolNum(int protocolNum) {
		this.protocolNum = protocolNum;
	}
	public int getProtocolVisitNum() {
		return this.protocolVisitNum;
	}
	public void setProtocolVisitNum(int protocolVisitNum) {
		this.protocolVisitNum = protocolVisitNum;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof CtaProtocolVisitPK)) {
			return false;
		}
		CtaProtocolVisitPK castOther = (CtaProtocolVisitPK)other;
		return 
			(this.protocolNum == castOther.protocolNum)
			&& (this.protocolVisitNum == castOther.protocolVisitNum);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.protocolNum;
		hash = hash * prime + this.protocolVisitNum;
		
		return hash;
	}
	public CtaProtocolVisitPK(int protocolNum, int protocolVisitNum) {
		this.protocolNum = protocolNum;
		this.protocolVisitNum = protocolVisitNum;
	}
	
	
}