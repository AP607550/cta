package com.ctastudio.dto;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the cta_role database table.
 * 
 */
@Entity
@Table(name="cta_role")
public class CtaRole implements Serializable, DomainObject {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="role_num")
	private int roleNum;

	@Lob
	private String description;

	@Lob
	@Column(name="profile_num")
	private String profileNum;

	@Lob
	@Column(name="role_name")
	private String roleName;

	@Column(name="updated_by_client_uuid")
	private String updatedByClientUuid;

	@Column(name="updated_by_user")
	private String updatedByUser;

	@Lob
	@Column(name="updated_date_time")
	private String updatedDateTime;

	@Column(name="updated_gmt_offset")
	private int updatedGmtOffset;

	@Column(name="updated_ts_gmt")
	private int updatedTsGmt;

	@Lob
	@Column(name="version_date_time")
	private String versionDateTime;

	@Lob
	@Column(name="version_number")
	private String versionNumber;

	//bi-directional many-to-one association to ScreenRole
	@OneToMany(mappedBy="ctaRole")
	private List<ScreenRole> screenRoles;

	public CtaRole() {
	}

	public int getRoleNum() {
		return this.roleNum;
	}

	public void setRoleNum(int roleNum) {
		this.roleNum = roleNum;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getProfileNum() {
		return this.profileNum;
	}

	public void setProfileNum(String profileNum) {
		this.profileNum = profileNum;
	}

	public String getRoleName() {
		return this.roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public String getUpdatedByClientUuid() {
		return this.updatedByClientUuid;
	}

	public void setUpdatedByClientUuid(String updatedByClientUuid) {
		this.updatedByClientUuid = updatedByClientUuid;
	}

	public String getUpdatedByUser() {
		return this.updatedByUser;
	}

	public void setUpdatedByUser(String updatedByUser) {
		this.updatedByUser = updatedByUser;
	}

	public String getUpdatedDateTime() {
		return this.updatedDateTime;
	}

	public void setUpdatedDateTime(String updatedDateTime) {
		this.updatedDateTime = updatedDateTime;
	}

	public int getUpdatedGmtOffset() {
		return this.updatedGmtOffset;
	}

	public void setUpdatedGmtOffset(int updatedGmtOffset) {
		this.updatedGmtOffset = updatedGmtOffset;
	}

	public int getUpdatedTsGmt() {
		return this.updatedTsGmt;
	}

	public void setUpdatedTsGmt(int updatedTsGmt) {
		this.updatedTsGmt = updatedTsGmt;
	}

	public String getVersionDateTime() {
		return this.versionDateTime;
	}

	public void setVersionDateTime(String versionDateTime) {
		this.versionDateTime = versionDateTime;
	}

	public String getVersionNumber() {
		return this.versionNumber;
	}

	public void setVersionNumber(String versionNumber) {
		this.versionNumber = versionNumber;
	}

	public List<ScreenRole> getScreenRoles() {
		return this.screenRoles;
	}

	public void setScreenRoles(List<ScreenRole> screenRoles) {
		this.screenRoles = screenRoles;
	}

	public ScreenRole addScreenRole(ScreenRole screenRole) {
		getScreenRoles().add(screenRole);
		screenRole.setCtaRole(this);

		return screenRole;
	}

	public ScreenRole removeScreenRole(ScreenRole screenRole) {
		getScreenRoles().remove(screenRole);
		screenRole.setCtaRole(null);

		return screenRole;
	}

}