package com.ctastudio.dto;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the cta_study_status database table.
 * 
 */
@Entity
@Table(name="cta_study_status")
public class CtaStudyStatus implements Serializable, DomainObject {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="study_status_num")
	private int studyStatusNum;

	@Lob
	private String description;

	@Lob
	@Column(name="study_status_name")
	private String studyStatusName;

	@Lob
	@Column(name="version_date_time")
	private String versionDateTime;

	@Lob
	@Column(name="version_number")
	private String versionNumber;

	public CtaStudyStatus() {
	}

	public int getStudyStatusNum() {
		return this.studyStatusNum;
	}

	public void setStudyStatusNum(int studyStatusNum) {
		this.studyStatusNum = studyStatusNum;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getStudyStatusName() {
		return this.studyStatusName;
	}

	public void setStudyStatusName(String studyStatusName) {
		this.studyStatusName = studyStatusName;
	}

	public String getVersionDateTime() {
		return this.versionDateTime;
	}

	public void setVersionDateTime(String versionDateTime) {
		this.versionDateTime = versionDateTime;
	}

	public String getVersionNumber() {
		return this.versionNumber;
	}

	public void setVersionNumber(String versionNumber) {
		this.versionNumber = versionNumber;
	}

}