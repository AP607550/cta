package com.ctastudio.dto;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the cta_subject_form database table.
 * 
 */
@Entity
@Table(name="cta_subject_form")
public class CtaSubjectForm implements Serializable, DomainObject {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private CtaSubjectFormPK id;

	@Lob
	@Column(name="form_owner")
	private String formOwner;

	@Column(name="form_owner_updated_ts_gmt")
	private int formOwnerUpdatedTsGmt;

	@Column(name="form_status_num")
	private int formStatusNum;

	@Column(name="study_num")
	private int studyNum;

	@Column(name="subject_form_order_num")
	private int subjectFormOrderNum;

	@Column(name="updated_by_client_uuid")
	private String updatedByClientUuid;

	@Column(name="updated_by_user")
	private String updatedByUser;

	@Lob
	@Column(name="updated_comment")
	private String updatedComment;

	@Lob
	@Column(name="updated_date_time")
	private String updatedDateTime;

	@Column(name="updated_gmt_offset")
	private int updatedGmtOffset;

	@Column(name="updated_ts_gmt")
	private int updatedTsGmt;

	@Lob
	@Column(name="version_date_time")
	private String versionDateTime;

	@Lob
	@Column(name="version_number")
	private String versionNumber;

	public CtaSubjectForm() {
	}

	public CtaSubjectFormPK getId() {
		return this.id;
	}

	public void setId(CtaSubjectFormPK id) {
		this.id = id;
	}

	public String getFormOwner() {
		return this.formOwner;
	}

	public void setFormOwner(String formOwner) {
		this.formOwner = formOwner;
	}

	public int getFormOwnerUpdatedTsGmt() {
		return this.formOwnerUpdatedTsGmt;
	}

	public void setFormOwnerUpdatedTsGmt(int formOwnerUpdatedTsGmt) {
		this.formOwnerUpdatedTsGmt = formOwnerUpdatedTsGmt;
	}

	public int getFormStatusNum() {
		return this.formStatusNum;
	}

	public void setFormStatusNum(int formStatusNum) {
		this.formStatusNum = formStatusNum;
	}

	public int getStudyNum() {
		return this.studyNum;
	}

	public void setStudyNum(int studyNum) {
		this.studyNum = studyNum;
	}

	public int getSubjectFormOrderNum() {
		return this.subjectFormOrderNum;
	}

	public void setSubjectFormOrderNum(int subjectFormOrderNum) {
		this.subjectFormOrderNum = subjectFormOrderNum;
	}

	public String getUpdatedByClientUuid() {
		return this.updatedByClientUuid;
	}

	public void setUpdatedByClientUuid(String updatedByClientUuid) {
		this.updatedByClientUuid = updatedByClientUuid;
	}

	public String getUpdatedByUser() {
		return this.updatedByUser;
	}

	public void setUpdatedByUser(String updatedByUser) {
		this.updatedByUser = updatedByUser;
	}

	public String getUpdatedComment() {
		return this.updatedComment;
	}

	public void setUpdatedComment(String updatedComment) {
		this.updatedComment = updatedComment;
	}

	public String getUpdatedDateTime() {
		return this.updatedDateTime;
	}

	public void setUpdatedDateTime(String updatedDateTime) {
		this.updatedDateTime = updatedDateTime;
	}

	public int getUpdatedGmtOffset() {
		return this.updatedGmtOffset;
	}

	public void setUpdatedGmtOffset(int updatedGmtOffset) {
		this.updatedGmtOffset = updatedGmtOffset;
	}

	public int getUpdatedTsGmt() {
		return this.updatedTsGmt;
	}

	public void setUpdatedTsGmt(int updatedTsGmt) {
		this.updatedTsGmt = updatedTsGmt;
	}

	public String getVersionDateTime() {
		return this.versionDateTime;
	}

	public void setVersionDateTime(String versionDateTime) {
		this.versionDateTime = versionDateTime;
	}

	public String getVersionNumber() {
		return this.versionNumber;
	}

	public void setVersionNumber(String versionNumber) {
		this.versionNumber = versionNumber;
	}

}