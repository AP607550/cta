package com.ctastudio.dto;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the cta_subject_history database table.
 * 
 */
@Entity
@Table(name="cta_subject_history")
public class CtaSubjectHistory implements Serializable, DomainObject {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private CtaSubjectHistoryPK id;

	@Column(name="country_num")
	private int countryNum;

	@Column(name="created_by_user")
	private String createdByUser;

	@Lob
	private String dob;

	@Lob
	private String gender;

	@Lob
	private String initials;

	@Column(name="site_num")
	private int siteNum;

	@Lob
	@Column(name="source_subject_code")
	private String sourceSubjectCode;

	@Column(name="study_num")
	private int studyNum;

	@Lob
	@Column(name="subject_code")
	private String subjectCode;

	@Column(name="subject_status_num")
	private int subjectStatusNum;

	@Column(name="updated_by_user")
	private String updatedByUser;

	@Lob
	@Column(name="updated_comment")
	private String updatedComment;

	@Lob
	@Column(name="updated_date_time")
	private String updatedDateTime;

	@Column(name="updated_gmt_offset")
	private int updatedGmtOffset;

	@Lob
	@Column(name="version_date_time")
	private String versionDateTime;

	@Lob
	@Column(name="version_number")
	private String versionNumber;

	public CtaSubjectHistory() {
	}

	public CtaSubjectHistoryPK getId() {
		return this.id;
	}

	public void setId(CtaSubjectHistoryPK id) {
		this.id = id;
	}

	public int getCountryNum() {
		return this.countryNum;
	}

	public void setCountryNum(int countryNum) {
		this.countryNum = countryNum;
	}

	public String getCreatedByUser() {
		return this.createdByUser;
	}

	public void setCreatedByUser(String createdByUser) {
		this.createdByUser = createdByUser;
	}

	public String getDob() {
		return this.dob;
	}

	public void setDob(String dob) {
		this.dob = dob;
	}

	public String getGender() {
		return this.gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getInitials() {
		return this.initials;
	}

	public void setInitials(String initials) {
		this.initials = initials;
	}

	public int getSiteNum() {
		return this.siteNum;
	}

	public void setSiteNum(int siteNum) {
		this.siteNum = siteNum;
	}

	public String getSourceSubjectCode() {
		return this.sourceSubjectCode;
	}

	public void setSourceSubjectCode(String sourceSubjectCode) {
		this.sourceSubjectCode = sourceSubjectCode;
	}

	public int getStudyNum() {
		return this.studyNum;
	}

	public void setStudyNum(int studyNum) {
		this.studyNum = studyNum;
	}

	public String getSubjectCode() {
		return this.subjectCode;
	}

	public void setSubjectCode(String subjectCode) {
		this.subjectCode = subjectCode;
	}

	public int getSubjectStatusNum() {
		return this.subjectStatusNum;
	}

	public void setSubjectStatusNum(int subjectStatusNum) {
		this.subjectStatusNum = subjectStatusNum;
	}

	public String getUpdatedByUser() {
		return this.updatedByUser;
	}

	public void setUpdatedByUser(String updatedByUser) {
		this.updatedByUser = updatedByUser;
	}

	public String getUpdatedComment() {
		return this.updatedComment;
	}

	public void setUpdatedComment(String updatedComment) {
		this.updatedComment = updatedComment;
	}

	public String getUpdatedDateTime() {
		return this.updatedDateTime;
	}

	public void setUpdatedDateTime(String updatedDateTime) {
		this.updatedDateTime = updatedDateTime;
	}

	public int getUpdatedGmtOffset() {
		return this.updatedGmtOffset;
	}

	public void setUpdatedGmtOffset(int updatedGmtOffset) {
		this.updatedGmtOffset = updatedGmtOffset;
	}

	public String getVersionDateTime() {
		return this.versionDateTime;
	}

	public void setVersionDateTime(String versionDateTime) {
		this.versionDateTime = versionDateTime;
	}

	public String getVersionNumber() {
		return this.versionNumber;
	}

	public void setVersionNumber(String versionNumber) {
		this.versionNumber = versionNumber;
	}

}