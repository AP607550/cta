package com.ctastudio.dto;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the cta_subject_history database table.
 * 
 */
@Embeddable
public class CtaSubjectHistoryPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="subject_uuid")
	private String subjectUuid;

	@Column(name="updated_by_client_uuid")
	private String updatedByClientUuid;

	@Column(name="updated_ts_gmt")
	private int updatedTsGmt;

	public CtaSubjectHistoryPK() {
	}
	public String getSubjectUuid() {
		return this.subjectUuid;
	}
	public void setSubjectUuid(String subjectUuid) {
		this.subjectUuid = subjectUuid;
	}
	public String getUpdatedByClientUuid() {
		return this.updatedByClientUuid;
	}
	public void setUpdatedByClientUuid(String updatedByClientUuid) {
		this.updatedByClientUuid = updatedByClientUuid;
	}
	public int getUpdatedTsGmt() {
		return this.updatedTsGmt;
	}
	public void setUpdatedTsGmt(int updatedTsGmt) {
		this.updatedTsGmt = updatedTsGmt;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof CtaSubjectHistoryPK)) {
			return false;
		}
		CtaSubjectHistoryPK castOther = (CtaSubjectHistoryPK)other;
		return 
			this.subjectUuid.equals(castOther.subjectUuid)
			&& this.updatedByClientUuid.equals(castOther.updatedByClientUuid)
			&& (this.updatedTsGmt == castOther.updatedTsGmt);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.subjectUuid.hashCode();
		hash = hash * prime + this.updatedByClientUuid.hashCode();
		hash = hash * prime + this.updatedTsGmt;
		
		return hash;
	}
}