package com.ctastudio.dto;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the cta_subject_screening_answer database table.
 * 
 */
@Embeddable
public class CtaSubjectScreeningAnswerPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="subject_screening_anum")
	private int subjectScreeningAnum;

	@Column(name="protocol_screening_qnum")
	private int protocolScreeningQnum;

	@Column(name="protocol_num")
	private int protocolNum;

	public CtaSubjectScreeningAnswerPK() {
	}
	public int getSubjectScreeningAnum() {
		return this.subjectScreeningAnum;
	}
	public void setSubjectScreeningAnum(int subjectScreeningAnum) {
		this.subjectScreeningAnum = subjectScreeningAnum;
	}
	public int getProtocolScreeningQnum() {
		return this.protocolScreeningQnum;
	}
	public void setProtocolScreeningQnum(int protocolScreeningQnum) {
		this.protocolScreeningQnum = protocolScreeningQnum;
	}
	public int getProtocolNum() {
		return this.protocolNum;
	}
	public void setProtocolNum(int protocolNum) {
		this.protocolNum = protocolNum;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof CtaSubjectScreeningAnswerPK)) {
			return false;
		}
		CtaSubjectScreeningAnswerPK castOther = (CtaSubjectScreeningAnswerPK)other;
		return 
			(this.subjectScreeningAnum == castOther.subjectScreeningAnum)
			&& (this.protocolScreeningQnum == castOther.protocolScreeningQnum)
			&& (this.protocolNum == castOther.protocolNum);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.subjectScreeningAnum;
		hash = hash * prime + this.protocolScreeningQnum;
		hash = hash * prime + this.protocolNum;
		
		return hash;
	}
}