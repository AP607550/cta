package com.ctastudio.dto;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the cta_subject_status database table.
 * 
 */
@Entity
@Table(name="cta_subject_status")
public class CtaSubjectStatus implements Serializable, DomainObject {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="subject_status_num")
	private int subjectStatusNum;

	@Lob
	private String description;

	@Lob
	@Column(name="subject_status_code")
	private String subjectStatusCode;

	@Lob
	@Column(name="version_date_time")
	private String versionDateTime;

	@Lob
	@Column(name="version_number")
	private String versionNumber;

	public CtaSubjectStatus() {
	}

	public int getSubjectStatusNum() {
		return this.subjectStatusNum;
	}

	public void setSubjectStatusNum(int subjectStatusNum) {
		this.subjectStatusNum = subjectStatusNum;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getSubjectStatusCode() {
		return this.subjectStatusCode;
	}

	public void setSubjectStatusCode(String subjectStatusCode) {
		this.subjectStatusCode = subjectStatusCode;
	}

	public String getVersionDateTime() {
		return this.versionDateTime;
	}

	public void setVersionDateTime(String versionDateTime) {
		this.versionDateTime = versionDateTime;
	}

	public String getVersionNumber() {
		return this.versionNumber;
	}

	public void setVersionNumber(String versionNumber) {
		this.versionNumber = versionNumber;
	}

}