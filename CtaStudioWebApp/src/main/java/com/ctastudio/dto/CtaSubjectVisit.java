package com.ctastudio.dto;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the cta_subject_visit database table.
 * 
 */
@Entity
@Table(name="cta_subject_visit")
public class CtaSubjectVisit implements Serializable, DomainObject {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private CtaSubjectVisitPK id;

	@Column(name="protocol_visit_num")
	private int protocolVisitNum;

	@Column(name="study_num")
	private int studyNum;

	@Column(name="updated_by_client_uuid")
	private String updatedByClientUuid;

	@Column(name="updated_by_user")
	private String updatedByUser;

	@Lob
	@Column(name="updated_comment")
	private String updatedComment;

	@Lob
	@Column(name="updated_date_time")
	private String updatedDateTime;

	@Column(name="updated_gmt_offset")
	private int updatedGmtOffset;

	@Column(name="updated_ts_gmt")
	private int updatedTsGmt;

	@Lob
	@Column(name="version_date_time")
	private String versionDateTime;

	@Lob
	@Column(name="version_number")
	private String versionNumber;

	@Column(name="visit_end_ts_gmt")
	private int visitEndTsGmt;

	@Column(name="visit_lock_ts_gmt")
	private int visitLockTsGmt;

	@Column(name="visit_start_ts_gmt")
	private int visitStartTsGmt;

	@Column(name="visit_status_num")
	private int visitStatusNum;

	public CtaSubjectVisit() {
	}

	public CtaSubjectVisitPK getId() {
		return this.id;
	}

	public void setId(CtaSubjectVisitPK id) {
		this.id = id;
	}

	public int getProtocolVisitNum() {
		return this.protocolVisitNum;
	}

	public void setProtocolVisitNum(int protocolVisitNum) {
		this.protocolVisitNum = protocolVisitNum;
	}

	public int getStudyNum() {
		return this.studyNum;
	}

	public void setStudyNum(int studyNum) {
		this.studyNum = studyNum;
	}

	public String getUpdatedByClientUuid() {
		return this.updatedByClientUuid;
	}

	public void setUpdatedByClientUuid(String updatedByClientUuid) {
		this.updatedByClientUuid = updatedByClientUuid;
	}

	public String getUpdatedByUser() {
		return this.updatedByUser;
	}

	public void setUpdatedByUser(String updatedByUser) {
		this.updatedByUser = updatedByUser;
	}

	public String getUpdatedComment() {
		return this.updatedComment;
	}

	public void setUpdatedComment(String updatedComment) {
		this.updatedComment = updatedComment;
	}

	public String getUpdatedDateTime() {
		return this.updatedDateTime;
	}

	public void setUpdatedDateTime(String updatedDateTime) {
		this.updatedDateTime = updatedDateTime;
	}

	public int getUpdatedGmtOffset() {
		return this.updatedGmtOffset;
	}

	public void setUpdatedGmtOffset(int updatedGmtOffset) {
		this.updatedGmtOffset = updatedGmtOffset;
	}

	public int getUpdatedTsGmt() {
		return this.updatedTsGmt;
	}

	public void setUpdatedTsGmt(int updatedTsGmt) {
		this.updatedTsGmt = updatedTsGmt;
	}

	public String getVersionDateTime() {
		return this.versionDateTime;
	}

	public void setVersionDateTime(String versionDateTime) {
		this.versionDateTime = versionDateTime;
	}

	public String getVersionNumber() {
		return this.versionNumber;
	}

	public void setVersionNumber(String versionNumber) {
		this.versionNumber = versionNumber;
	}

	public int getVisitEndTsGmt() {
		return this.visitEndTsGmt;
	}

	public void setVisitEndTsGmt(int visitEndTsGmt) {
		this.visitEndTsGmt = visitEndTsGmt;
	}

	public int getVisitLockTsGmt() {
		return this.visitLockTsGmt;
	}

	public void setVisitLockTsGmt(int visitLockTsGmt) {
		this.visitLockTsGmt = visitLockTsGmt;
	}

	public int getVisitStartTsGmt() {
		return this.visitStartTsGmt;
	}

	public void setVisitStartTsGmt(int visitStartTsGmt) {
		this.visitStartTsGmt = visitStartTsGmt;
	}

	public int getVisitStatusNum() {
		return this.visitStatusNum;
	}

	public void setVisitStatusNum(int visitStatusNum) {
		this.visitStatusNum = visitStatusNum;
	}

}