package com.ctastudio.dto;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the cta_subject_visit database table.
 * 
 */
@Embeddable
public class CtaSubjectVisitPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="subject_uuid")
	private String subjectUuid;

	@Column(name="subject_visit_num")
	private int subjectVisitNum;

	public CtaSubjectVisitPK() {
	}
	public String getSubjectUuid() {
		return this.subjectUuid;
	}
	public void setSubjectUuid(String subjectUuid) {
		this.subjectUuid = subjectUuid;
	}
	public int getSubjectVisitNum() {
		return this.subjectVisitNum;
	}
	public void setSubjectVisitNum(int subjectVisitNum) {
		this.subjectVisitNum = subjectVisitNum;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof CtaSubjectVisitPK)) {
			return false;
		}
		CtaSubjectVisitPK castOther = (CtaSubjectVisitPK)other;
		return 
			this.subjectUuid.equals(castOther.subjectUuid)
			&& (this.subjectVisitNum == castOther.subjectVisitNum);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.subjectUuid.hashCode();
		hash = hash * prime + this.subjectVisitNum;
		
		return hash;
	}
}