package com.ctastudio.dto;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the cta_visit_review database table.
 * 
 */
@Entity
@Table(name="cta_visit_review")
public class CtaVisitReview implements Serializable, DomainObject {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private CtaVisitReviewPK id;

	@Column(name="accepted_ts_gmt")
	private int acceptedTsGmt;

	@Lob
	@Column(name="assigned_by_user")
	private String assignedByUser;

	@Column(name="assigned_ts_gmt")
	private int assignedTsGmt;

	@Lob
	@Column(name="assignment_accepted")
	private String assignmentAccepted;

	@Column(name="protocol_num")
	private int protocolNum;

	@Column(name="protocol_visit_num")
	private int protocolVisitNum;

	@Column(name="study_num")
	private int studyNum;

	@Column(name="updated_by_client_uuid")
	private String updatedByClientUuid;

	@Column(name="updated_by_user")
	private String updatedByUser;

	@Lob
	@Column(name="updated_comment")
	private String updatedComment;

	@Lob
	@Column(name="updated_date_time")
	private String updatedDateTime;

	@Column(name="updated_gmt_offset")
	private int updatedGmtOffset;

	@Column(name="updated_ts_gmt")
	private int updatedTsGmt;

	@Lob
	@Column(name="version_date_time")
	private String versionDateTime;

	@Lob
	@Column(name="version_number")
	private String versionNumber;

	public CtaVisitReview() {
	}

	public CtaVisitReviewPK getId() {
		return this.id;
	}

	public void setId(CtaVisitReviewPK id) {
		this.id = id;
	}

	public int getAcceptedTsGmt() {
		return this.acceptedTsGmt;
	}

	public void setAcceptedTsGmt(int acceptedTsGmt) {
		this.acceptedTsGmt = acceptedTsGmt;
	}

	public String getAssignedByUser() {
		return this.assignedByUser;
	}

	public void setAssignedByUser(String assignedByUser) {
		this.assignedByUser = assignedByUser;
	}

	public int getAssignedTsGmt() {
		return this.assignedTsGmt;
	}

	public void setAssignedTsGmt(int assignedTsGmt) {
		this.assignedTsGmt = assignedTsGmt;
	}

	public String getAssignmentAccepted() {
		return this.assignmentAccepted;
	}

	public void setAssignmentAccepted(String assignmentAccepted) {
		this.assignmentAccepted = assignmentAccepted;
	}

	public int getProtocolNum() {
		return this.protocolNum;
	}

	public void setProtocolNum(int protocolNum) {
		this.protocolNum = protocolNum;
	}

	public int getProtocolVisitNum() {
		return this.protocolVisitNum;
	}

	public void setProtocolVisitNum(int protocolVisitNum) {
		this.protocolVisitNum = protocolVisitNum;
	}

	public int getStudyNum() {
		return this.studyNum;
	}

	public void setStudyNum(int studyNum) {
		this.studyNum = studyNum;
	}

	public String getUpdatedByClientUuid() {
		return this.updatedByClientUuid;
	}

	public void setUpdatedByClientUuid(String updatedByClientUuid) {
		this.updatedByClientUuid = updatedByClientUuid;
	}

	public String getUpdatedByUser() {
		return this.updatedByUser;
	}

	public void setUpdatedByUser(String updatedByUser) {
		this.updatedByUser = updatedByUser;
	}

	public String getUpdatedComment() {
		return this.updatedComment;
	}

	public void setUpdatedComment(String updatedComment) {
		this.updatedComment = updatedComment;
	}

	public String getUpdatedDateTime() {
		return this.updatedDateTime;
	}

	public void setUpdatedDateTime(String updatedDateTime) {
		this.updatedDateTime = updatedDateTime;
	}

	public int getUpdatedGmtOffset() {
		return this.updatedGmtOffset;
	}

	public void setUpdatedGmtOffset(int updatedGmtOffset) {
		this.updatedGmtOffset = updatedGmtOffset;
	}

	public int getUpdatedTsGmt() {
		return this.updatedTsGmt;
	}

	public void setUpdatedTsGmt(int updatedTsGmt) {
		this.updatedTsGmt = updatedTsGmt;
	}

	public String getVersionDateTime() {
		return this.versionDateTime;
	}

	public void setVersionDateTime(String versionDateTime) {
		this.versionDateTime = versionDateTime;
	}

	public String getVersionNumber() {
		return this.versionNumber;
	}

	public void setVersionNumber(String versionNumber) {
		this.versionNumber = versionNumber;
	}

}