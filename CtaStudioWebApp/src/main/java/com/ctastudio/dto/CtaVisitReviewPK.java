package com.ctastudio.dto;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the cta_visit_review database table.
 * 
 */
@Embeddable
public class CtaVisitReviewPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="subject_uuid")
	private String subjectUuid;

	@Column(name="subject_visit_num")
	private int subjectVisitNum;

	@Column(name="assigned_to_user")
	private String assignedToUser;

	public CtaVisitReviewPK() {
	}
	public String getSubjectUuid() {
		return this.subjectUuid;
	}
	public void setSubjectUuid(String subjectUuid) {
		this.subjectUuid = subjectUuid;
	}
	public int getSubjectVisitNum() {
		return this.subjectVisitNum;
	}
	public void setSubjectVisitNum(int subjectVisitNum) {
		this.subjectVisitNum = subjectVisitNum;
	}
	public String getAssignedToUser() {
		return this.assignedToUser;
	}
	public void setAssignedToUser(String assignedToUser) {
		this.assignedToUser = assignedToUser;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof CtaVisitReviewPK)) {
			return false;
		}
		CtaVisitReviewPK castOther = (CtaVisitReviewPK)other;
		return 
			this.subjectUuid.equals(castOther.subjectUuid)
			&& (this.subjectVisitNum == castOther.subjectVisitNum)
			&& this.assignedToUser.equals(castOther.assignedToUser);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.subjectUuid.hashCode();
		hash = hash * prime + this.subjectVisitNum;
		hash = hash * prime + this.assignedToUser.hashCode();
		
		return hash;
	}
}