package com.ctastudio.dto;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the current_metrics database table.
 * 
 */
@Entity
@Table(name="current_metrics")
public class CurrentMetric implements Serializable, DomainObject {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="current_metrics_id")
	private int currentMetricsId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="metric_timestamp")
	private Date metricTimestamp;

	@Lob
	@Column(name="metric_value")
	private String metricValue;

	@Lob
	@Column(name="target_key")
	private String targetKey;

	//bi-directional many-to-one association to Metric
	@ManyToOne
	@JoinColumn(name="metric_id")
	private Metric metric;

	public CurrentMetric() {
	}

	public int getCurrentMetricsId() {
		return this.currentMetricsId;
	}

	public void setCurrentMetricsId(int currentMetricsId) {
		this.currentMetricsId = currentMetricsId;
	}

	public Date getMetricTimestamp() {
		return this.metricTimestamp;
	}

	public void setMetricTimestamp(Date metricTimestamp) {
		this.metricTimestamp = metricTimestamp;
	}

	public String getMetricValue() {
		return this.metricValue;
	}

	public void setMetricValue(String metricValue) {
		this.metricValue = metricValue;
	}

	public String getTargetKey() {
		return this.targetKey;
	}

	public void setTargetKey(String targetKey) {
		this.targetKey = targetKey;
	}

	public Metric getMetric() {
		return this.metric;
	}

	public void setMetric(Metric metric) {
		this.metric = metric;
	}

}