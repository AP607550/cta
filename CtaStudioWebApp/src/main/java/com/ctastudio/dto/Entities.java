package com.ctastudio.dto;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Entities<T extends DomainObject> implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private String operation;
	
	private List<T> dtoObjects;

	@JsonProperty("Action")
	public String getOperation() {
		return operation;
	}

	public void setOperation(String operation) {
		this.operation = operation;
	}

	@JsonProperty("Cta_Entity")
	public List<T> getDtoObjects() {
		return dtoObjects;
	}

	public void setDtoObjects(List<T> dtoObjects) {
		this.dtoObjects = dtoObjects;
	}
	
	
}
