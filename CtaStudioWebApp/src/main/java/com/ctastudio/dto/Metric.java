package com.ctastudio.dto;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the metrics database table.
 * 
 */
@Entity
@Table(name="metrics")
public class Metric implements Serializable, DomainObject {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="metric_id")
	private int metricId;

	@Column(name="metric_name")
	private String metricName;

	@Column(name="target_type")
	private String targetType;

	@Column(name="threshold_lower")
	private String thresholdLower;

	@Column(name="threshold_upper")
	private String thresholdUpper;

	//bi-directional many-to-one association to CurrentMetric
	@OneToMany(mappedBy="metric")
	private List<CurrentMetric> currentMetrics;

	public Metric() {
	}

	public int getMetricId() {
		return this.metricId;
	}

	public void setMetricId(int metricId) {
		this.metricId = metricId;
	}

	public String getMetricName() {
		return this.metricName;
	}

	public void setMetricName(String metricName) {
		this.metricName = metricName;
	}

	public String getTargetType() {
		return this.targetType;
	}

	public void setTargetType(String targetType) {
		this.targetType = targetType;
	}

	public String getThresholdLower() {
		return this.thresholdLower;
	}

	public void setThresholdLower(String thresholdLower) {
		this.thresholdLower = thresholdLower;
	}

	public String getThresholdUpper() {
		return this.thresholdUpper;
	}

	public void setThresholdUpper(String thresholdUpper) {
		this.thresholdUpper = thresholdUpper;
	}

	public List<CurrentMetric> getCurrentMetrics() {
		return this.currentMetrics;
	}

	public void setCurrentMetrics(List<CurrentMetric> currentMetrics) {
		this.currentMetrics = currentMetrics;
	}

	public CurrentMetric addCurrentMetric(CurrentMetric currentMetric) {
		getCurrentMetrics().add(currentMetric);
		currentMetric.setMetric(this);

		return currentMetric;
	}

	public CurrentMetric removeCurrentMetric(CurrentMetric currentMetric) {
		getCurrentMetrics().remove(currentMetric);
		currentMetric.setMetric(null);

		return currentMetric;
	}

}