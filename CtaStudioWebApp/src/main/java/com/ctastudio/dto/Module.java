package com.ctastudio.dto;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the module database table.
 * 
 */
@Entity
public class Module implements Serializable, DomainObject {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="module_id")
	private int moduleId;

	@Column(name="module_name")
	private String moduleName;

	@Lob
	@Column(name="version_date_time")
	private String versionDateTime;

	@Lob
	@Column(name="version_number")
	private String versionNumber;

	//bi-directional many-to-one association to Screen
	@OneToMany(mappedBy="module")
	private List<Screen> screens;

	public Module() {
	}

	public int getModuleId() {
		return this.moduleId;
	}

	public void setModuleId(int moduleId) {
		this.moduleId = moduleId;
	}

	public String getModuleName() {
		return this.moduleName;
	}

	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}

	public String getVersionDateTime() {
		return this.versionDateTime;
	}

	public void setVersionDateTime(String versionDateTime) {
		this.versionDateTime = versionDateTime;
	}

	public String getVersionNumber() {
		return this.versionNumber;
	}

	public void setVersionNumber(String versionNumber) {
		this.versionNumber = versionNumber;
	}

	public List<Screen> getScreens() {
		return this.screens;
	}

	public void setScreens(List<Screen> screens) {
		this.screens = screens;
	}

	public Screen addScreen(Screen screen) {
		getScreens().add(screen);
		screen.setModule(this);

		return screen;
	}

	public Screen removeScreen(Screen screen) {
		getScreens().remove(screen);
		screen.setModule(null);

		return screen;
	}

}