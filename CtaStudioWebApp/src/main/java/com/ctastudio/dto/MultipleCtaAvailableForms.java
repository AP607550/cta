package com.ctastudio.dto;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class MultipleCtaAvailableForms implements Serializable {
	private static final long serialVersionUID = 1L;

	@JsonProperty("Multiple_Entities")
	private List<CtaAvailableForms> ctaAvailableForm;

	public List<CtaAvailableForms> getCtaAvailableForm() {
		return ctaAvailableForm;
	}

	public void setCtaAvailableForm(List<CtaAvailableForms> ctaAvailableForm) {
		this.ctaAvailableForm = ctaAvailableForm;
	}

	public static class CtaAvailableForms implements Serializable {
		private static final long serialVersionUID = 1L;

		private String action;

		private List<CtaAvailableForm> CtaAvailableForms;

		public CtaAvailableForms(
				@JsonProperty("action") String action,
				@JsonProperty("CtaAvailableForms") List<CtaAvailableForm> CtaAvailableForms) {
			this.action = action;
			this.CtaAvailableForms = CtaAvailableForms;
		}

		public String getAction() {
			return action;
		}

		public void setAction(String action) {
			this.action = action;
		}

		public List<CtaAvailableForm> getCtaAvailableForms() {
			return CtaAvailableForms;
		}

		public void setCtaAvailableForms(
				List<CtaAvailableForm> ctaAvailableForms) {
			CtaAvailableForms = ctaAvailableForms;
		}

	}
}
