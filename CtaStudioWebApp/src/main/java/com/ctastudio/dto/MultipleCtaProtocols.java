package com.ctastudio.dto;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class MultipleCtaProtocols implements Serializable {
	private static final long serialVersionUID = 1L;

	@JsonProperty("Multiple_Entities")
	private List<CtaProtocols> ctaProtocol;

	public List<CtaProtocols> getCtaProtocol() {
		return ctaProtocol;
	}

	public void setCtaProtocol(List<CtaProtocols> ctaProtocol) {
		this.ctaProtocol = ctaProtocol;
	}

	public static class CtaProtocols implements Serializable {
		private static final long serialVersionUID = 1L;

		private String action;

		private List<CtaProtocol> CtaProtocols;

		public CtaProtocols(
				@JsonProperty("action") String action,
				@JsonProperty("CtaProtocols") List<CtaProtocol> CtaProtocols) {
			this.action = action;
			this.CtaProtocols = CtaProtocols;
		}

		public String getAction() {
			return action;
		}

		public void setAction(String action) {
			this.action = action;
		}

		public List<CtaProtocol> getCtaProtocols() {
			return CtaProtocols;
		}

		public void setCtaProtocols(
				List<CtaProtocol> ctaProtocols) {
			CtaProtocols = ctaProtocols;
		}

	}
}
