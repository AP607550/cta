package com.ctastudio.dto;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class MultipleCtaSites implements Serializable {
	private static final long serialVersionUID = 1L;

	@JsonProperty("Multiple_Entities")
	private List<CtaSites> ctaSite;

	public List<CtaSites> getCtaSite() {
		return ctaSite;
	}

	public void setCtaSite(List<CtaSites> ctaSite) {
		this.ctaSite = ctaSite;
	}

	public static class CtaSites implements Serializable {
		private static final long serialVersionUID = 1L;

		private String action;

		private List<CtaSite> CtaSites;

		public CtaSites(
				@JsonProperty("action") String action,
				@JsonProperty("CtaSites") List<CtaSite> CtaSites) {
			this.action = action;
			this.CtaSites = CtaSites;
		}

		public String getAction() {
			return action;
		}

		public void setAction(String action) {
			this.action = action;
		}

		public List<CtaSite> getCtaSites() {
			return CtaSites;
		}

		public void setCtaSites(
				List<CtaSite> ctaSites) {
			CtaSites = ctaSites;
		}

	}
}
