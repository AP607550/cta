package com.ctastudio.dto;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class MultipleCtaStudies implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@JsonProperty("Multiple_Entities")
	private List<CtaStudies> ctaStudies;

	public List<CtaStudies> getCtaStudies() {
		return ctaStudies;
	}

	public void setCtaStudies(List<CtaStudies> ctaStudies) {
		this.ctaStudies = ctaStudies;
	}

	public static class CtaStudies implements Serializable {
		private static final long serialVersionUID = 1L;

		//@JsonProperty("action")
		private String action;

		//@JsonProperty("Entity")
		private List<CtaStudy> CtaStudies;
		
		/*public CtaStudies(@JsonProperty("action") String operation, @JsonProperty("CtaStudies") List<CtaStudy> ctaStudyList) {
			this.operation = operation;
			this.ctaStudyList = ctaStudyList;
		}*/
		
		public CtaStudies(@JsonProperty("action") String action,
				@JsonProperty("CtaStudies") List<CtaStudy> CtaStudies) {
			this.action = action;
			this.CtaStudies = CtaStudies;
		}

		public String getAction() {
			return action;
		}

		public void setAction(String action) {
			this.action = action;
		}

		public List<CtaStudy> getCtaStudies() {
			return CtaStudies;
		}

		public void setCtaStudies(List<CtaStudy> ctaStudies) {
			CtaStudies = ctaStudies;
		}

		

	}

}
