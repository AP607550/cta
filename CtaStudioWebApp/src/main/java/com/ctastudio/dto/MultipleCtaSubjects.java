package com.ctastudio.dto;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class MultipleCtaSubjects implements Serializable {
	private static final long serialVersionUID = 1L;

	@JsonProperty("Multiple_Entities")
	private List<CtaSubjects> ctaSubject;

	public List<CtaSubjects> getCtaSubject() {
		return ctaSubject;
	}

	public void setCtaSubject(List<CtaSubjects> ctaSubject) {
		this.ctaSubject = ctaSubject;
	}

	public static class CtaSubjects implements Serializable {
		private static final long serialVersionUID = 1L;

		private String action;

		private List<CtaSubject> CtaSubjects;

		public CtaSubjects(
				@JsonProperty("action") String action,
				@JsonProperty("CtaSubjects") List<CtaSubject> CtaSubjects) {
			this.action = action;
			this.CtaSubjects = CtaSubjects;
		}

		public String getAction() {
			return action;
		}

		public void setAction(String action) {
			this.action = action;
		}

		public List<CtaSubject> getCtaSubjects() {
			return CtaSubjects;
		}

		public void setCtaSubjects(
				List<CtaSubject> ctaSubjects) {
			CtaSubjects = ctaSubjects;
		}

	}
}
