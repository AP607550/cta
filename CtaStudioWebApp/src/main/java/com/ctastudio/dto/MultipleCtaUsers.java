package com.ctastudio.dto;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class MultipleCtaUsers implements Serializable {
	private static final long serialVersionUID = 1L;

	@JsonProperty("Multiple_Entities")
	private List<CtaUsers> ctaUser;

	public List<CtaUsers> getCtaUser() {
		return ctaUser;
	}

	public void setCtaUser(List<CtaUsers> ctaUser) {
		this.ctaUser = ctaUser;
	}

	public static class CtaUsers implements Serializable {
		private static final long serialVersionUID = 1L;

		private String action;

		private List<CtaUser> CtaUsers;

		public CtaUsers(@JsonProperty("action") String action,
				@JsonProperty("CtaUsers") List<CtaUser> CtaUsers) {
			this.action = action;
			this.CtaUsers = CtaUsers;
		}

		public String getAction() {
			return action;
		}

		public void setAction(String action) {
			this.action = action;
		}

		public List<CtaUser> getCtaUsers() {
			return CtaUsers;
		}

		public void setCtaUsers(List<CtaUser> ctaUsers) {
			CtaUsers = ctaUsers;
		}

	}
}
