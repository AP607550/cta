package com.ctastudio.dto;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class MultipleEntities<T extends List<Entities<S>>, S extends DomainObject> implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private T allEntities;
	
	private S type;

	@JsonCreator
	public MultipleEntities(@JsonProperty("Cta_Entities") T allEntities,@JsonProperty("type") S type) {
		this.allEntities = allEntities;
		this.type = type;
	}

	@JsonProperty("Cta_Entities")
	public T getAllEntities() {
		return allEntities;
	}

	public void setAllEntities(T allEntities) {
		this.allEntities = allEntities;
	}

}
