package com.ctastudio.dto;

import java.io.Serializable;
import java.util.List;

public class MultipleOperations implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private List<Operations> allOperations;

	public List<Operations> getAllOperations() {
		return allOperations;
	}

	public void setAllOperations(List<Operations> allOperations) {
		this.allOperations = allOperations;
	}
	

}
