package com.ctastudio.dto;

import java.io.Serializable;
import java.util.List;

public class Operations implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private String operation;
	
	private List<DomainObject> dtoObjects;

	public String getOperation() {
		return operation;
	}

	public void setOperation(String operation) {
		this.operation = operation;
	}

	public List<DomainObject> getDtoObjects() {
		return dtoObjects;
	}

	public void setDtoObjects(List<DomainObject> dtoObjects) {
		this.dtoObjects = dtoObjects;
	}
	
	
}
