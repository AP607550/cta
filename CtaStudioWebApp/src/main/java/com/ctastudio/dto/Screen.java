package com.ctastudio.dto;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the screen database table.
 * 
 */
@Entity
public class Screen implements Serializable, DomainObject {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="screen_id")
	private int screenId;

	@Lob
	@Column(name="version_date_time")
	private String versionDateTime;

	@Lob
	@Column(name="version_number")
	private String versionNumber;

	//bi-directional many-to-one association to Module
	@ManyToOne
	@JoinColumn(name="module_id")
	private Module module;

	//bi-directional many-to-one association to ScreenRole
	@OneToMany(mappedBy="screen")
	private List<ScreenRole> screenRoles;

	public Screen() {
	}

	public int getScreenId() {
		return this.screenId;
	}

	public void setScreenId(int screenId) {
		this.screenId = screenId;
	}

	public String getVersionDateTime() {
		return this.versionDateTime;
	}

	public void setVersionDateTime(String versionDateTime) {
		this.versionDateTime = versionDateTime;
	}

	public String getVersionNumber() {
		return this.versionNumber;
	}

	public void setVersionNumber(String versionNumber) {
		this.versionNumber = versionNumber;
	}

	public Module getModule() {
		return this.module;
	}

	public void setModule(Module module) {
		this.module = module;
	}

	public List<ScreenRole> getScreenRoles() {
		return this.screenRoles;
	}

	public void setScreenRoles(List<ScreenRole> screenRoles) {
		this.screenRoles = screenRoles;
	}

	public ScreenRole addScreenRole(ScreenRole screenRole) {
		getScreenRoles().add(screenRole);
		screenRole.setScreen(this);

		return screenRole;
	}

	public ScreenRole removeScreenRole(ScreenRole screenRole) {
		getScreenRoles().remove(screenRole);
		screenRole.setScreen(null);

		return screenRole;
	}

}