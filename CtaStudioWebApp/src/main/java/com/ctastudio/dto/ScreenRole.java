package com.ctastudio.dto;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the screen_role database table.
 * 
 */
@Entity
@Table(name="screen_role")
public class ScreenRole implements Serializable, DomainObject {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="screen_role_id")
	private int screenRoleId;

	@Lob
	@Column(name="version_date_time")
	private String versionDateTime;

	@Lob
	@Column(name="version_number")
	private String versionNumber;

	//bi-directional many-to-one association to CtaRole
	@ManyToOne
	@JoinColumn(name="role_num")
	private CtaRole ctaRole;

	//bi-directional many-to-one association to Screen
	@ManyToOne
	@JoinColumn(name="screen_id")
	private Screen screen;

	public ScreenRole() {
	}

	public int getScreenRoleId() {
		return this.screenRoleId;
	}

	public void setScreenRoleId(int screenRoleId) {
		this.screenRoleId = screenRoleId;
	}

	public String getVersionDateTime() {
		return this.versionDateTime;
	}

	public void setVersionDateTime(String versionDateTime) {
		this.versionDateTime = versionDateTime;
	}

	public String getVersionNumber() {
		return this.versionNumber;
	}

	public void setVersionNumber(String versionNumber) {
		this.versionNumber = versionNumber;
	}

	public CtaRole getCtaRole() {
		return this.ctaRole;
	}

	public void setCtaRole(CtaRole ctaRole) {
		this.ctaRole = ctaRole;
	}

	public Screen getScreen() {
		return this.screen;
	}

	public void setScreen(Screen screen) {
		this.screen = screen;
	}

}