package com.ctastudio.dto;

import java.io.Serializable;

/**
 * cta_site table dto
 */
public class SiteDto implements Serializable{

	
	private static final long serialVersionUID = -1173871654306059650L;
	
	private int siteNum;
	private String siteCode;
	private String siteName;
	private String address;
	private String department;
	private String city;
	private String state;
	private int countryNum;
	private int siteGmtOffset;
	private String phone;
	private String email;
	private int updatedTsGmt;
	private int updatedGmtOffset;
	private String updatedByUser;
	private String updatedByClientUuid;
	private String updatedDateTime;
	private String versionDateTime;
	public int getSiteNum() {
		return siteNum;
	}
	public void setSiteNum(int siteNum) {
		this.siteNum = siteNum;
	}
	public String getSiteCode() {
		return siteCode;
	}
	public void setSiteCode(String siteCode) {
		this.siteCode = siteCode;
	}
	public String getSiteName() {
		return siteName;
	}
	public void setSiteName(String siteName) {
		this.siteName = siteName;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getDepartment() {
		return department;
	}
	public void setDepartment(String department) {
		this.department = department;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public int getCountryNum() {
		return countryNum;
	}
	public void setCountryNum(int countryNum) {
		this.countryNum = countryNum;
	}
	public int getSiteGmtOffset() {
		return siteGmtOffset;
	}
	public void setSiteGmtOffset(int siteGmtOffset) {
		this.siteGmtOffset = siteGmtOffset;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public int getUpdatedTsGmt() {
		return updatedTsGmt;
	}
	public void setUpdatedTsGmt(int updatedTsGmt) {
		this.updatedTsGmt = updatedTsGmt;
	}
	public int getUpdatedGmtOffset() {
		return updatedGmtOffset;
	}
	public void setUpdatedGmtOffset(int updatedGmtOffset) {
		this.updatedGmtOffset = updatedGmtOffset;
	}
	public String getUpdatedByUser() {
		return updatedByUser;
	}
	public void setUpdatedByUser(String updatedByUser) {
		this.updatedByUser = updatedByUser;
	}
	public String getUpdatedByClientUuid() {
		return updatedByClientUuid;
	}
	public void setUpdatedByClientUuid(String updatedByClientUuid) {
		this.updatedByClientUuid = updatedByClientUuid;
	}
	public String getUpdatedDateTime() {
		return updatedDateTime;
	}
	public void setUpdatedDateTime(String updatedDateTime) {
		this.updatedDateTime = updatedDateTime;
	}
	public String getVersionDateTime() {
		return versionDateTime;
	}
	public void setVersionDateTime(String versionDateTime) {
		this.versionDateTime = versionDateTime;
	}
	
	

}
