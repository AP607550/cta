package com.ctastudio.dto;

import java.io.Serializable;

public class StudyDto implements Serializable{

	
	private static final long serialVersionUID = -1173871654306059651L;
	
	private int studyNum;
	private int protocolNum;
	private String studyCode;
	private int studyStatusNum;
	private String description;	
	private String startDate;
	private String endDate;	
	private int updatedTsGmt;
	private int updatedGmtOffset;
	private String updatedByUser;
	private String updatedByClientUuid;
	private String updatedDateTime;
	private String versionDateTime;
	public int getStudyNum() {
		return studyNum;
	}
	public void setStudyNum(int studyNum) {
		this.studyNum = studyNum;
	}
	public int getProtocolNum() {
		return protocolNum;
	}
	public void setProtocolNum(int protocolNum) {
		this.protocolNum = protocolNum;
	}
	public String getStudyCode() {
		return studyCode;
	}
	public void setStudyCode(String studyCode) {
		this.studyCode = studyCode;
	}
	public int getStudyStatusNum() {
		return studyStatusNum;
	}
	public void setStudyStatusNum(int studyStatusNum) {
		this.studyStatusNum = studyStatusNum;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	public int getUpdatedTsGmt() {
		return updatedTsGmt;
	}
	public void setUpdatedTsGmt(int updatedTsGmt) {
		this.updatedTsGmt = updatedTsGmt;
	}
	public int getUpdatedGmtOffset() {
		return updatedGmtOffset;
	}
	public void setUpdatedGmtOffset(int updatedGmtOffset) {
		this.updatedGmtOffset = updatedGmtOffset;
	}
	public String getUpdatedByUser() {
		return updatedByUser;
	}
	public void setUpdatedByUser(String updatedByUser) {
		this.updatedByUser = updatedByUser;
	}
	public String getUpdatedByClientUuid() {
		return updatedByClientUuid;
	}
	public void setUpdatedByClientUuid(String updatedByClientUuid) {
		this.updatedByClientUuid = updatedByClientUuid;
	}
	public String getUpdatedDateTime() {
		return updatedDateTime;
	}
	public void setUpdatedDateTime(String updatedDateTime) {
		this.updatedDateTime = updatedDateTime;
	}
	public String getVersionDateTime() {
		return versionDateTime;
	}
	public void setVersionDateTime(String versionDateTime) {
		this.versionDateTime = versionDateTime;
	}
	
	

}
