package com.ctastudio.filter;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class CORSFilter implements Filter {
	private FilterConfig filterConfig;
	private final Collection<String> allowedOrigins;
	private boolean anyOriginAllowed;
	private final Collection<String> allowedHttpMethods;
	private final Collection<String> allowedHttpHeaders;
	private final Collection<String> exposedHeaders;
	private boolean supportsCredentials;
	private long preflightMaxAge;
	private boolean loggingEnabled;
	private boolean decorateRequest;
	public static final String RESPONSE_HEADER_ACCESS_CONTROL_ALLOW_ORIGIN = "Access-Control-Allow-Origin";
	public static final String RESPONSE_HEADER_ACCESS_CONTROL_ALLOW_CREDENTIALS = "Access-Control-Allow-Credentials";
	public static final String RESPONSE_HEADER_ACCESS_CONTROL_EXPOSE_HEADERS = "Access-Control-Expose-Headers";
	public static final String RESPONSE_HEADER_ACCESS_CONTROL_MAX_AGE = "Access-Control-Max-Age";
	public static final String RESPONSE_HEADER_ACCESS_CONTROL_ALLOW_METHODS = "Access-Control-Allow-Methods";
	public static final String RESPONSE_HEADER_ACCESS_CONTROL_ALLOW_HEADERS = "Access-Control-Allow-Headers";
	public static final String REQUEST_HEADER_ORIGIN = "Origin";
	public static final String REQUEST_HEADER_ACCESS_CONTROL_REQUEST_METHOD = "Access-Control-Request-Method";
	public static final String REQUEST_HEADER_ACCESS_CONTROL_REQUEST_HEADERS = "Access-Control-Request-Headers";
	public static final String HTTP_REQUEST_ATTRIBUTE_PREFIX = "cors.";
	public static final String HTTP_REQUEST_ATTRIBUTE_ORIGIN = "cors.request.origin";
	public static final String HTTP_REQUEST_ATTRIBUTE_IS_CORS_REQUEST = "cors.isCorsRequest";
	public static final String HTTP_REQUEST_ATTRIBUTE_REQUEST_TYPE = "cors.request.type";
	public static final String HTTP_REQUEST_ATTRIBUTE_REQUEST_HEADERS = "cors.request.headers";
	public static final Collection<String> HTTP_METHODS = new HashSet(
			Arrays.asList(new String[] { "OPTIONS", "GET", "HEAD", "POST",
					"PUT", "DELETE", "TRACE", "CONNECT" }));

	public static final Collection<String> COMPLEX_HTTP_METHODS = new HashSet(
			Arrays.asList(new String[] { "PUT", "DELETE", "TRACE", "CONNECT" }));

	public static final Collection<String> SIMPLE_HTTP_METHODS = new HashSet(
			Arrays.asList(new String[] { "GET", "POST", "HEAD" }));

	public static final Collection<String> SIMPLE_HTTP_REQUEST_HEADERS = new HashSet(
			Arrays.asList(new String[] { "Accept", "Accept-Language",
					"Content-Language" }));

	public static final Collection<String> SIMPLE_HTTP_RESPONSE_HEADERS = new HashSet(
			Arrays.asList(new String[] { "Cache-Control", "Content-Language",
					"Content-Type", "Expires", "Last-Modified", "Pragma" }));

	public static final Collection<String> SIMPLE_HTTP_REQUEST_CONTENT_TYPE_VALUES = new HashSet(
			Arrays.asList(new String[] { "application/x-www-form-urlencoded",
					"multipart/form-data", "text/plain" }));
	public static final String DEFAULT_ALLOWED_ORIGINS = "*";
	public static final String DEFAULT_ALLOWED_HTTP_METHODS = "GET,POST,HEAD,OPTIONS";
	public static final String DEFAULT_PREFLIGHT_MAXAGE = "1800";
	public static final String DEFAULT_SUPPORTS_CREDENTIALS = "true";
	public static final String DEFAULT_ALLOWED_HTTP_HEADERS = "Origin,Accept,X-Requested-With,Content-Type,Access-Control-Request-Method,Access-Control-Request-Headers";
	public static final String DEFAULT_EXPOSED_HEADERS = "";
	public static final String DEFAULT_LOGGING_ENABLED = "false";
	public static final String DEFAULT_DECORATE_REQUEST = "true";
	public static final String PARAM_CORS_ALLOWED_ORIGINS = "cors.allowed.origins";
	public static final String PARAM_CORS_SUPPORT_CREDENTIALS = "cors.support.credentials";
	public static final String PARAM_CORS_EXPOSED_HEADERS = "cors.exposed.headers";
	public static final String PARAM_CORS_ALLOWED_HEADERS = "cors.allowed.headers";
	public static final String PARAM_CORS_ALLOWED_METHODS = "cors.allowed.methods";
	public static final String PARAM_CORS_PREFLIGHT_MAXAGE = "cors.preflight.maxage";
	public static final String PARAM_CORS_LOGGING_ENABLED = "cors.logging.enabled";
	public static final String PARAM_CORS_REQUEST_DECORATE = "cors.request.decorate";

	public CORSFilter() {
		this.allowedOrigins = new HashSet();
		this.allowedHttpMethods = new HashSet();
		this.allowedHttpHeaders = new HashSet();
		this.exposedHeaders = new HashSet();
	}

	public void doFilter(ServletRequest servletRequest,
			ServletResponse servletResponse, FilterChain filterChain)
			throws IOException, ServletException {
		if ((!(servletRequest instanceof HttpServletRequest))
				|| (!(servletResponse instanceof HttpServletResponse))) {
			String message = "CORS doesn't support non-HTTP request or response.";

			throw new ServletException(message);
		}

		HttpServletRequest request = (HttpServletRequest) servletRequest;
		HttpServletResponse response = (HttpServletResponse) servletResponse;

		CORSFilter.CORSRequestType requestType = checkRequestType(request);

		if (this.decorateRequest) {
			decorateCORSProperties(request, requestType);
		}
		switch (requestType.ordinal()) {
		case 1:
			handleSimpleCORS(request, response, filterChain);
			break;
		case 2:
			handleSimpleCORS(request, response, filterChain);
			break;
		case 3:
			handlePreflightCORS(request, response, filterChain);
			break;
		case 4:
			handleNonCORS(request, response, filterChain);
			break;
		default:
			handleInvalidCORS(request, response, filterChain);
		}
	}

	public void init(FilterConfig filterConfig) throws ServletException {
		parseAndStore(
				"*",
				"GET,POST,HEAD,OPTIONS",
				"Origin,Accept,X-Requested-With,Content-Type,Access-Control-Request-Method,Access-Control-Request-Headers",
				"", "true", "1800", "false", "true");

		this.filterConfig = filterConfig;
		this.loggingEnabled = false;

		if (filterConfig != null) {
			String configAllowedOrigins = filterConfig
					.getInitParameter("cors.allowed.origins");

			String configAllowedHttpMethods = filterConfig
					.getInitParameter("cors.allowed.methods");

			String configAllowedHttpHeaders = filterConfig
					.getInitParameter("cors.allowed.headers");

			String configExposedHeaders = filterConfig
					.getInitParameter("cors.exposed.headers");

			String configSupportsCredentials = filterConfig
					.getInitParameter("cors.support.credentials");

			String configPreflightMaxAge = filterConfig
					.getInitParameter("cors.preflight.maxage");

			String configLoggingEnabled = filterConfig
					.getInitParameter("cors.logging.enabled");

			String configDecorateRequest = filterConfig
					.getInitParameter("cors.request.decorate");

			parseAndStore(configAllowedOrigins, configAllowedHttpMethods,
					configAllowedHttpHeaders, configExposedHeaders,
					configSupportsCredentials, configPreflightMaxAge,
					configLoggingEnabled, configDecorateRequest);
		}
	}

	public void handleSimpleCORS(HttpServletRequest request,
			HttpServletResponse response, FilterChain filterChain)
			throws IOException, ServletException {
		CORSFilter.CORSRequestType requestType = checkRequestType(request);

		if ((requestType != CORSFilter.CORSRequestType.SIMPLE)
				&& (requestType != CORSFilter.CORSRequestType.ACTUAL)) {
			String message = new StringBuilder()
					.append("Expects a HttpServletRequest object of type ")
					.append(CORSFilter.CORSRequestType.SIMPLE).append(" or ")
					.append(CORSFilter.CORSRequestType.ACTUAL).toString();

			throw new IllegalArgumentException(message);
		}

		String origin = request.getHeader("Origin");

		String method = request.getMethod();

		if (!isOriginAllowed(origin)) {
			handleInvalidCORS(request, response, filterChain);
			return;
		}

		if (!this.allowedHttpMethods.contains(method)) {
			handleInvalidCORS(request, response, filterChain);
			return;
		}

		if ((this.anyOriginAllowed) && (!this.supportsCredentials)) {
			response.addHeader("Access-Control-Allow-Origin", "*");
		} else {
			response.addHeader("Access-Control-Allow-Origin", origin);
		}

		if (this.supportsCredentials) {
			response.addHeader("Access-Control-Allow-Credentials", "true");
		}

		if ((this.exposedHeaders != null) && (this.exposedHeaders.size() > 0)) {
			String exposedHeadersString = join(this.exposedHeaders, ",");
			response.addHeader("Access-Control-Expose-Headers",
					exposedHeadersString);
		}

		filterChain.doFilter(request, response);
	}

	public void handlePreflightCORS(HttpServletRequest request,
			HttpServletResponse response, FilterChain filterChain)
			throws IOException, ServletException {
		CORSFilter.CORSRequestType requestType = checkRequestType(request);
		if (requestType != CORSFilter.CORSRequestType.PRE_FLIGHT) {
			throw new IllegalArgumentException(new StringBuilder()
					.append("Expects a HttpServletRequest object of type ")
					.append(CORSFilter.CORSRequestType.PRE_FLIGHT.name()
							.toLowerCase()).toString());
		}

		String origin = request.getHeader("Origin");

		if (!isOriginAllowed(origin)) {
			handleInvalidCORS(request, response, filterChain);
			return;
		}

		String accessControlRequestMethod = request
				.getHeader("Access-Control-Request-Method");

		if ((accessControlRequestMethod == null)
				|| ((accessControlRequestMethod != null) && (!HTTP_METHODS
						.contains(accessControlRequestMethod.trim())))) {
			handleInvalidCORS(request, response, filterChain);
			return;
		}
		accessControlRequestMethod = accessControlRequestMethod.trim();

		String accessControlRequestHeadersHeader = request
				.getHeader("Access-Control-Request-Headers");

		List<String> accessControlRequestHeaders = new LinkedList();
		if ((accessControlRequestHeadersHeader != null)
				&& (!accessControlRequestHeadersHeader.trim().isEmpty())) {
			String[] headers = accessControlRequestHeadersHeader.trim().split(
					",");

			for (String header : headers) {
				accessControlRequestHeaders.add(header.trim().toLowerCase());
			}

		}

		if (!this.allowedHttpMethods.contains(accessControlRequestMethod)) {
			handleInvalidCORS(request, response, filterChain);
			return;
		}

		if (!accessControlRequestHeaders.isEmpty()) {
			for (String header : accessControlRequestHeaders) {
				if (!this.allowedHttpHeaders.contains(header)) {
					handleInvalidCORS(request, response, filterChain);
					return;
				}
			}

		}

		if (this.supportsCredentials) {
			response.addHeader("Access-Control-Allow-Origin", origin);

			response.addHeader("Access-Control-Allow-Credentials", "true");
		} else if (this.anyOriginAllowed) {
			response.addHeader("Access-Control-Allow-Origin", "*");
		} else {
			response.addHeader("Access-Control-Allow-Origin", origin);
		}

		if (this.preflightMaxAge > 0L) {
			response.addHeader("Access-Control-Max-Age",
					String.valueOf(this.preflightMaxAge));
		}

		response.addHeader("Access-Control-Allow-Methods",
				accessControlRequestMethod);

		if ((this.allowedHttpHeaders != null)
				&& (!this.allowedHttpHeaders.isEmpty()))
			response.addHeader("Access-Control-Allow-Headers",
					join(this.allowedHttpHeaders, ","));
	}

	public void handleNonCORS(HttpServletRequest request,
			HttpServletResponse response, FilterChain filterChain)
			throws IOException, ServletException {
		filterChain.doFilter(request, response);
	}

	public void handleInvalidCORS(HttpServletRequest request,
			HttpServletResponse response, FilterChain filterChain)
			throws IOException, ServletException {
		String origin = request.getHeader("Origin");
		String method = request.getMethod();
		String accessControlRequestHeaders = request
				.getHeader("Access-Control-Request-Headers");

		String message = new StringBuilder()
				.append("Invalid CORS request; Origin=").append(origin)
				.append(";Method=").append(method).toString();

		if (accessControlRequestHeaders != null) {
			message = new StringBuilder().append(message)
					.append(";Access-Control-Request-Headers=")
					.append(accessControlRequestHeaders).toString();
		}

		response.setContentType("text/plain");
		response.setStatus(403);
		response.resetBuffer();

		log(message);
	}

	public void destroy() {
	}

	public static void decorateCORSProperties(HttpServletRequest request,
			CORSFilter.CORSRequestType corsRequestType) {
		if (request == null) {
			throw new IllegalArgumentException(
					"HttpServletRequest object is null");
		}

		if (corsRequestType == null) {
			throw new IllegalArgumentException("CORSRequestType object is null");
		}

		switch (corsRequestType.ordinal()) {
		case 1:
			request.setAttribute("cors.isCorsRequest", Boolean.valueOf(true));

			request.setAttribute("cors.request.origin",
					request.getHeader("Origin"));

			request.setAttribute("cors.request.type", corsRequestType.name()
					.toLowerCase());

			break;
		case 2:
			request.setAttribute("cors.isCorsRequest", Boolean.valueOf(true));

			request.setAttribute("cors.request.origin",
					request.getHeader("Origin"));

			request.setAttribute("cors.request.type", corsRequestType.name()
					.toLowerCase());

			break;
		case 3:
			request.setAttribute("cors.isCorsRequest", Boolean.valueOf(true));

			request.setAttribute("cors.request.origin",
					request.getHeader("Origin"));

			request.setAttribute("cors.request.type", corsRequestType.name()
					.toLowerCase());

			String headers = request
					.getHeader("Access-Control-Request-Headers");

			if (headers == null) {
				headers = "";
			}
			request.setAttribute("cors.request.headers", headers);

			break;
		case 4:
			request.setAttribute("cors.isCorsRequest", Boolean.valueOf(false));

			break;
		}
	}

	public static String join(Collection<String> elements, String joinSeparator) {
		String separator = ",";
		if (elements == null) {
			return null;
		}
		if (joinSeparator != null) {
			separator = joinSeparator;
		}
		StringBuilder buffer = new StringBuilder();
		boolean isFirst = true;
		Iterator iterator = elements.iterator();
		while (iterator.hasNext()) {
			Object element = iterator.next();

			if (!isFirst)
				buffer.append(separator);
			else {
				isFirst = false;
			}

			if (element != null) {
				buffer.append(element);
			}
		}

		return buffer.toString();
	}

	public CORSFilter.CORSRequestType checkRequestType(
			HttpServletRequest request) {
		CORSFilter.CORSRequestType requestType = CORSFilter.CORSRequestType.INVALID_CORS;
		if (request == null) {
			throw new IllegalArgumentException(
					"HttpServletRequest object is null");
		}

		String originHeader = request.getHeader("Origin");

		if (originHeader != null) {
			if (originHeader.isEmpty()) {
				requestType = CORSFilter.CORSRequestType.INVALID_CORS;
			} else if (!isValidOrigin(originHeader)) {
				requestType = CORSFilter.CORSRequestType.INVALID_CORS;
			} else {
				String method = request.getMethod();
				if ((method != null) && (HTTP_METHODS.contains(method)))
					if ("OPTIONS".equals(method)) {
						String accessControlRequestMethodHeader = request
								.getHeader("Access-Control-Request-Method");

						if ((accessControlRequestMethodHeader != null)
								&& (!accessControlRequestMethodHeader.isEmpty())) {
							requestType = CORSFilter.CORSRequestType.PRE_FLIGHT;
						} else if ((accessControlRequestMethodHeader != null)
								&& (accessControlRequestMethodHeader.isEmpty())) {
							requestType = CORSFilter.CORSRequestType.INVALID_CORS;
						} else
							requestType = CORSFilter.CORSRequestType.ACTUAL;
					} else if (("GET".equals(method))
							|| ("HEAD".equals(method))) {
						requestType = CORSFilter.CORSRequestType.SIMPLE;
					} else if ("POST".equals(method)) {
						String contentType = request.getContentType();
						if (contentType != null) {
							contentType = contentType.toLowerCase().trim();
							if (SIMPLE_HTTP_REQUEST_CONTENT_TYPE_VALUES
									.contains(contentType)) {
								requestType = CORSFilter.CORSRequestType.SIMPLE;
							} else
								requestType = CORSFilter.CORSRequestType.ACTUAL;
						}
					} else if (COMPLEX_HTTP_METHODS.contains(method)) {
						requestType = CORSFilter.CORSRequestType.ACTUAL;
					}
			}
		} else {
			requestType = CORSFilter.CORSRequestType.NOT_CORS;
		}

		return requestType;
	}

	private boolean isOriginAllowed(String origin) {
		if (this.anyOriginAllowed) {
			return true;
		}

		if (this.allowedOrigins.contains(origin)) {
			return true;
		}

		return false;
	}

	private void log(String message) {
		if (this.loggingEnabled)
			this.filterConfig.getServletContext().log(message);
	}

	private void parseAndStore(String allowedOrigins,
			String allowedHttpMethods, String allowedHttpHeaders,
			String exposedHeaders, String supportsCredentials,
			String preflightMaxAge, String loggingEnabled,
			String decorateRequest) throws ServletException {
		if (allowedOrigins != null) {
			if (allowedOrigins.trim().equals("*")) {
				this.anyOriginAllowed = true;
			} else {
				this.anyOriginAllowed = false;
				Set setAllowedOrigins = parseStringToSet(allowedOrigins);

				this.allowedOrigins.clear();
				this.allowedOrigins.addAll(setAllowedOrigins);
			}
		}

		if (allowedHttpMethods != null) {
			Set<String> setAllowedHttpMethods = parseStringToSet(allowedHttpMethods);

			this.allowedHttpMethods.clear();
			this.allowedHttpMethods.addAll(setAllowedHttpMethods);
		}

		if (allowedHttpHeaders != null) {
			Set<String> setAllowedHttpHeaders = parseStringToSet(allowedHttpHeaders);

			Set lowerCaseHeaders = new HashSet();
			for (String header : setAllowedHttpHeaders) {
				String lowerCase = header.toLowerCase();
				lowerCaseHeaders.add(lowerCase);
			}
			this.allowedHttpHeaders.clear();
			this.allowedHttpHeaders.addAll(lowerCaseHeaders);
		}

		if (exposedHeaders != null) {
			Set setExposedHeaders = parseStringToSet(exposedHeaders);
			this.exposedHeaders.clear();
			this.exposedHeaders.addAll(setExposedHeaders);
		}

		if (supportsCredentials != null) {
			boolean isSupportsCredentials = Boolean
					.parseBoolean(supportsCredentials);

			this.supportsCredentials = isSupportsCredentials;
		}

		if (preflightMaxAge != null) {
			try {
				if (!preflightMaxAge.isEmpty())
					this.preflightMaxAge = Long.parseLong(preflightMaxAge);
				else
					this.preflightMaxAge = 0L;
			} catch (NumberFormatException e) {
				throw new ServletException("Unable to parse preflightMaxAge", e);
			}
		}

		if (loggingEnabled != null) {
			boolean isLoggingEnabled = Boolean.parseBoolean(loggingEnabled);

			this.loggingEnabled = isLoggingEnabled;
		}

		if (decorateRequest != null) {
			boolean shouldDecorateRequest = Boolean
					.parseBoolean(decorateRequest);

			this.decorateRequest = shouldDecorateRequest;
		}
	}

	private Set<String> parseStringToSet(String data) {
		String[] splits = null;

		if ((data != null) && (data.length() > 0))
			splits = data.split(",");
		else {
			splits = new String[0];
		}

		Set set = new HashSet();
		if (splits.length > 0) {
			for (String split : splits) {
				set.add(split.trim());
			}
		}

		return set;
	}

	public static boolean isValidOrigin(String origin) {
		if (origin.contains("%")) {
			return false;
		}

		URI originURI = null;
		try {
			originURI = new URI(origin);
		} catch (URISyntaxException e) {
			return false;
		}

		if (originURI.getScheme() == null) {
			return false;
		}

		return true;
	}

	public boolean isLoggingEnabled() {
		return this.loggingEnabled;
	}

	public boolean isAnyOriginAllowed() {
		return this.anyOriginAllowed;
	}

	public Collection<String> getExposedHeaders() {
		return this.exposedHeaders;
	}

	public boolean isSupportsCredentials() {
		return this.supportsCredentials;
	}

	public long getPreflightMaxAge() {
		return this.preflightMaxAge;
	}

	public Collection<String> getAllowedOrigins() {
		return this.allowedOrigins;
	}

	public Collection<String> getAllowedHttpMethods() {
		return this.allowedHttpMethods;
	}

	public Collection<String> getAllowedHttpHeaders() {
		return this.allowedHttpHeaders;
	}

	public enum CORSRequestType {
		SIMPLE,

		ACTUAL,

		PRE_FLIGHT,

		NOT_CORS,

		INVALID_CORS;
	}
}
