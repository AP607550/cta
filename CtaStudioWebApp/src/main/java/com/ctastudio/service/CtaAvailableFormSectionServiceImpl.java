/********************
+------------------------------------------------------------+
 | Class Name: CtaAvailableFormSectionServiceImpl.java                                  |
 | Class Purpose: Service for cta_available_form_section                          |
 | Comment : This is Service layer for cta_available_form_section                 |
 | Author : Mriganka Shekhar Roy                 |
+------------------------------------------------------------+
 ************/
package com.ctastudio.service;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ctastudio.dao.CtaAvailableFormSectionDaoImpl;
import com.ctastudio.dto.CtaAvailableFormSection;
import com.ctastudio.dto.CtaAvailableFormSectionPK;

@Service
public class CtaAvailableFormSectionServiceImpl {

	private static final Logger LOGGER = Logger
			.getLogger(CtaSiteServiceImpl.class);
	
	@Autowired
	CtaAvailableFormSectionDaoImpl ctaAvailableFormSectionDaoImpl;

	public CtaAvailableFormSection getCtaAvailableFormSection(int formNum,
			String formSection) throws Exception {
		CtaAvailableFormSectionPK id = new CtaAvailableFormSectionPK();
		id.setFormNum(formNum);
		id.setFormSection(formSection);
		return ctaAvailableFormSectionDaoImpl.get(id);
	}

	public List<CtaAvailableFormSection> getAllCtaAvailableFormSections()
			throws Exception {
		return ctaAvailableFormSectionDaoImpl.getAll();
	}

	public void createCtaAvailableFormSection(CtaAvailableFormSection object)
			throws Exception {
		ctaAvailableFormSectionDaoImpl.save(object);
	}

	public void deleteCtaAvailableFormSection(int formNum, String formSection)
			throws Exception {
		CtaAvailableFormSectionPK id = new CtaAvailableFormSectionPK();
		id.setFormNum(formNum);
		id.setFormSection(formSection);
		CtaAvailableFormSection ctaAvailableFormSection = ctaAvailableFormSectionDaoImpl
				.get(id);
		ctaAvailableFormSectionDaoImpl.delete(ctaAvailableFormSection);
	}

	public void updateCtaAvailableFormSection(CtaAvailableFormSection object)
			throws Exception {
		ctaAvailableFormSectionDaoImpl.update(object);
	}

}
