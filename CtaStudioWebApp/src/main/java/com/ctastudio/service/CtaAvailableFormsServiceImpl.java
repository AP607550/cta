/********************
+------------------------------------------------------------+
 | Class Name: CtaAvailableFormsServiceImpl.java                                  |
 | Class Purpose: Service for cta_available_forms                          |
 | Comment : This is Service layer for cta_available_forms                 |
 | Author : Mriganka Shekhar Roy                 |
+------------------------------------------------------------+
 ************/
package com.ctastudio.service;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ctastudio.dao.CtaAvailableFormsDaoImpl;
import com.ctastudio.dto.CtaAvailableForm;
import com.ctastudio.dto.MultipleCtaAvailableForms;

@Service
public class CtaAvailableFormsServiceImpl {

	private static final Logger LOGGER = Logger
			.getLogger(CtaAvailableFormsServiceImpl.class);

	@Autowired
	CtaAvailableFormsDaoImpl ctaAvailableFormsDaoImpl;

	public CtaAvailableForm getCtaAvailableForm(int siteNum) throws Exception {
		return ctaAvailableFormsDaoImpl.get(siteNum);
	}

	public List<CtaAvailableForm> getAllCtaAvailableForms() throws Exception {
		return ctaAvailableFormsDaoImpl.getAll();
	}

	public void createCtaAvailableForm(CtaAvailableForm object)
			throws Exception {
		ctaAvailableFormsDaoImpl.save(object);
	}

	public void deleteCtaAvailableForm(int siteNum) throws Exception {
		CtaAvailableForm ctaAvailableForm = ctaAvailableFormsDaoImpl
				.get(siteNum);
		ctaAvailableFormsDaoImpl.delete(ctaAvailableForm);
	}

	public void updateCtaAvailableForm(CtaAvailableForm object)
			throws Exception {
		ctaAvailableFormsDaoImpl.update(object);
	}
	
	public List<CtaAvailableForm> getCtaAvailableFormByVersionNumber(String versionNumber) {
		return ctaAvailableFormsDaoImpl.getCtaAvailableFormByVersionNumber(versionNumber);
	}

	public MultipleCtaAvailableForms performMultipleOperation(
			MultipleCtaAvailableForms multipleCtaAvailableForms) throws Exception {
		multipleCtaAvailableForms = ctaAvailableFormsDaoImpl
				.performMultipleOperation(multipleCtaAvailableForms);
		return multipleCtaAvailableForms;
	}

}
