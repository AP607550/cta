/********************
+------------------------------------------------------------+
 | Class Name: CtaProtocolServiceImpl.java                                  |
 | Class Purpose: Service for cta_subject                          |
 | Comment : This is Service layer for cta_subject                 |
 | Author : Mriganka Shekhar Roy                 |
+------------------------------------------------------------+
 ************/
package com.ctastudio.service;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ctastudio.dao.CtaProtocolDaoImpl;
import com.ctastudio.dto.CtaProtocol;
import com.ctastudio.dto.MultipleCtaProtocols;

@Service
public class CtaProtocolServiceImpl {

	private static final Logger LOGGER = Logger
			.getLogger(CtaSiteServiceImpl.class);
	@Autowired
	CtaProtocolDaoImpl ctaProtocolDaoImpl;

	public CtaProtocol getCtaProtocol(int siteNum) throws Exception {
		return ctaProtocolDaoImpl.get(siteNum);
	}

	public List<CtaProtocol> getAllCtaProtocols() throws Exception {
		return ctaProtocolDaoImpl.getAll();
	}

	public void createCtaProtocol(CtaProtocol object) throws Exception {
		ctaProtocolDaoImpl.save(object);
	}

	public void deleteCtaProtocol(int siteNum) throws Exception {
		CtaProtocol ctaProtocol = ctaProtocolDaoImpl.get(siteNum);
		ctaProtocolDaoImpl.delete(ctaProtocol);
	}

	public void updateCtaProtocol(CtaProtocol object) throws Exception {
		ctaProtocolDaoImpl.update(object);
	}

	public List<CtaProtocol> getCtaProtocolByVersionNumber(String versionNumber) {
		return ctaProtocolDaoImpl.getCtaProtocolByVersionNumber(versionNumber);
	}

	public MultipleCtaProtocols performMultipleOperation(
			MultipleCtaProtocols multipleCtaProtocols) throws Exception {
		multipleCtaProtocols = ctaProtocolDaoImpl
				.performMultipleOperation(multipleCtaProtocols);
		return multipleCtaProtocols;
	}

}
