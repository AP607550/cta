package com.ctastudio.service;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.stereotype.Service;

import com.ctastudio.dao.CtaProtocolVisitDaoImpl;
import com.ctastudio.dto.CtaProtocolVisit;
import com.ctastudio.dto.CtaProtocolVisitPK;

@Service
public class CtaProtocolVisitServiceImpl {

	private static final Logger LOGGER = Logger
			.getLogger(CtaProtocolVisitServiceImpl.class);

	@Autowired
	CtaProtocolVisitDaoImpl ctaProtocolVisitDaoImpl;

	public CtaProtocolVisit getCtaProtocolVisit(int protocolNum,
			int protocolVisitNum) throws Exception {
		CtaProtocolVisitPK id = new CtaProtocolVisitPK();
		id.setProtocolNum(protocolNum);
		id.setProtocolVisitNum(protocolVisitNum);
		return ctaProtocolVisitDaoImpl.get(id);
	}

	public List<CtaProtocolVisit> getAllCtaProtocolVisits() throws Exception {
		return ctaProtocolVisitDaoImpl.getAll();
	}

	public void createCtaProtocolVisit(CtaProtocolVisit object)
			throws Exception {
		CtaProtocolVisitPK id = object.getId();
		int protocolVisitNum = 0;
		Integer maxVisitNumber = ctaProtocolVisitDaoImpl
				.getMaxProtocolVistNumberForProtocolNumber(id.getProtocolNum());
		if (maxVisitNumber == null) {
			protocolVisitNum = 1;
		} else {
			protocolVisitNum = maxVisitNumber.intValue() + 1;
		}
		id.setProtocolVisitNum(protocolVisitNum);
		object.setId(id);
		ctaProtocolVisitDaoImpl.save(object);
	}

	public void deleteCtaProtocolVisit(int protocolNum, int protocolVisitNum)
			throws Exception {
		CtaProtocolVisitPK id = new CtaProtocolVisitPK();
		id.setProtocolNum(protocolNum);
		id.setProtocolVisitNum(protocolVisitNum);
		CtaProtocolVisit ctaProtocolVisit = ctaProtocolVisitDaoImpl.get(id);
		ctaProtocolVisitDaoImpl.delete(ctaProtocolVisit);
	}

	public void updateCtaProtocolVisit(CtaProtocolVisit object)
			throws Exception {
		ctaProtocolVisitDaoImpl.update(object);
	}
}
