/********************
+------------------------------------------------------------+
 | Class Name: CtaSiteServiceImpl.java                                  |
 | Class Purpose: Service for cta_site                          |
 | Comment : This is Service layer for cta_site                 |
 | Author : Mriganka Shekhar Roy                 |
+------------------------------------------------------------+
 ************/
package com.ctastudio.service;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ctastudio.dao.CtaSiteDaoImpl;
import com.ctastudio.dto.CtaSite;
import com.ctastudio.dto.MultipleCtaSites;

@Service
public class CtaSiteServiceImpl {

	private static final Logger LOGGER = Logger
			.getLogger(CtaSiteServiceImpl.class);
	@Autowired
	CtaSiteDaoImpl ctaSiteDaoImpl;

	public CtaSite getCtaSite(int siteNum) throws Exception {
		return ctaSiteDaoImpl.get(siteNum);
	}

	public List<CtaSite> getAllCtaSites() throws Exception {
		return ctaSiteDaoImpl.getAll();
	}

	public void createCtaSite(CtaSite object) throws Exception {
		ctaSiteDaoImpl.save(object);
	}

	public void deleteCtaSite(int siteNum) throws Exception {
		CtaSite ctaSite = ctaSiteDaoImpl.get(siteNum);
		ctaSiteDaoImpl.delete(ctaSite);
	}

	public void updateCtaSite(CtaSite object) throws Exception {
		ctaSiteDaoImpl.update(object);
	}
	
	public List<CtaSite> getCtaSiteByVersionNumber(String versionNumber) {
		return ctaSiteDaoImpl.getCtaSiteByVersionNumber(versionNumber);
	}

	public MultipleCtaSites performMultipleOperation(
			MultipleCtaSites multipleCtaSites) throws Exception {
		multipleCtaSites = ctaSiteDaoImpl
				.performMultipleOperation(multipleCtaSites);
		return multipleCtaSites;
	}

}
