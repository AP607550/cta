/********************
+------------------------------------------------------------+
 | Class Name: CtaStudyServiceImpl.java                                  |
 | Class Purpose: Service for cta_study                          |
 | Comment : This is Service layer for cta_study                 |
 | Author : Mriganka Shekhar Roy                 |
+------------------------------------------------------------+
 ************/
package com.ctastudio.service;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ctastudio.dao.CtaStudyDaoImpl;
import com.ctastudio.dto.CtaStudy;
import com.ctastudio.dto.MultipleCtaStudies;
import com.ctastudio.dto.MultipleEntities;

@Service
public class CtaStudyServiceImpl {

	private static final Logger LOGGER = Logger
			.getLogger(CtaStudyServiceImpl.class);
	@Autowired
	CtaStudyDaoImpl ctaStudsDaoImpl;

	public CtaStudy getCtaStudy(int siteNum) throws Exception {
		return ctaStudsDaoImpl.get(siteNum);
	}

	public List<CtaStudy> getAllCtaStudys() throws Exception {
		return ctaStudsDaoImpl.getAll();
	}

	public void createCtaStudy(CtaStudy object) throws Exception {
		ctaStudsDaoImpl.save(object);
	}

	public void deleteCtaStudy(int siteNum) throws Exception {
		CtaStudy ctaStudy = ctaStudsDaoImpl.get(siteNum);
		ctaStudsDaoImpl.delete(ctaStudy);
	}

	public void updateCtaStudy(CtaStudy object) throws Exception {
		ctaStudsDaoImpl.update(object);
	}

	public List<CtaStudy> getCtaStudyByVersionNumber(String versionNumber) {
		return ctaStudsDaoImpl.getCtaStudyByVersionNumber(versionNumber);
	}

	public MultipleCtaStudies performMultipleOperation(
			MultipleCtaStudies multipleCtaStudies) throws Exception {
		multipleCtaStudies = ctaStudsDaoImpl
				.performMultipleOperation(multipleCtaStudies);
		return multipleCtaStudies;
	}
}
