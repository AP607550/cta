/********************
+------------------------------------------------------------+
 | Class Name: CtaSubjectServiceImpl.java                                  |
 | Class Purpose: Service for cta_subject                          |
 | Comment : This is Service layer for cta_subject                 |
 | Author : Mriganka Shekhar Roy                 |
+------------------------------------------------------------+
************/
package com.ctastudio.service;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ctastudio.dao.CtaSubjectDaoImpl;
import com.ctastudio.dto.CtaSubject;
import com.ctastudio.dto.MultipleCtaSubjects;

@Service
public class CtaSubjectServiceImpl {

	private static final Logger LOGGER = Logger
			.getLogger(CtaSiteServiceImpl.class);
	@Autowired
	CtaSubjectDaoImpl ctaSubjectDaoImpl;
	
	public CtaSubject getCtaSubject(String siteNum) throws Exception {
		return ctaSubjectDaoImpl.get(siteNum);
	}

	public List<CtaSubject> getAllCtaSubjects() throws Exception {
		return ctaSubjectDaoImpl.getAll();
	}

	public void createCtaSubject(CtaSubject object) throws Exception {
		ctaSubjectDaoImpl.save(object);
	}

	public void deleteCtaSubject(String siteNum) throws Exception {
		CtaSubject ctaSubject = ctaSubjectDaoImpl.get(siteNum);
		ctaSubjectDaoImpl.delete(ctaSubject);
	}

	public void updateCtaSubject(CtaSubject object) throws Exception {
		ctaSubjectDaoImpl.update(object);
	}
	
	public List<CtaSubject> getCtaSubjectByVersionNumber(String versionNumber) {
		return ctaSubjectDaoImpl.getCtaSubjectByVersionNumber(versionNumber);
	}

	public MultipleCtaSubjects performMultipleOperation(
			MultipleCtaSubjects multipleCtaSubjects) throws Exception {
		multipleCtaSubjects = ctaSubjectDaoImpl
				.performMultipleOperation(multipleCtaSubjects);
		return multipleCtaSubjects;
	}
	
}
