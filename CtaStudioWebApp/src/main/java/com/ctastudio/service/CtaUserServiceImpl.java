/********************
+------------------------------------------------------------+
 | Class Name: CtaUserServiceImpl.java                                  |
 | Class Purpose: Service for cta_user                          |
 | Comment : This is Service layer for cta_user                 |
 | Author : Mriganka Shekhar Roy                 |
+------------------------------------------------------------+
 ************/
package com.ctastudio.service;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ctastudio.dao.CtaUserDaoImpl;
import com.ctastudio.dto.CtaUser;
import com.ctastudio.dto.MultipleCtaUsers;

@Service
public class CtaUserServiceImpl {
	private static final Logger LOGGER = Logger
			.getLogger(CtaUserServiceImpl.class);
	@Autowired
	CtaUserDaoImpl ctaUserDaoImpl;

	public CtaUser getCtaUser(String siteNum) throws Exception {
		return ctaUserDaoImpl.get(siteNum);
	}

	public List<CtaUser> getAllCtaUsers() throws Exception {
		return ctaUserDaoImpl.getAll();
	}

	public void createCtaUser(CtaUser object) throws Exception {
		ctaUserDaoImpl.save(object);
	}

	public void deleteCtaUser(String siteNum) throws Exception {
		CtaUser ctaUser = ctaUserDaoImpl.get(siteNum);
		ctaUserDaoImpl.delete(ctaUser);
	}

	public void updateCtaUser(CtaUser object) throws Exception {
		ctaUserDaoImpl.update(object);
	}
	
	public List<CtaUser> getCtaUserByVersionNumber(String versionNumber) {
		return ctaUserDaoImpl.getCtaUserByVersionNumber(versionNumber);
	}

	public MultipleCtaUsers performMultipleOperation(
			MultipleCtaUsers multipleCtaUsers) throws Exception {
		multipleCtaUsers = ctaUserDaoImpl
				.performMultipleOperation(multipleCtaUsers);
		return multipleCtaUsers;
	}
}
