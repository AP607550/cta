package com.ctastudio.util;

public interface Operations {

	public static final String UPDATE_OPERATION = "UPDATE";

	public static final String CREATE_OPERATION = "CREATE";

	public static final String DELETE_OPERATION = "DELETE";
}
