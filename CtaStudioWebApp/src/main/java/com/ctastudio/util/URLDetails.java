/********************
+------------------------------------------------------------+
 | Class Name: URLDetails.java                                  |
 | Class Purpose: All API URLs                         |
 | Comment : Contains all API details                |
 | Author : Mriganka Shekhar Roy                 |
+------------------------------------------------------------+
 ************/
package com.ctastudio.util;

public interface URLDetails {

	public static final String CtaSite_Get_On_site_num = "/ctaSite/{site_num}";

	public static final String CtaSite_Get_All = "/ctaSites";

	public static final String CtaSite_Create = "/ctaSite/create";

	public static final String CtaSite_Update = "/ctaSite/update";

	public static final String CtaSite_Delete = "/ctaSite/delete/{site_num}";

	public static final String CtaSite_Multiple = "/ctaSite/multiple";

	public static final String CtaSite_Get_By_VersionNumber = "/ctaSite/version/{versionNumber}";

	public static final String CtaAvailableFormSection_Get_On_id = "/ctaAvailableFormSection/form_num/{formNum}/form_section/{formSection}";

	public static final String CtaAvailableFormSection_Get_All = "/ctaAvailableFormSections";

	public static final String CtaAvailableFormSection_Create = "/ctaAvailableFormSection/create";

	public static final String CtaAvailableFormSection_Update = "/ctaAvailableFormSection/update";

	public static final String CtaAvailableFormSection_Delete = "/ctaAvailableFormSection/delete/form_num/{formNum}/form_section/{formSection}";

	public static final String CtaAvailableForm_Get_On_id = "/ctaAvailableForm/{id}";

	public static final String CtaAvailableForm_Get_All = "/ctaAvailableForms";

	public static final String CtaAvailableForm_Create = "/ctaAvailableForm/create";

	public static final String CtaAvailableForm_Update = "/ctaAvailableForm/update";

	public static final String CtaAvailableForm_Delete = "/ctaAvailableForm/delete/{id}";

	public static final String CtaAvailableForm_Multiple = "/ctaAvailableForm/multiple";

	public static final String CtaAvailableForm_Get_By_VersionNumber = "/ctaAvailableForm/version/{versionNumber}";

	public static final String CtaStudy_Get_On_id = "/ctaStudy/{id}";

	public static final String CtaStudy_Get_All = "/ctaStudys";

	public static final String CtaStudy_Create = "/ctaStudy/create";

	public static final String CtaStudy_Update = "/ctaStudy/update";

	public static final String CtaStudy_Multiple = "/ctaStudy/multiple";

	public static final String CtaStudy_Get_By_VersionNumber = "/ctaStudy/version/{versionNumber}";

	public static final String CtaStudy_Delete = "/ctaStudy/delete/{id}";

	public static final String CtaSubject_Get_On_id = "/ctaSubject/{id}";

	public static final String CtaSubject_Get_All = "/ctaSubjects";

	public static final String CtaSubject_Create = "/ctaSubject/create";

	public static final String CtaSubject_Update = "/ctaSubject/update";

	public static final String CtaSubject_Delete = "/ctaSubject/delete/{id}";

	public static final String CtaSubject_Multiple = "/ctaSubject/multiple";

	public static final String CtaSubject_Get_By_VersionNumber = "/ctaSubject/version/{versionNumber}";

	public static final String CtaUser_Get_On_id = "/ctaUser/{id}";

	public static final String CtaUser_Get_All = "/ctaUsers";

	public static final String CtaUser_Create = "/ctaUser/create";

	public static final String CtaUser_Update = "/ctaUser/update";

	public static final String CtaUser_Delete = "/ctaUser/delete/{id}";

	public static final String CtaUser_Multiple = "/ctaUser/multiple";

	public static final String CtaUser_Get_By_VersionNumber = "/ctaUser/version/{versionNumber}";

	public static final String CtaProtocol_Get_On_id = "/ctaProtocol/{id}";

	public static final String CtaProtocol_Get_All = "/ctaProtocols";

	public static final String CtaProtocol_Create = "/ctaProtocol/create";

	public static final String CtaProtocol_Update = "/ctaProtocol/update";

	public static final String CtaProtocol_Delete = "/ctaProtocol/delete/{id}";

	public static final String CtaProtocol_Multiple = "/ctaProtocol/multiple";

	public static final String CtaProtocol_Get_By_VersionNumber = "/ctaProtocol/version/{versionNumber}";
	
	public static final String CtaProtocolVisit_Get_On_id = "/ctaProtocolVisit/protocolNum/{protocolNum}/protocolVisitNum/{protocolVisitNum}";

	public static final String CtaProtocolVisit_Get_All = "/ctaProtocolVisits";

	public static final String CtaProtocolVisit_Create = "/ctaProtocolVisit/create";

	public static final String CtaProtocolVisit_Update = "/ctaProtocolVisit/update";

	public static final String CtaProtocolVisit_Delete = "/ctaProtocolVisit/delete/protocolNum/{protocolNum}/protocolVisitNum/{protocolVisitNum}";
	
}
