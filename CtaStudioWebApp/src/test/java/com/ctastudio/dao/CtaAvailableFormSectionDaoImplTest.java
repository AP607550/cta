package com.ctastudio.dao;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.ctastudio.dto.CtaAvailableFormSection;
import com.ctastudio.dto.CtaAvailableFormSectionPK;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(locations = { "classpath:ctaStudio-servlet.xml" })
public class CtaAvailableFormSectionDaoImplTest {

	@Autowired
	private CtaAvailableFormSectionDaoImpl ctaAvailableFormSectionDaoImpl;
	
	@Test
	public void testGetCall() throws Exception {
		CtaAvailableFormSectionPK pk = new CtaAvailableFormSectionPK();
		pk.setFormNum(1);
		pk.setFormSection("1");
		CtaAvailableFormSection formSection = ctaAvailableFormSectionDaoImpl.get(pk);
		System.out.println(formSection.toString());
		
	}
}
