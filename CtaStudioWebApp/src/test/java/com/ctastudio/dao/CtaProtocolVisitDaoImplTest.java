package com.ctastudio.dao;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.ctastudio.dto.CtaProtocolVisit;
import com.ctastudio.dto.CtaProtocolVisitPK;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(locations = { "classpath:ctaStudio-servlet.xml" })
public class CtaProtocolVisitDaoImplTest {

	@Autowired
	private CtaProtocolVisitDaoImpl ctaProtocolVisitDaoImpl;

	@Test
	public void testGetCall() throws Exception {
		CtaProtocolVisitPK ctaProtocolVisitPK = new CtaProtocolVisitPK();
		ctaProtocolVisitPK.setProtocolNum(1);
		ctaProtocolVisitPK.setProtocolVisitNum(1);
		CtaProtocolVisit ctaProtocolVisit = ctaProtocolVisitDaoImpl
				.get(ctaProtocolVisitPK);
		System.out.println(ctaProtocolVisit.getId().getProtocolNum());
		ctaProtocolVisitDaoImpl.getCtaProtocolVisitByProtocolNumber(1);
		Integer i = ctaProtocolVisitDaoImpl.getMaxProtocolVistNumberForProtocolNumber(2);
		if(i == null)
		{
			System.out.println("Null value");
		}
		 i = ctaProtocolVisitDaoImpl.getMaxProtocolVistNumberForProtocolNumber(1);
		 System.out.println(i.intValue()+1);

	}

	/*@Test
	public void testAddCall() throws Exception {
		CtaProtocolVisit ctaProtocolVisit = new CtaProtocolVisit();
		ctaProtocolVisit.setProtocolNum(1);
		//ctaProtocolVisit.setProtocolVisitNum(11);
		ctaProtocolVisit.setVisitType(0);
		ctaProtocolVisit.setVisitName("Unplanned");
		//ctaProtocolVisitDaoImpl.save(ctaProtocolVisit);
	}*/
}
