package com.ctastudio.dao;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import static org.junit.Assert.*;

import com.ctastudio.dto.CtaStudy;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(locations = { "classpath:ctaStudio-servlet.xml" })
public class CtaStudyDaoImplTest {
	
	@Autowired
	private CtaStudyDaoImpl ctaStudyDaoImpl;
	
	@Test
	public void testGetCtaStudyByVersionNumber() throws Exception {
		List<CtaStudy> ctaStudy = ctaStudyDaoImpl.getCtaStudyByVersionNumber("RVP30-001");
		
	}

}
