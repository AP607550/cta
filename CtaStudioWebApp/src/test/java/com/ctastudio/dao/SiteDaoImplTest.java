package com.ctastudio.dao;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.ctastudio.dto.CtaSite;
import com.ctastudio.dto.CtaStudy;
import com.ctastudio.dto.DomainObject;
import com.ctastudio.dto.Entities;
import com.ctastudio.dto.MultipleCtaStudies;
import com.ctastudio.dto.MultipleEntities;
import com.ctastudio.util.Operations;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(locations = { "classpath:ctaStudio-servlet.xml" })
public class SiteDaoImplTest {

	private static final Logger LOGGER = Logger
			.getLogger(SiteDaoImplTest.class);

	@Autowired
	@Qualifier("ctaSiteDaoImpl")
	private CtaSiteDaoImpl ctaSiteDaoImpl;

	/*
	 * @Test public void testInsertion() throws Exception { CtaSite ctaSite =
	 * new CtaSite(); //ctaSite.setSiteNum(31); ctaSite.setSiteCode("BLR");
	 * ctaSite.setSiteName("Bangalore"); ctaSiteDaoImpl.save(ctaSite); }
	 */

	/*
	 * @Test public void testModify() throws Exception { CtaSite ctaSite =
	 * ctaSiteDaoImpl.get(2); ctaSite.setSiteCode("UUU");
	 * ctaSiteDaoImpl.update(ctaSite); }
	 */

	@Test
	public void testDBConnection() throws Exception {
		List<CtaSite> ctaSiteList = ctaSiteDaoImpl.getAll();
		/*MultipleEntities<List<Entities<CtaStudy>>, CtaStudy> multipleEntities = new MultipleEntities<List<Entities<CtaStudy>>, CtaStudy>(
				new ArrayList<Entities<CtaStudy>>(), new CtaStudy());
		Entities entities = new Entities();
		entities.setOperation(Operations.UPDATE_OPERATION);
		List<DomainObject> dtoObjects = new ArrayList<DomainObject>();
		dtoObjects.add(new CtaStudy());
		dtoObjects.add(new CtaStudy());
		entities.setDtoObjects(dtoObjects);
		List<Entities<CtaStudy>> allEntities = new ArrayList<Entities<CtaStudy>>();
		allEntities.add(entities);
		multipleEntities.setAllEntities(allEntities);
		ObjectMapper mapper = new ObjectMapper();
		System.out.println(mapper.writeValueAsString(multipleEntities));
		String ss = mapper.writeValueAsString(multipleEntities);
		// MultipleEntities<CtaStudy> dd = new MultipleEntities<CtaStudy>();
		multipleEntities = mapper
				.readValue(
						ss,
						new TypeReference<MultipleEntities<List<Entities<CtaStudy>>, CtaStudy>>() {
						});*/
		MultipleCtaStudies dd = new MultipleCtaStudies();
		
		List<CtaStudy> dtoObjects = new ArrayList<CtaStudy>();
		dtoObjects.add(new CtaStudy());
		dtoObjects.add(new CtaStudy());
		MultipleCtaStudies.CtaStudies pp = new MultipleCtaStudies.CtaStudies(Operations.UPDATE_OPERATION,dtoObjects);
		List<MultipleCtaStudies.CtaStudies> dd1 = new ArrayList<MultipleCtaStudies.CtaStudies>();
		dd1.add(pp);
		dd.setCtaStudies(dd1);
		ObjectMapper mapper = new ObjectMapper();
		System.out.println(mapper.writeValueAsString(dd));
		String ss = mapper.writeValueAsString(dd);
		dd = mapper.readValue(ss, MultipleCtaStudies.class);

	}

}
